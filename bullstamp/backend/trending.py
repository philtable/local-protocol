import functools
import math
import requests
import json
import queries
import time

from flask import request, jsonify, session
from datetime import datetime


def trending(result):
    APP_TOKEN_UNIT = 10000

    DEFAULT_PAGE_SIZE = 25
    MAX_PAGE_SIZE = 100

    def safe_div(x,y):
        if y == 0:
            return 0
        return x / y

    def average(data):
        sum = functools.reduce((lambda sum, value: sum + value), data)
        avg = safe_div(sum, len(data))
        return avg

    def standardDeviationAndAverage(values):
        avg = average(values)

        squareDiffs = list(map((lambda value: (value - avg) * (value - avg)), values))

        avgSquareDiff = average(squareDiffs)

        stdDev = math.sqrt(avgSquareDiff)
        return [stdDev, avg]


    timeStampsArr = []
    votesArr = []
    numRepliesArr = []

    for stamp in result:
        # check if timestamp has a decimal which messes up the conversion from datetime str to timestamp
        if (len(stamp["timestamp"])) == 21:
            stamp["timestamp"] = time.mktime(time.strptime(stamp["timestamp"], "%Y-%m-%dT%H:%M:%S.%f")) + 0.5
        else:
            stamp["timestamp"] = time.mktime(time.strptime(stamp["timestamp"], "%Y-%m-%dT%H:%M:%S"))
        timeStampsArr.append(stamp["timestamp"])
        votesArr.append(safe_div(stamp["tips"], APP_TOKEN_UNIT))
        numRepliesArr.append(stamp["num_replies"])

    stdDevAvgTimeStamps = standardDeviationAndAverage(timeStampsArr)
    stdDevAvgVotes = standardDeviationAndAverage(votesArr)
    stdDevAvgNumReplies = standardDeviationAndAverage(votesArr)

    # compute z score
    for stamp in result:
        zScoreVotes = min((safe_div(stamp["tips"], APP_TOKEN_UNIT) - safe_div(stdDevAvgVotes[1], stdDevAvgVotes[0])), 0.5)
        zScoreTime = safe_div((stamp["timestamp"] - stdDevAvgTimeStamps[1]), stdDevAvgTimeStamps[0])
        zScoreReplies = safe_div((stamp["num_replies"] - stdDevAvgNumReplies[1]), stdDevAvgNumReplies[0])
        # after brief expirement, time should be less weighted
        stamp["score"] = zScoreVotes + zScoreReplies + zScoreTime

    # sort by score
    result.sort(key=lambda stamp: stamp["score"], reverse=True)

    # result top N results for score
    # result["stamps"] = result["stamps"].slice(0, topN);

    # delete the score and change timestamp to datetime before sending to front-end
    for stamp in result:
        del stamp["score"]
        stamp["timestamp"] = datetime.fromtimestamp(stamp["timestamp"]).strftime('%Y-%m-%dT%H:%M:%S.%f')[:-5]

    # result["stamps"] = result["stamps"].filter(stamp => stamp["timestamp"] > LAUNCH_DATE)

    return result
