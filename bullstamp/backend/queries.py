from sqlalchemy.sql import text
from app import db
import models
from constants import Actions


def get_stamp(stamp_id):
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        row_to_json(a) AS stamp
    FROM (
        {format_stamp}
        WHERE s.transaction_id = decode(:id, 'hex')
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), id=stamp_id).fetchone()
        return result['stamp'] if result else None


def get_stamps(offset, limit):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS stamps
    FROM (
        {format_stamp}
        WHERE reply_to_id IS NULL
        ORDER BY s.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), offset=offset, limit=limit)
    
        stamps = cursor.fetchone()['stamps']
        return stamps if stamps is not None else []


def get_stamps_by_author(author, offset, limit):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
        SELECT
            array_to_json(array_agg(row_to_json(a))) AS stamps
        FROM (
            {format_stamp}
            WHERE s.author = :author
            ORDER BY s.timestamp DESC
            OFFSET :offset
            LIMIT :limit
        ) a
        """.format(format_stamp=__FORMAT_STAMP__)), author=author, offset=offset, limit=limit)

        stamps = cursor.fetchone()['stamps']

        for stamp in stamps:
            stamp['parent_stamp'] = get_stamp(stamp["reply_to"])

        return stamps if stamps is not None else []


def get_replies(stamp_id, offset, limit):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS replies
    FROM (
        {format_stamp}
        WHERE s.reply_to_id = decode(:id, 'hex')
        ORDER BY s.timestamp ASC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), id=stamp_id, offset=offset, limit=limit)
        replies = cursor.fetchone()['replies']
        return replies if replies is not None else []


def get_tip(tip_id):
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        row_to_json(a) AS tip
    FROM (
        SELECT
            ENCODE(t.transaction_id, 'hex') AS transaction_id,
            t.timestamp,
            t.seq,
            t.creator,
            t.amount,
            row_to_json(l) as to
        FROM tips t
        INNER JOIN LATERAL (
            {format_stamp}
        ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
        WHERE t.transaction_id = decode(:id, 'hex')
    ) a;
    """.format(format_stamp=__FORMAT_STAMP__)), id=tip_id).fetchone()
        return result['tip'] if result else None


def get_tips(offset, limit):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM (
        SELECT
            ENCODE(t.transaction_id, 'hex') AS transaction_id,
            t.timestamp,
            t.seq,
            t.creator,
            t.amount,
            row_to_json(l) as to
        FROM tips t
        INNER JOIN LATERAL (
            {format_stamp}
        ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
        ORDER BY t.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a;
    """.format(format_stamp=__FORMAT_STAMP__)), offset=offset, limit=limit)

        tips = cursor.fetchone()['tips']
        return tips if tips is not None else []


def get_tips_by_creator(creator, offset, limit):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM (
        SELECT
            ENCODE(t.transaction_id, 'hex') AS transaction_id,
            t.timestamp,
            t.seq,
            t.creator,
            t.amount,
            row_to_json(l) as to
        FROM tips t
        INNER JOIN LATERAL (
            {format_stamp}
        ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
        WHERE t.creator = :creator
        ORDER BY t.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), creator=creator, offset=offset, limit=limit)

    tips = cursor.fetchone()['tips']
    return tips if tips is not None else []



def get_tips_by_stamp(stamp_id):
    with db.engine.begin() as connection:
        cursor = connection.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM (
        SELECT
            ENCODE(t.transaction_id, 'hex') AS transaction_id,
            t.timestamp,
            t.seq,
            t.creator,
            t.amount,
            row_to_json(l) as to
        FROM tips t
        INNER JOIN LATERAL (
            {format_stamp}
        ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
        WHERE t.stamp_id = decode(:stamp_id, 'hex')
        ORDER BY t.timestamp DESC
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), stamp_id=stamp_id)

    tips = cursor.fetchone()['tips']
    return tips if tips is not None else []


def get_avatar(username):
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        row_to_json(a) AS avatar
    FROM (
        SELECT
            ENCODE(a.transaction_id, 'hex') AS transaction_id,
            a.timestamp,
            a.seq,
            a.username AS username,
            a.url
        FROM avatars a
        WHERE a.username = :username
        ORDER BY a.seq DESC
        LIMIT 1
    ) a;
    """), username=username).fetchone()
        return result['avatar'] if result else None


# backfill for backend queries
def get_stamps_seq_latest():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        seq
    FROM stamps
    WHERE timestamp = (SELECT MAX(timestamp) FROM stamps)
        AND reply_to_id IS NULL
    """)).fetchone()
    return result['seq'] if result else 0

def get_replies_seq_latest():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        seq
    FROM stamps
    WHERE timestamp = (SELECT MAX(timestamp) FROM stamps)
        AND reply_to_id IS NOT NULL
    """)).fetchone()
    return result['seq'] if result else 0

def get_tips_seq_latest():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        seq
    FROM tips
    WHERE timestamp = (SELECT MAX(timestamp) FROM tips)
    """)).fetchone()
    return result['seq'] if result else 0



def truncate_stamps():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
        TRUNCATE TABLE stamps
        CASCADE
        """))

def get_stamps_count():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        COUNT(*)
    FROM stamps
    """)).fetchone()
    return result['count'] if result else None

def get_stamps_all():
    with db.engine.begin() as connection:
        result = connection.execute(text("""
    SELECT
        transaction_id
    FROM stamps
    WHERE reply_to_id IS NULL
    ORDER BY seq ASC
    """)).fetchall()
    return result if result else None

def get_notifications(username, offset, limit):
    cursor = models.Notification.query.filter_by(username=username).order_by(
        models.Notification.timestamp.desc()
    ).limit(limit).offset(offset)

    notifications = cursor.all()

    result = [{
        'id': notification.id,
        'transaction_id': notification.transaction_id,
        'timestamp': notification.timestamp,
        'username': notification.username,
        'action_type': notification.action_type.value,
        'is_read': notification.is_read
    } for notification in notifications]

    for entry in result:
        if entry["action_type"] == Actions.STAMP.value:  # Someone replied to your stamp
            entry['stamp'] = get_stamp(entry["transaction_id"])
            entry['parent_stamp'] = get_stamp(entry["stamp"]["reply_to"])
        elif entry["action_type"] == Actions.TIP.value:  # Someone tipped your stamp
            entry['tip'] = get_tip(entry["transaction_id"])

    return result


__FORMAT_STAMP__ = """
SELECT
    ENCODE(s.transaction_id, 'hex') AS transaction_id,
    s.seq,
    s.timestamp,
    s.author,
    ENCODE(s.reply_to_id, 'hex') AS reply_to,
    s.content,
    num_replies,
    tips as tips
FROM stamps s
LEFT JOIN LATERAL (
    SELECT COALESCE(COUNT(*), 0) AS num_replies
    FROM stamps r
    WHERE s.transaction_id = r.reply_to_id
) r ON TRUE
LEFT JOIN LATERAL (
    SELECT COALESCE(SUM(amount), 0) AS tips
    FROM tips t
    WHERE s.transaction_id = t.stamp_id
) t ON TRUE
"""
