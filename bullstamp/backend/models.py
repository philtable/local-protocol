from constants import Actions

from app import db


class TransactionIdColumn(db.TypeDecorator):
    impl = db.LargeBinary

    def __init__(self):
        self.impl.length = 16
        db.TypeDecorator.__init__(self, length=self.impl.length)

    def process_bind_param(self, value, dialect):
        return bytearray.fromhex(value) if value else None

    def process_result_value(self, value, dialect):
        return bytes(value).hex() if value else None


class Stamp(db.Model):
    __tablename__ = 'stamps'

    transaction_id = db.Column(TransactionIdColumn, primary_key=True, nullable=False)
    seq = db.Column(db.BigInteger, index=True, nullable=False)
    timestamp = db.Column(db.DateTime, index=True, nullable=False)
    author = db.Column(db.CHAR(length=12), index=True, nullable=False)
    reply_to_id = db.Column(TransactionIdColumn, db.ForeignKey('stamps.transaction_id'), index=True, nullable=True)
    content = db.Column(db.String, nullable=False)

    reply_to = db.relationship('Stamp', remote_side=[transaction_id])


class Tip(db.Model):
    __tablename__ = 'tips'

    transaction_id = db.Column(TransactionIdColumn, primary_key=True, nullable=False)
    seq = db.Column(db.BigInteger, index=True, nullable=False)
    timestamp = db.Column(db.DateTime, index=True, nullable=False)
    creator = db.Column(db.CHAR(length=12), index=True, nullable=False)
    stamp_id = db.Column(TransactionIdColumn, db.ForeignKey('stamps.transaction_id'), index=True, nullable=False)
    amount = db.Column(db.BigInteger, nullable=False)

    stamp = db.relationship('Stamp')


class Avatar(db.Model):
    __tablename__ = 'avatars'

    transaction_id = db.Column(TransactionIdColumn, primary_key=True, nullable=False)
    seq = db.Column(db.BigInteger, index=True, nullable=False)
    timestamp = db.Column(db.DateTime, index=True, nullable=False)
    username = db.Column(db.CHAR(length=12), index=True, nullable=False)
    url = db.Column(db.String, nullable=False)


class Following(db.Model):
    __tablename__ = 'following'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.CHAR(length=12), index=True)
    following_username = db.Column(db.CHAR(length=12), index=True)


class Notification(db.Model):
    __tablename__ = 'notifications'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    transaction_id = db.Column(TransactionIdColumn, index=True, nullable=False)
    timestamp = db.Column(db.DateTime, index=True, nullable=False)
    username = db.Column(db.CHAR(length=12), index=True, nullable=False)
    action_type = db.Column(db.Enum(Actions), index=True, nullable=False)
    is_read = db.Column(db.Boolean, default=False)
