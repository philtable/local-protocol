from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_socketio import SocketIO
from flask_session import Session

app = Flask(__name__)

app.config.from_object('config.Config')
app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] = 'super secret key'
app.config['MAX_CONTENT_LENGTH'] = 100 * 1024
Session(app)

ALLOWED_ORIGINS = ['https://bullstamp.co', 'http://localhost:3000', 'http://127.0.0.1:3000']

CORS(app, origins=ALLOWED_ORIGINS, supports_credentials=True)

socketio = SocketIO(app, cors_allowed_origins=ALLOWED_ORIGINS)

# Initialize Postgres.
db = SQLAlchemy(app)
migrate = Migrate(app, db)
