from eospy.keys import EOSKey
from eospy.utils import sha256, hex_to_int
from binascii import hexlify, unhexlify
import ecdsa


def ecrecover(msg, sig):
    digest = sha256(msg.encode('utf-8'))
    return ecrecover_hash(digest, sig)


def ecrecover_hash(digest, encoded_sig):
    encoded_sig = encoded_sig[4:]
    # remove curve prefix
    curvePre = encoded_sig[:3].strip('_')
    if curvePre != 'K1':
        raise TypeError('Unsupported curve prefix {}'.format(curvePre))

    try:
        decoded_sig = EOSKey._check_decode(None, encoded_sig[3:], curvePre)
        # first 2 bytes are recover param
        recover_param = hex_to_int(decoded_sig[:2]) - 4 - 27
        # use sig
        sig = decoded_sig[2:]
        # verify sig

        p = EOSKey._recover_key(None, unhexlify(digest), unhexlify(sig), recover_param)

        r = p.verify_digest(unhexlify(sig), unhexlify(digest), sigdecode=ecdsa.util.sigdecode_string)
        if not r:
            return None

        x_str = ecdsa.util.number_to_string(p.pubkey.point.x(), p.pubkey.generator.order())
        hex_data = bytearray(chr(2 + (p.pubkey.point.y() & 1)), 'utf-8')
        compressed = hexlify(hex_data + x_str).decode()

        recovered_key = "EOS" + EOSKey._check_encode(None, compressed).decode()
    except ValueError as e:
        return None

    return recovered_key

# expected = "EOS66C1jZHvMFZZwR7R2qEtfnNZThFcyMWyYue9cHQTy1ZtdmGUs4"
# recovered = ecrecover("authority=fqyfyyphiczp@active,time=1537586391502",
#                       'SIG_K1_KjpkHQxvyMyhWHtuu2oKirpM4wvyhFxk8Dxc59mvn7ncCtpjyynFAeQNJ4q28hv3tKxgTJkMfmw8YxJsbefXwRSTLQd1oc')
# print(expected)
# print(recovered)
