from datetime import datetime
import binascii
import http
import json
import queries

from app import db
from models import Stamp, Tip, TransactionIdColumn

from sqlalchemy.orm import sessionmaker


PROD_URI = 'api.apptoken.co'
DEV_URI  = '0.0.0.0'
DEV_PORT = 5557
is_prod = True # toggle True pull from prod

if is_prod: 
    conn = http.client.HTTPSConnection(PROD_URI)
else:
    conn = http.client.HTTPConnection(DEV_URI, port=DEV_PORT)

def backfill_stamps_after_seqid(conn):
    db_session = sessionmaker(bind=db.engine)()

    latest_seq = queries.get_stamps_seq_latest() or 0

    conn.request("GET", "/stamps/backfill/{0}".format(1))
    #print ('latest in db', queries.get_tips_seq_latest())

    response = conn.getresponse()
    response_content = response.read()

    stamps = json.loads(response_content)

    #print ('incoming', len(stamps))
    process_and_stamps(db_session, stamps)
    #print('in local', queries.get_stamps_count())

def backfill_replies_after_seqid(conn):
    db_session = sessionmaker(bind=db.engine)()

    latest_seq = queries.get_replies_seq_latest() or 0

    conn.request("GET", "/replies/backfill/{0}".format(latest_seq))
    response = conn.getresponse()
    response_content = response.read()

    stamps = json.loads(response_content)
    process_and_stamps(db_session, stamps, True)


def backfill_tips_after_seqid(conn):
    db_session = sessionmaker(bind=db.engine)()

    latest_seq = queries.get_tips_seq_latest() or 0

    conn.request("GET", "/tips/backfill/{0}".format(latest_seq))

    response = conn.getresponse()
    response_content = response.read()
    tips = json.loads(response_content)

    for a_tip in tips:
        temp_tip = {}

        temp_tip['transaction_id'] = bytes(a_tip['transaction_id'],'utf8')
        byte_str = str(temp_tip['transaction_id'])
        temp_tip['transaction_id'] = byte_str[5:len(byte_str)-1]

        temp_tip['seq'] = int(a_tip['seq'])
        
        try:
            ts = datetime.strptime(a_tip['timestamp'],'%Y-%m-%dT%H:%M:%S')
        except ValueError:
            ts = datetime.strptime(a_tip['timestamp'],'%Y-%m-%dT%H:%M:%S.%f')

        temp_tip['creator'] = a_tip['creator']

        temp_tip['stamp_id'] = bytes(a_tip['stamp_id'],'utf8')
        byte_str = str(temp_tip['stamp_id'])
        temp_tip['stamp_id'] = byte_str[5:len(byte_str)-1]

        temp_tip['amount'] = int(a_tip['amount'])
        temp_tip['timestamp'] = ts

        db_session.add(Tip(**temp_tip))
    try:
        db_session.commit()
    except Exception as e:
        print (str(e))

def process_and_stamps(db_session, stamps, is_replies=False):

    latest_seq = None
    for a_stamp in stamps:

        temp_stamp = {}

        temp_stamp['transaction_id'] = bytes(a_stamp['transaction_id'],'utf8')
        byte_str = str(temp_stamp['transaction_id'])
        temp_stamp['transaction_id'] = byte_str[5:len(byte_str)-1]

        temp_stamp['seq'] = int(a_stamp['seq'])

        temp_stamp['author'] = a_stamp['author']
        temp_stamp['content'] = a_stamp['content']

        if a_stamp['reply_to_id'] is None:
            temp_stamp['reply_to_id'] = None
        else:
            temp_stamp['reply_to_id'] = bytes(a_stamp['reply_to_id'],'utf8')
            byte_str = str(temp_stamp['reply_to_id'])
            temp_stamp['reply_to_id'] = byte_str[5:len(byte_str)-1]

        try:
            ts = datetime.strptime(a_stamp['timestamp'],'%Y-%m-%dT%H:%M:%S')
        except ValueError:
            ts = datetime.strptime(a_stamp['timestamp'],'%Y-%m-%dT%H:%M:%S.%f')
        temp_stamp['timestamp'] = ts


        if is_replies == False:
            existing_stamp_ids.add(temp_stamp['transaction_id'])
        else:
            if temp_stamp['reply_to_id'] not in existing_stamp_ids:
                continue

        try:
            db_session.add(Stamp(**temp_stamp))
        except:
            print ('---------------------why----------------')

        latest_seq = int(a_stamp['seq'])
    try:
        db_session.commit()
    except Exception as e:
        print ('-----commit error ---------')
        print (str(e))


queries.truncate_stamps()
# avoid any FK constraints by getting existing non-reply stamps transaction id into a set
existing_stamp_ids = set()
backfill_stamps_after_seqid(conn)
backfill_replies_after_seqid(conn)
backfill_tips_after_seqid(conn)
