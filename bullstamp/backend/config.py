import os

DB_USERNAME = os.environ.get('DB_USERNAME', 'postgres')
DB_PASSWORD = os.environ.get('DB_PASSWORD', 'password')
DB_NAME = os.environ.get('DB_NAME', 'bullstamp')

class Config(object):
    DEBUG = False
    TESTING = False

    # Postgres settings
    SQLALCHEMY_DATABASE_URI = "postgresql://{}:{}@127.0.0.1:5432/{}".format(DB_USERNAME, DB_PASSWORD, DB_NAME)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
