import argparse
import datetime
import json
import logging
import models
import threading
import zmq

from logging.handlers import RotatingFileHandler
from flask_socketio import join_room
from app import db, socketio
from constants import APPTOKEN_LISTENER_URL, APPTOKEN_LISTENER_PORT, Actions

# zeromq socket connection (bullstamp_backend <- apptoken)
context = zmq.Context()
socket = context.socket(zmq.SUB)


# socket.io server (bullstamp_backend -> frontend)
@socketio.on('connect')
def on_connect():
    print('connected')


@socketio.on('user')
def handle_user(user):
    print('user connected: ', user)
    join_room(user)


@socketio.on('disconnect')
def on_disconnect():
    print('disconnected')


# process methods for zeromq messages
def process_stamp(msg):
    # Soft ignore duplicate submissions.
    stamp = models.Stamp.query.get(msg['data']['transaction_id'])
    if stamp:
        logging.info('Stamp {} already exists'.format(msg['data']['transaction_id']))
        return

    reply_to_id = msg['data']['reply_to']
    stamp_author = msg['data']['author']

    # Check if this stamp is a reply to another
    if reply_to_id:
        reply_to = models.Stamp.query.get(reply_to_id)

        # Check that the stamp author did not create the original stamp
        if stamp_author != reply_to.author:
            # Notify the stamp author
            db.session.add(models.Notification(
                transaction_id=msg['data']['transaction_id'],
                timestamp=msg['data']['timestamp'],
                username=reply_to.author,
                action_type=Actions.STAMP
            ))

        # TEMPORARY: notify everyone who replied
        replies = models.Stamp.query.filter_by(reply_to_id=reply_to_id).all()
        seen = {} # super hacky but cba to get the orm working

        # Author of original stamp already has a notification so don't create another
        seen[reply_to.author] = True

        for reply in replies:
            if reply.author not in seen:
                seen[reply.author] = True

                # Check that you aren't notifying the reply author because they replied previously
                if msg['data']['author'] != reply.author:
                    db.session.add(models.Notification(
                        transaction_id=msg['data']['transaction_id'],
                        timestamp=msg['data']['timestamp'],
                        username=reply.author,
                        action_type=Actions.STAMP
                    ))

    else:
        reply_to = None

    db.session.add(models.Stamp(
        transaction_id=msg['data']['transaction_id'],
        seq=msg['data']['seq'],
        timestamp=msg['data']['timestamp'],
        author=msg['data']['author'],
        reply_to=reply_to,
        content=msg['data']['content']
    ))
    db.session.commit()


# Always take the latest avatar url on the blockchain
def process_avatar(msg):
    db.session.add(models.Avatar(
        transaction_id=msg['data']['transaction_id'],
        seq=msg['data']['seq'],
        timestamp=msg['data']['timestamp'],
        username=msg['data']['username'],
        url=msg['data']['url']
    ))
    db.session.commit()


def process_tip(msg):
    destination_id = msg['data']['to']['transaction_id']
    amount = msg['data']['amount']

    stamp = models.Stamp.query.get(destination_id)
    if stamp:
        db.session.add(models.Tip(
            transaction_id=msg['data']['transaction_id'],
            seq=msg['data']['seq'],
            timestamp=msg['data']['timestamp'],
            creator=msg['data']['creator'],
            stamp=stamp,
            amount=amount,
        ))

        # Notify the stamp author
        db.session.add(models.Notification(
            transaction_id=msg['data']['transaction_id'],
            timestamp=msg['data']['timestamp'],
            username=stamp.author,
            action_type=Actions.TIP
        ))

        db.session.commit()
    else:
        logging.info('Unable to find stamp {}'.format(destination_id))


# mapping from zeromq message type to handler
message_type_to_processor = {
    Actions.STAMP.value: process_stamp,
    Actions.AVATAR.value: process_avatar,
    Actions.TIP.value: process_tip,
}


# process a message from zeromq
def process_message(msg):
    if msg['type'] in message_type_to_processor:
        message_type_to_processor[msg['type']](msg)
    else:
        logging.info('Invalid message type {}'.format(msg['type']))
        pass


# broadcast to frontend
def broadcast(msg):
    if msg['type'] == 'STAMP':
        socketio.emit('event', json.dumps(msg))
    elif msg['type'] == 'TIP':
        socketio.emit('event', json.dumps(msg), room=msg['data']['to']['author'])
        socketio.emit('event', json.dumps(msg), room=msg['data']['creator'])
    elif msg['type'] == 'AVATAR':
        socketio.emit('event', json.dumps(msg), room=msg['data']['username'])


def handle_events():
    logging.info('Starting event handler...')
    while True:
        msg = socket.recv_json()
        logging.info('Received message: {}'.format(msg))
        process_message(msg)
        broadcast(msg)


parser = argparse.ArgumentParser(description='Bullstamp event handler.')
parser.add_argument('--log_to_file', type=bool, default=False)


def start():
    args = parser.parse_args()

    if args.log_to_file:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                            filename='/var/log/bullstamp/poller.log',
                            filemode='w')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s')

    socket.connect('tcp://{}:{}'.format(
        APPTOKEN_LISTENER_URL, APPTOKEN_LISTENER_PORT))
    socket.setsockopt_string(zmq.SUBSCRIBE, '')

    socketio.start_background_task(target=handle_events)
