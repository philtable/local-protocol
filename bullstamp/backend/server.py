import argparse
import json
import sys

import event_manager
import models
import queries

from flask import request, jsonify, session
from eospy.cleos import Cleos
from app import app, db, socketio
from constants import EOS_URL, Actions
from signatures import ecrecover
from dateutil.parser import parse
from datetime import datetime, timedelta
import pytz
from functools import wraps

# upload to DigitalOcean
from boto3 import session as boto3session
from botocore.client import Config
from constants import DO_ACCESS_KEY_ID, DO_SECRET_ACCESS_KEY, ALLOWED_EXTENSIONS

# created functions in this directory
from trending import trending

DEFAULT_PAGE_SIZE = 25
MAX_PAGE_SIZE = 100

ce = Cleos(url=EOS_URL)

class APIError(Exception):
    def __init__(self, message, status_code=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code if status_code is not None else 400

    def to_dict(self):
        return {'error': self.message}


@app.errorhandler(APIError)
def handle_api_error(e):
    response = jsonify(e.to_dict())
    response.status_code = e.status_code
    return response, e.status_code


@app.route('/', methods=['GET'])
def hello():
    return '...'


# @app.before_request
# def expire_session():
#     username = session.get('username', None)
#     if not username:
#         return
#
#     expiry = session.get('expiry')
#
#     now = datetime.utcnow().replace(tzinfo=pytz.utc)
#
#     if now > expiry:
#         session['username'] = None
#         session['expiry'] = None


def authed(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        username = session.get('username', None)
        if not username:
            raise APIError('unauthorized')
        return f(*args, **kwargs)

    return decorated_function


@app.route('/me', methods=['GET'])
def me():
    username = session.get('username', None)
    if not username:
        raise APIError('not authenticated')

    return jsonify({'ok': True, 'username': username})


@app.route('/login', methods=['POST'])
def login():
    data = request.get_json()
    if not data or not data['message'] or not data['signature']:
        raise APIError('invalid body')

    message = data["message"]
    signature = data["signature"]

    try:
        message_obj = json.loads(message)
    except BaseException:
        raise APIError('invalid message')

    if not message_obj["username"] or not message_obj["authority"] or not message_obj["timestamp"]:
        raise APIError('invalid message')

    try:
        signed_at = parse(message_obj["timestamp"])
    except BaseException as e:
        print(e)
        raise APIError('invalid message')

    now = datetime.utcnow().replace(tzinfo=pytz.utc)

    if not (now - timedelta(hours=2) < signed_at < (now + timedelta(hours=2))):
        raise APIError('message expired')

    pubkey = ecrecover(message, signature)
    if not pubkey:
        raise APIError('invalid signature')

    is_valid_key = False

    permissions = ce.get_account(message_obj["username"])["permissions"]
    for permission in permissions:
        if permission["perm_name"] != message_obj["authority"]:
            pass

        for key in permission["required_auth"]["keys"]:
            if pubkey == key["key"]:
                is_valid_key = True
                break

    if not is_valid_key:
        raise APIError('bad signature')

    session['username'] = message_obj["username"]
    session['expiry'] = signed_at + timedelta(days=7)

    return jsonify({'ok': True})


# returns page size, offset, and limit
def get_page_info_from_request():
    page = request.args.get('page', default=1, type=int)
    page_size = request.args.get(
        'page_size', default=DEFAULT_PAGE_SIZE, type=int)
    page_size = min(page_size, MAX_PAGE_SIZE)

    if page < 1:
        raise APIError('Page has to be positive')
    if page_size < 1:
        raise APIError('Page size has to be positive')

    return page_size, (page - 1) * page_size, page_size + 1


@app.route('/stamps/<stamp_id>', methods=['GET'])
def get_stamp(stamp_id):
    stamp = queries.get_stamp(stamp_id)
    if not stamp:
        raise APIError("unable to find stamp", status_code=404)

    return jsonify(stamp)


@app.route('/stamps', methods=['GET'])
def get_stamps():
    page_size, offset, limit = get_page_info_from_request()

    stamps = queries.get_stamps(offset, limit)

    return jsonify({
        'stamps': stamps[:page_size],
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/authors/<author>/stamps', methods=['GET'])
def get_stamps_for_author(author):
    page_size, offset, limit = get_page_info_from_request()

    stamps = queries.get_stamps_by_author(author, offset, limit)

    return jsonify({
        'stamps': stamps[:page_size],
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/stamps/<stamp_id>/replies', methods=['GET'])
def get_replies_for_stamp(stamp_id):
    page_size, offset, limit = get_page_info_from_request()

    replies = queries.get_replies(stamp_id, offset, limit)

    return jsonify({
        'replies': replies[:page_size],
        'page_info': {
            'has_next_page': len(replies) > page_size,
        }
    })


@app.route('/trending', methods=['GET'])
def get_trending():
    page_size, offset, limit = get_page_info_from_request()

    stamps = queries.get_stamps(offset, limit)
    trending_stamps = trending(stamps[:page_size])

    return jsonify({
        'stamps': trending_stamps,
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/tips/<tip_id>', methods=['GET'])
def get_tip(tip_id):
    tip = queries.get_tip(tip_id)
    if not tip:
        raise APIError("unable to find tip", status_code=404)

    return jsonify(tip)


@app.route('/tips', methods=['GET'])
def get_tips():
    page_size, offset, limit = get_page_info_from_request()

    tips = queries.get_tips(offset, limit)

    return jsonify({
        'tips': tips[:page_size],
        'page_info': {
            'has_next_page': len(tips) > page_size,
        }
    })


@app.route('/<creator>/tips', methods=['GET'])
def get_tips_for_creator(creator):
    page_size, offset, limit = get_page_info_from_request()

    tips = queries.get_tips_by_creator(creator, offset, limit)

    return jsonify({
        'tips': tips[:page_size],
        'page_info': {
            'has_next_page': len(tips) > page_size,
        }
    })


@app.route('/stamps/<stamp_id>/tips', methods=['GET'])
def get_tips_for_stamp(stamp_id):

    tips = queries.get_tips_by_stamp(stamp_id)

    return jsonify({
        'tips': tips
    })


@app.route('/authors/<author>/avatar', methods=['GET'])
def get_avatar_for_author(author):
    avatar = queries.get_avatar(author)

    return jsonify({
        'username': author,
        'url': avatar["url"] if avatar else None,
    })


@app.route('/authors/<author>/avatar', methods=['POST'])
@authed

def upload_file_to_digitalocean(author):

    def allowed_file(filename):
        return '.' in filename and \
            filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

    file = request.files[author]

    if allowed_file(file.filename):
        # Initiate session
        boto3sessionsession = boto3session.Session()
        client = boto3sessionsession.client('s3',
                                region_name='nyc3',
                                endpoint_url='https://nyc3.digitaloceanspaces.com',
                                aws_access_key_id=DO_ACCESS_KEY_ID,
                                aws_secret_access_key=DO_SECRET_ACCESS_KEY)

        avatarsBucketName = 'apptoken-avatars'

        # Upload a file to your Space
        client.upload_fileobj(file, avatarsBucketName, session['username'] + '.png', ExtraArgs={'ACL':'public-read'})

    return '', 204


@app.route('/follow', methods=['POST'])
@authed
def follow_user():
    json = request.get_json()
    following_username = json["user_to_follow"]

    following = models.Following.query.filter_by(username=session['username'], following_username=following_username).all()
    if len(following) > 0:
        return 'Already following', 400

    following = models.Following(
        username=session['username'],
        following_username=following_username
    )

    db.session.add(following)
    db.session.commit()

    return '', 204


@app.route('/unfollow', methods=['POST'])
@authed
def unfollow_user():
    json = request.get_json()
    following_username = json["user_to_follow"]

    models.Following.query.filter_by(username=session['username'], following_username=following_username).delete()
    db.session.commit()

    return '', 204


@app.route('/<user_id>/followers', methods=['GET'])
def get_user_followers(user_id):
    page_size, offset, limit = get_page_info_from_request()

    cursor = models.Following.query.filter_by(following_username=user_id).limit(limit).offset(offset)
    followers = cursor.all()
    has_next_page = len(followers) > page_size
    followers = [{
        'id': followers.id,
        'username': followers.username,
    } for followers in followers]

    return jsonify({
        'followers': followers[:page_size],
        'page_info': {'has_next_page': has_next_page}
    })


@app.route('/<user_id>/following', methods=['GET'])
def get_user_following(user_id):
    page_size, offset, limit = get_page_info_from_request()

    cursor = models.Following.query.filter_by(username=user_id).limit(limit).offset(offset)
    following = cursor.all()
    has_next_page = len(following) > page_size
    following = [{
        'id': following.id,
        'username': following.following_username,
    } for following in following]

    return jsonify({
        'following': following[:page_size],
        'page_info': {'has_next_page': has_next_page}
    })

@app.route('/<user_id>/following/<target_user>', methods=['GET'])
def get_user_following_target_user(user_id, target_user):

    following = models.Following.query.filter_by(username=user_id, following_username=target_user).all()
    if len(following) > 0:
        return jsonify({ 'following': 'true' })
    else:
        return jsonify({ 'following': 'false' })


@app.route('/<user_id>/followers/count', methods=['GET'])
def get_user_followers_count(user_id):
    count = models.Following.query.filter_by(following_username=user_id).count()

    return jsonify({
        'num_followers': count
    })


@app.route('/<user_id>/following/count', methods=['GET'])
def get_user_following_count(user_id):
    count = models.Following.query.filter_by(username=user_id).count()

    return jsonify({
        'num_following': count
    })


@app.route('/notifications', methods=['GET'])
@authed
def get_notifications_for_user():
    page_size, offset, limit = get_page_info_from_request()

    notifications = queries.get_notifications(session['username'], offset, limit)
    has_next_page = len(notifications) > page_size

    return jsonify({
        'notifications': notifications[:page_size],
        'page_info': {'has_next_page': has_next_page},
    })


@app.route('/notifications', methods=['POST'])
@authed
def mark_notifications_as_read():
    models.Notification.query.filter_by(username=session['username'], is_read=False).update(dict(is_read=True))
    db.session.commit()

    return '', 204


parser = argparse.ArgumentParser(description='bullstamp_backend.')
parser.add_argument('--port', type=int, default=5557)
if __name__ == '__main__':
    args = parser.parse_args()
    # app.run(host='0.0.0.0', port=args.port, threaded=True)
    event_manager.start()
    socketio.run(app, host='0.0.0.0', port=args.port)
