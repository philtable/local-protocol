#!/usr/bin/env bash

# set up bash to handle errors more aggressively - a "strict mode" of sorts
set -e # give an error if any command finishes with a non-zero exit code
set -u # give an error if we reference unset variables
set -o pipefail # for a pipeline, if any of the commands fail with a non-zero exit code, fail the entire pipeline with that exit code

pushd ~/backend

git reset --hard
git clean -fd
git fetch origin
git reset --hard origin/master

python3.6 -m pip install -r requirements.txt

tmux kill-session -t backend || true # kill old tmux session, if present
tmux new-session -d -s backend 'flask db upgrade && python3.6 server.py >> /var/log/bullstamp.log 2>&1' # start a new tmux session, but don't attach to it just yet

popd

echo 'BULLSTAMP BACKEND SERVER STARTED, MONITOR WITH tmux attach -t backend'
