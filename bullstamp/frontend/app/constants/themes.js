// Use https://material.io/tools/color/#!/ for nice contrasting colors :)

export default {
  main: {
    primaryColor: '#61CA9D',
    primaryColorDark: '#529B80',
    primaryColorLight: '#A3DDC5',
  },
};
