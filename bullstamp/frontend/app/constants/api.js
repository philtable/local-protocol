export const LAUNCH_DATE = 1532497467;

// Development
//export const MANAGEDEOS_ENDPOINT = 'http://127.0.0.1:8080';
//export const backend = 'http://localhost:5557';

// Production
export const MANAGEDEOS_ENDPOINT = 'https://managedeos.apptoken.co';
export const backend = 'https://api.bullstamp.co';
