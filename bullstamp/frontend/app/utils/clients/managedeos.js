import { TextEncoder, TextDecoder } from 'text-encoding';
import * as eosjs from 'eosjs';
import { authenticateBullstamp, getSelf } from '../api';
import { EOSClient } from './client';
import { MANAGEDEOS_ENDPOINT } from '../../constants/api';

function url(v) {
  return `${MANAGEDEOS_ENDPOINT}${v}`;
}

export class ManagedEOSClient extends EOSClient {
  constructor(config, loginCallback, setUsernameCallback, verifyEmailCallback, noUsernameCallback) {
    super('managedeos', config, loginCallback);
    this.setUsernameCallback = setUsernameCallback;
    this.verifyEmailCallback = verifyEmailCallback;
    this.noUsernameCallback = noUsernameCallback;

    this.rpc = new eosjs.Rpc.JsonRpc(config.api_endpoint, { fetch });
    this.eos = new eosjs.Api({
      rpc: this.rpc,
      chainId: this.config.chain_id,
      signatureProvider: this,
      textDecoder: new TextDecoder(),
      textEncoder: new TextEncoder(),
    });

    this.doLogin();
  }

  getUsername = () => this.self.username;

  isAdmin = () => this.self.admin;

  doLogin = () => {
    getSelf()
      .then(r => {
        if (r.username) {
          this.self = r;
          this.loginWithBullstamp();
        } else if (r.email_verified) {
          this.setUsernameCallback(r);
        } else if (r.beta_user) {
          this.verifyEmailCallback();
        } else {
          console.log(r)
          this.noUsernameCallback();
        }
      })
      .catch(e => {
        console.log('[managedeos] failed to load self', e);
      });
  };

  signPlaintext = msg =>
    fetch(url('/api/v1/sign_plaintext'), {
      method: 'POST',
      headers: {
        Authorization: localStorage.getItem('authorization'),
      },
      body: JSON.stringify({ message: JSON.stringify(msg) }),
    })
      .then(response => response.json())
      .then(response => {
        if (response.ok) return response;
        throw new Error(JSON.stringify(response));
      });

  signTransaction = args =>
    fetch(url('/api/v1/sign_transaction'), {
      method: 'POST',
      headers: {
        Authorization: localStorage.getItem('authorization'),
      },
      body: JSON.stringify(args, (k, v) => {
        if (v instanceof Uint8Array) {
          return Array.apply([], v);
        }
        return v;
      }),
    })
      .then(response => response.json())
      .then(response => {
        if (response.ok) return response;
        throw new Error(JSON.stringify(response));
      });

  authenticate = () => {
    this.signPlaintext({
      username: this.getUsername(),
      authority: 'active',
      timestamp: new Date(),
    })
      .then(result => {
        authenticateBullstamp(result.message, result.signature)
          .then(() => {
            this.loginWithBullstamp();
          })
          .catch(err => {
            alert('failed to authenticate');
            console.log('failed to authenticate', err);
          });
      })
      .catch(err => {
        console.log('failed to request signature', err);
        alert('failed to request signature');
      });
  };

  getAvailableKeys = () => Promise.resolve([this.self.public_key]);

  sign = args => this.signTransaction(args).then(r => r.signed_tx.signatures);
}
