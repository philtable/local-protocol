import { TextEncoder, TextDecoder } from 'text-encoding';
import * as eosjs from 'eosjs';
import ScatterJS from 'scatterjs-core';
import { authenticateBullstamp } from '../api';

import { EOSClient } from './client';

export class ScatterClassicClient extends EOSClient {
  constructor(config, loginCallback, scatterclassicAvailable) {
    super('scatter-classic', config, loginCallback);
    this.scatterclassicAvailable = scatterclassicAvailable;

    const apiURL = new URL(config.api_endpoint);
    this.network = {
      blockchain: 'eos',
      host: apiURL.host,
      port: parseInt(apiURL.port, 10) || (apiURL.protocol === 'https:' ? 443 : 80),
      protocol: apiURL.protocol.substring(0, apiURL.protocol.length - 1),
      chainId: this.config.chain_id,
    };

    console.log('Connecting scatter', this.network);
    ScatterJS.scatter
      .connect(
        'Bullstamp',
        { initTimeout: 10000 },
      )
      .then(connected => {
        console.log('Scatter connection', connected);
        if (!connected) {
          // User does not have Scatter installed/unlocked.
          return false;
        }

        const { scatter } = ScatterJS;

        // only null out in production, otherwise hot reloads are fucked
        if (process.env.NODE_ENV === 'production') {
          window.scatter = undefined;
          ScatterJS.scatter = undefined;
        }

        this.scatter = scatter;
        this.scatter.eosNetwork = this.network; // todo(mitsui): remove

        this.rpc = new eosjs.Rpc.JsonRpc(this.config.api_endpoint, { fetch });
        this.fake_eos = this.scatter.eos(this.network, args => {
          if (args.signProvider) {
            this.signatureProvider = args.signProvider;
          }
          return { fake: () => Promise.resolve(true) };
        });
        this.fake_eos.fake();

        this.eos = new eosjs.Api({
          rpc: this.rpc,
          chainId: this.config.chain_id,
          signatureProvider: this,
          authorityProvider: this,
          textDecoder: new TextDecoder(),
          textEncoder: new TextEncoder(),
        });

        // begin login
        if (localStorage.getItem('last-login-type') === 'scatter-classic') {
          this.doLogin();
        } else {
          this.loginFailed();
        }

        return Promise.resolve(true);
      });
  }

  getUsername = () => this.scatter.identity.accounts[0].name;

  getAuthority = () => this.scatter.identity.accounts[0].authority;

  doLogin = () => {
    console.log('[scatterclassic] doing login', this.network);
    return this.scatter
      .getIdentity({
        accounts: [this.network],
      })
      .then(() => {
        this.loginWithBullstamp();
      })
      .catch(e => {
        console.log('[scatterclassic] login failed', e);
        this.loginFailed();
      });
  };

  loginFailed = () => {
    this.scatterclassicAvailable(this.scatter);
  };

  authenticate = () => {
    const username = this.getUsername();
    const authority = this.getAuthority();

    console.log(
      '[scatterclassic] attempting to get signature for authenticatication',
    );
    this.rpc.get_account(username).then(a => {
      const perm = a.permissions.find(p => p.perm_name === authority);
      if (!perm) {
        console.log('couldn\'t find corresponding authority. weird');
        this.loginFailed();
        return;
      }

      const message = JSON.stringify({
        username,
        authority,
        timestamp: new Date(),
      });

      const tryKey = i => {
        if (i >= perm.required_auth.keys.length) {
          console.log('[scatterclassic] no more keys to try');
          this.loginFailed();
          return;
        }

        const { key } = perm.required_auth.keys[i];
        console.log('[scatterclassic] trying key', { key });
        this.scatter
          .getArbitrarySignature(key, message, 'Log in', false)
          .then(t => {
            console.log(message);
            console.log('scatter sign', t);
            authenticateBullstamp(message, t)
              .then(() => {
                this.loginWithBullstamp();
              })
              .catch(err => {
                alert('failed to authenticate');
                console.log('failed to authenticate', err);
              });
          })
          .catch(() => {
            console.log('[scatterclassic] failed to sign using key', { key });
            tryKey(i + 1);
          });
      };

      tryKey(0);
    });
  };

  // so fucking ugly
  // workaround to make eosjs2 work with scatter classic
  getAvailableKeys = () => Promise.resolve([]);

  // sneaky way to get the pre-serialized transaction
  getRequiredKeys = args => args;

  sign = args =>
    this.signatureProvider({
      transaction: args.requiredKeys.transaction,
      buf: Buffer.concat([
        Buffer.from(this.config.chain_id, 'hex'),
        Buffer.from(args.serializedTransaction),
        Buffer.from(new Uint8Array(32)),
      ]),
    });
}
