import { checkBullstampAuthentication } from '../api';

export class EOSClient {
  constructor(clientType, config, loginCallback) {
    this.clientType = clientType;
    this.config = config;
    this.loginCallback = loginCallback;
    this.eos = undefined;
    this.rpc = undefined;
  }

  getUsername = () => {
    throw new Error('please implement getUsername');
  };

  loginWithBullstamp = () => {
    const expectedUsername = this.getUsername();
    console.log(
      'authenticating with bullstamp backend, expecting username',
      expectedUsername,
    );
    checkBullstampAuthentication()
      .then(res => {
        if (res === expectedUsername) {
          // all set
          localStorage.setItem('last-login-user', expectedUsername);
          localStorage.setItem('last-login-type', this.clientType);
          this.loginCallback(this.clientType, expectedUsername);
          return;
        }

        this.authenticate();
      })
      .catch(() => {
        this.authenticate();
      });
  };

  authenticate = () => {
    throw new Error('please implement authenticate');
  };

  stamp = (content, replyTo) =>
    this.eos.transact(
      {
        actions: [
          {
            account: this.config.app_account,
            name: 'stamp',
            authorization: [
              {
                actor: this.getUsername(),
                permission: 'active',
              },
            ],
            data: {
              content,
              reply_to: replyTo || 'null',
            },
          },
        ],
      },
      {
        // todo(mitsui): are these values ok
        blocksBehind: 3,
        expireSeconds: 30,
      },
    );

  avatar = url =>
    this.eos.transact(
      {
        actions: [
          {
            account: this.config.app_account,
            name: 'avatar',
            authorization: [
              {
                actor: this.getUsername(),
                permission: 'active',
              },
            ],
            data: {
              url,
            },
          },
        ],
      },
      {
        // todo(mitsui): are these values ok
        blocksBehind: 3,
        expireSeconds: 30,
      },
    );

  tip = (author, id) =>
    this.eos.transact(
      {
        actions: [
          {
            account: this.config.token_account,
            name: 'transfer',
            authorization: [
              {
                actor: this.getUsername(),
                permission: 'active',
              },
            ],
            data: {
              from: this.getUsername(),
              to: author,
              quantity: '1.0000 APP',
              memo: `tip:${id}`,
            },
          },
        ],
      },
      {
        // todo(mitsui): are these values ok
        blocksBehind: 3,
        expireSeconds: 30,
      },
    );

  getBalance = () => {
    if (!this.rpc) return Promise.resolve('0.0000 APP');

    return this.rpc
      .get_table_rows({
        json: true,
        code: this.config.token_account,
        scope: this.getUsername(),
        table: 'accounts',
        limit: 5000,
      })
      .then(res => res.rows.map(row => row.balance))
      .then(balances => balances[0] || '0.0000 APP');
  };
}
