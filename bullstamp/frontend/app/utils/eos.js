export function isOutOfCpu(e) {
  return e.error && e.error.name === 'tx_cpu_usage_exceeded';
}

export function isOutOfNet(e) {
  return e.error && e.error.name === 'tx_net_usage_exceeded';
}

export function isOutOfRam(e) {
  return e.error && e.error.name === 'ram_usage_exceeded';
}

export function handleError(e) {
  if (e.error === 'eos_error') e = e.response;

  if (isOutOfCpu(e)) {
    alert(
      "You don't have enough EOS staked in CPU! Stake some more EOS at https://toolkit.genereos.io/account/delegate",
    );
  } else if (isOutOfNet(e)) {
    alert(
      "You don't have enough EOS staked in net! Stake some more EOS at https://toolkit.genereos.io/account/delegate",
    );
  } else if (isOutOfRam(e)) {
    alert(
      "You don't have enough EOS staked in ram! Stake some more EOS at https://toolkit.genereos.io/account/delegate",
    );
  } else {
    alert(`error: ${JSON.stringify(e)}`);
  }
}
