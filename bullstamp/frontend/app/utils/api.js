import { MANAGEDEOS_ENDPOINT, backend } from '../constants/api';

function refreshAuthorization(req) {
  console.log(req);
  const newHeader = req.getResponseHeader('Set-Authorization');
  if (newHeader) {
    console.log(`Refeshing authorization: ${newHeader}`);
    localStorage.setItem('authorization', newHeader);
  }
}

function managedeosRequest(type, url) {
  const req = new XMLHttpRequest();
  req.open(type, `${MANAGEDEOS_ENDPOINT}${url}`, type === 'POST');
  const header = localStorage.getItem('authorization');
  if (header) {
    req.setRequestHeader('Authorization', header);
  }
  if (type === 'POST') {
    req.setRequestHeader('Content-Type', 'application/json');
  }
  return req;
}

export function register(blob) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', '/api/v1/register');
    req.setRequestHeader('Content-Type', 'application/json');
    req.addEventListener('load', function handleLoad() {
      refreshAuthorization(this);
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify(blob));
  });
}

export function login(blob) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', '/api/v1/login');
    req.setRequestHeader('Authorization', undefined);
    req.addEventListener('load', function handleLoad() {
      refreshAuthorization(this);
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify(blob));
  });
}

export function uploadAvatar(file, author) {
  const formData = new FormData();
  formData.append(author, file);
  return fetch(new Request(`${backend}/authors/${author}/avatar`), {
    method: 'POST',
    credentials: 'include',
    body: formData,
  });
}

export function getAvatar(author) {
  return fetch(new Request(`${backend}/authors/${author}/avatar`)).then(res =>
    res.json(),
  );
}

export function getSelf() {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('GET', '/api/v1/users/self');
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.user);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function getUsers() {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('GET', '/api/v1/users');
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function updateUserBeta(id, beta_user) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', `/api/v1/users/${id}/beta_user`);
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ beta_user }));
  });
}

export function updateUserUsername(id, username) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', `/api/v1/users/${id}/username`);
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ username }));
  });
}

export function requestPasswordReset(email) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', '/api/v1/request_password_reset');
    req.setRequestHeader('Content-Type', 'application/json');
    req.addEventListener('load', function handleLoad() {
      refreshAuthorization(this);
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ email }));
  });
}

export function resetPassword(id, newPassword) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open(
      'POST',
      `${MANAGEDEOS_ENDPOINT}/api/v1/users/${id}/reset_password`,
      true,
    );
    req.setRequestHeader('Content-Type', 'application/json');
    req.withCredentials = true;
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ password: newPassword }));
  });
}

export function setUsername(username) {
  return new Promise((resolve, reject) => {
    const req = managedeosRequest('POST', '/api/v1/createaccount');
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ username }));
  });
}

export function getStamps(page) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', `${backend}/stamps?page=${page}`, true);
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        resolve(result);
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function getTrendingStamps(page) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', `${backend}/trending?page=${page}`, true);
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        resolve(result);
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function getStamp(id: string) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', `${backend}/stamps/${id}`, true);
    req.addEventListener('load', function handleLoad() {
      try {
        const resp = JSON.parse(this.responseText);
        if (resp.error) {
          reject(new Error(resp.error));
        } else {
          resolve(resp);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function getReplies(id: string, page: number) {
  return fetch(
    new Request(`${backend}/stamps/${id}/replies?page=${page}`),
  ).then(res => res.json());
}

export function getStampsByAuthor(id: string, page: number) {
  return fetch(
    new Request(`${backend}/authors/${id}/stamps?page=${page}`),
  ).then(res => res.json());
}

export function getTipsByCreator(id: string, page: number) {
  return fetch(new Request(`${backend}/${id}/tips?page=${page}`)).then(res =>
    res.json(),
  );
}

export function getTipsByStamp(id: string) {
  return fetch(
    new Request(`${backend}/stamps/${id}/tips`),
  ).then(res => res.json());
}

export function getNotifications(author, page) {
  return fetch(
    new Request(`${backend}/notifications?page=${page}`, {
      credentials: 'include',
    }),
  ).then(res => res.json());
}

export function markNotificationsRead() {
  document.dispatchEvent(new Event('notificationsRead'));
  return fetch(new Request(`${backend}/notifications`), {
    method: 'POST',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    body: JSON.stringify({}),
  });
}

export function followUser(user_to_follow) {
  return fetch(new Request(`${backend}/follow`), {
    method: 'POST',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    body: JSON.stringify({ user_to_follow }),
  });
}

export function unfollowUser(user_to_follow) {
  return fetch(new Request(`${backend}/unfollow`), {
    method: 'POST',
    credentials: 'include',
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    body: JSON.stringify({ user_to_follow }),
  });
}

export function getFollowers(author: string, page: number) {
  return fetch(
    new Request(`${backend}/${author}/followers?page=${page}`),
  ).then(res => res.json());
}

export function getFollowing(author: string, page: number) {
  return fetch(
    new Request(`${backend}/${author}/following?page=${page}`),
  ).then(res => res.json());
}

export function getUserFollowingTargetUser(user: string, target_user: string) {
  return fetch(
    new Request(`${backend}/${user}/following/${target_user}`),
  ).then(res => res.json());
}

export function checkBullstampAuthentication() {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('GET', `${backend}/me`, true);
    req.withCredentials = true;
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve(result.username);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send();
  });
}

export function authenticateBullstamp(message, signature) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('POST', `${backend}/login`, true);
    req.withCredentials = true;
    req.setRequestHeader('Content-Type', 'application/json');
    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        if (result.ok) {
          resolve();
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    req.send(JSON.stringify({ message, signature }));
  });
}
