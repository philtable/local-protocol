import io from 'socket.io-client';

export class WebSocketClient {
  constructor(username, handleMessage) {
    this.handleMessage = handleMessage;
    this.connect(username);

    window.push = this.handleMessage;
  }

  connect = (username) => {
    this.ws = io('https://api.bullstamp.co');

    this.ws.on('connect', () => {
      this.ws.emit('user', username);
    });
    this.ws.on('disconnect', () => {
      setTimeout(() => {
        this.connect();
      }, 1);
    });
    this.ws.on('event', this.handleMessage);
  };
}
