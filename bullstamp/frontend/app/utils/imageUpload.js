// TODO remove this
export function uploadFileToImgur(file) {
  return new Promise((resolve, reject) => {
    const req = new XMLHttpRequest();
    req.open('POST', 'https://api.imgur.com/3/image');
    req.setRequestHeader('Authorization', 'Client-ID 63cc3103e2172dd');
    req.setRequestHeader('Content-Type', 'application/json');

    req.addEventListener('load', function handleLoad() {
      try {
        const result = JSON.parse(this.responseText);
        console.log(result);
        if (result.success) {
          resolve(result);
        } else {
          reject(result);
        }
      } catch (err) {
        reject(err);
      }
    });
    req.addEventListener('error', evt => {
      reject(evt);
    });

    // File must be in base64.
    req.send(JSON.stringify({image: file, type: 'base64'}));
  });
}