/*
 * RegisterPage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import {setUsername} from '../../utils/api';
import Button from '../../components/BigButton';
import logo from '../../images/bullstamplogo.png';
import jumbo from '../../images/jumbo.png';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PageContainer = styled.div`
  height: 100vh;
  background-color: #458f70;
`;

const ContentContainer = styled.div`
  background-image: url(${jumbo});
  background-size: cover;
  padding-top: 100px;
  padding-bottom: 300px;
`;

const TextContainer = styled.div`
  color: white;
  padding-left: 10%;
`;

const LoginContainer = styled.div`
  :hover {
    cursor: ${'pointer'};
  }
  width: 300px;
  height: 50px;
`;

const UsernameInput = styled.input`
  padding: 0.5em;
  margin: 0.5em 0em 0.5em 0em;
  width: 240px;
  color: black;
  background: white;
`;

const CharacterCountInput = styled.input`
  padding: 0.5em;
  margin: 0.5em 0em 0.5em 0em;
  width: 60px;
  color: black;
  background: white;
`;

/* eslint-disable react/prefer-stateless-function */
export default class SetUsernamePage extends React.Component {
  state = {
    username: '',
  };

  render() {
    return (
      <PageContainer>

        <ToastContainer />

        <ContentContainer>
          <TextContainer>
            <h1>
              <img src={logo} width="80px" height="80px"/>BullStamp
            </h1>
            <h3>Set Username</h3>
            <h5>Username must contain exactly 12 characters <br /> and can only contain a-z (no uppercase) or 1-5 <br /> due to blockchain account creation rules.</h5>
            <form onSubmit={this.handleSubmit}>
              <label>
                <UsernameInput
                  type="text"
                  placeholder="Exactly 12 char, a-z or 1-5"
                  value={this.state.username}
                  onChange={event =>
                    this.setState({ username: event.target.value })
                  }
                  pattern={"[a-z1-5]{12}"}
                  maxLength="12"
                />
                <CharacterCountInput
                  type="text"
                  value={this.state.username.length + "/12"}
                  disabled
                />
              </label>
              <br />
              <Button type="submit" text="Create Account">
              </Button>
            </form>
          </TextContainer>
        </ContentContainer>
      </PageContainer>
    );
  }

  handleSubmit = event => {
    event.preventDefault();
    const usernameChosen = this.state.username;

    setUsername(usernameChosen)
      .then(() => {
        this.props.onLogin(usernameChosen, false, false);
      })
      .catch(e => {
        toast.error(`An error occurred while setting your username: ${JSON.stringify(e, null, " ")}`, {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 5000,
          hideProgressBar: true,
        })
      });
  };
}
