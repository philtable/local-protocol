/*
 * The post detail page with
 */

import React from 'react';
import styled from 'styled-components';
import Button from 'components/Button';
import ProfileSummaryCard from 'components/ProfileSummaryCard';
import PostContents from 'components/PostContents';
import RulesCard from 'components/RulesCard';
import DiscordCard from 'components/DiscordCard';
import Card from 'components/Card';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import Themes from 'constants/themes';
import Spinner from 'components/Spinner';
import InfiniteScroll from 'react-infinite-scroller';
import { getReplies, getStamp } from '../../utils/api';
import { handleError } from '../../utils/eos';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -60px;
`;

const StyledProfileSummaryCard = styled(ProfileSummaryCard)`
  margin-right: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledDiscordCard = styled(DiscordCard)`
  margin-right: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledRulesCard = styled(RulesCard)`
  margin-left: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledCard = styled(Card)`
  margin-top: 12px;
  display: flex;
  flex-direction: column;
`;

const MainPostContents = styled(PostContents)`
  padding-top: 14px;
  padding-bottom: 14px;
`;

const Divider = styled.div`
  align-self: stretch;
  background-color: #e8e8e8;
  height: 1px;
  margin-top: 8px;
  margin-bottom: 8px;
  margin-left: -16px;
  margin-right: -16px;
`;

const ReplyComposerContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
`;

const TextArea = styled.textarea`
  border-width: ${props => (props.isFocused ? '2px' : '1px')};
  border-style: solid;
  border-color: ${props =>
    props.isFocused ? props.theme.primaryColorDark : '#dcdcdc'};
  border-radius: 6px;
  margin-top: 10px;
  outline: none;
  padding-left: ${props => (props.isFocused ? '7px' : '8px')};
  padding-right: ${props => (props.isFocused ? '7px' : '8px')};
  padding-top: ${props => (props.isFocused ? '5px' : '6px')};
  padding-bottom: ${props => (props.isFocused ? '5px' : '6px')};
  color: ${props => (props.disabled ? '#dcdcdc' : 'initial')};
`;

TextArea.defaultProps = {
  theme: Themes.main,
};

const StyledButton = styled(Button)`
  margin-left: 14px;
`;

const HorizontalContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-top: 12px;
`;

const RepliesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

const NoRepliesContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

const NameSpan = styled.span`
  font-weight: bold;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const MainSpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 40px;
  margin-bottom: 40px;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

export default class PostPage extends React.Component {
  state = {
    replies: [],
    hasMore: true,
    pageIsLoading: false,
    message: '',
    isFocused: false,
    isLoading: false,
    isMainStampLoading: true,
    mainStamp: null,
  };

  componentWillMount() {
    this.props.changeTab("none");
    // When stamp is replied to, add reply to stamp page
    this.listenerId = this.props.api.addStampListener(newStamp => {
      // Ensure new stamp is actually a reply to the current stamp
      if (newStamp.reply_to !== this.state.mainStamp.transaction_id) {
        return;
      }

      // Store the reply
      this.setState(prevState => {
        const newReplies = prevState.replies.slice(0);
        newReplies.push(newStamp);
        return {
          replies: newReplies,
          isLoading: false,
          message: '',
        };
      });
    });
  }

  componentWillUnmount() {
    this.props.api.removeStampListener(this.listenerId);
  }

  componentDidMount() {
    this._loadmainStamp();
  }

  _onTextAreaChange = event => {
    this.setState({
      message: event.target.value,
    });
  };

  _onTextAreaFocus = event => {
    this.setState({
      isFocused: true,
    });
  };

  _onTextAreaBlur = event => {
    this.setState({
      isFocused: false,
    });
  };

  _onPost = () => {
    this.setState({ isLoading: true });

    const message = this.state.message;

    this.props.api
      .reply(message, this.state.mainStamp.transaction_id)
      .catch(e => {
        handleError(e);
        this.setState({ isLoading: false, message: '' });
      });
  };

  _loadPage = page => {
    this.setState({pageIsLoading: true})
    getReplies(this.props.match.params.stampId, page)
      .then(json => {
        this.setState(prevState => ({
          replies: prevState.replies.concat(json.replies),
          hasMore: json.page_info.has_next_page,
        }));
        this.setState({pageIsLoading: false})
      })
      .catch(error => {
        this.setState({pageIsLoading: false})
        console.error(error);
      });
  };

  _loadmainStamp() {
    getStamp(this.props.match.params.stampId)
      .then(json => {
        this.setState(prevState => ({
          mainStamp: json,
          isMainStampLoading: false,
        }));
      })
      .catch(error => {
        console.error(error);
      });
  }

  _renderReplies() {
    return (
      <InfiniteScroll
        pageStart={0}
        loadMore={this._loadPage}
        hasMore={this.state.hasMore && !this.state.pageIsLoading}
        loader={
          <SpinnerContainer key={0}>
            <Spinner />
          </SpinnerContainer>
        }
      >
        {this.state.replies.map((stamp, i) => (
          <div key={stamp.transaction_id}>
            {i !== 0 ? <Divider /> : null}
            <PostContents
              api={this.props.api}
              showReplyHint={false}
              replyingTo={this.state.mainStamp.author}
              stamp={stamp}
            />
          </div>
        ))}
      </InfiniteScroll>
    );
  }

  _renderRepliesContainer() {
    if (this.state.isMainStampLoading) {
      return null;
    }
    return (
      <RepliesContainer>
        {!this.state.hasMore && this.state.replies.length === 0 ? (
          <NoRepliesContainer>No replies yet!</NoRepliesContainer>
        ) : (
          this._renderReplies()
        )}
      </RepliesContainer>
    );
  }

  render() {
    return (
      <div>
        <NavBar api={this.props.api} currentTab={this.props.currentTab} />
        <Banner />
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            {this.props.api.isLoggedIn ? (
              <StyledProfileSummaryCard
                username={this.props.api.getUsername()}
                api={this.props.api}
              />
            ) : (
              <StyledDiscordCard />
            )}
            <FeedContainer>
              <StyledCard>
                {this.state.isMainStampLoading ? (
                  <MainSpinnerContainer>
                    <Spinner />
                  </MainSpinnerContainer>
                ) : (
                  <MainPostContents
                    showReplyHint={false}
                    stamp={this.state.mainStamp}
                    api={this.props.api}
                  />
                )}
                <Divider />
                {!this.props.api.isLoggedIn ? null : (
                  <ReplyComposerContainer>
                    <NameSpan>@{this.props.api.getUsername()}</NameSpan>
                    <TextArea
                      disabled={this.state.isLoading}
                      isFocused={this.state.isFocused}
                      onBlur={this._onTextAreaBlur}
                      onChange={this._onTextAreaChange}
                      onFocus={this._onTextAreaFocus}
                      placeholder="Write a reply!"
                      rows={2}
                      value={this.state.message}
                    />
                    <HorizontalContainer>
                      {<div>{this.state.message.length}/280</div>}
                      <span>&nbsp;&nbsp;&nbsp;</span>
                      {this.state.isLoading ? <Spinner /> : null}
                      <StyledButton
                        isDisabled={
                          this.state.isLoading ||
                          this.state.message.length === 0 ||
                          this.state.message.length > 280
                        }
                        text="Reply"
                        onClick={this._onPost}
                      />
                    </HorizontalContainer>
                    <Divider />
                  </ReplyComposerContainer>
                )}
                {this._renderRepliesContainer()}
              </StyledCard>
              {!this.state.isMainStampLoading &&
              !this.state.hasMore &&
              this.state.replies.length > 0 ? (
                <FooterContainer>That is it folks! 🐂</FooterContainer>
              ) : null}
            </FeedContainer>
            <StyledRulesCard />
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
