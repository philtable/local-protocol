/*
 * The post detail page with
 */

import React from 'react';
import styled from 'styled-components';
import ProfileSummaryCard from 'components/ProfileSummaryCard';
import PostContents from 'components/PostContents';
import RulesCard from 'components/RulesCard';
import Card from 'components/Card';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import Themes from 'constants/themes';
import Spinner from 'components/Spinner';
import InfiniteScroll from 'react-infinite-scroller';
import { getNotifications, markNotificationsRead } from '../../utils/api';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -60px;
`;

const StyledProfileSummaryCard = styled(ProfileSummaryCard)`
  margin-right: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const StyledRulesCard = styled(RulesCard)`
  margin-left: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const StyledCard = styled(Card)`
  margin-top: 12px;
  display: flex;
  flex-direction: column;
`;

const Divider = styled.div`
  align-self: stretch;
  background-color: #e8e8e8;
  height: 1px;
  margin-top: 8px;
  margin-bottom: 8px;
  margin-left: -16px;
  margin-right: -16px;
`;

const TextArea = styled.textarea`
  border-width: ${props => (props.isFocused ? '2px' : '1px')};
  border-style: solid;
  border-color: ${props =>
  props.isFocused ? props.theme.primaryColorDark : '#dcdcdc'};
  border-radius: 6px;
  margin-top: 10px;
  outline: none;
  padding-left: ${props => (props.isFocused ? '7px' : '8px')};
  padding-right: ${props => (props.isFocused ? '7px' : '8px')};
  padding-top: ${props => (props.isFocused ? '5px' : '6px')};
  padding-bottom: ${props => (props.isFocused ? '5px' : '6px')};
  color: ${props => (props.disabled ? '#dcdcdc' : 'initial')};
`;

TextArea.defaultProps = {
  theme: Themes.main,
};

const RepliesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

const NoRepliesContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

const RenderReplyNotification = ({ api, history, notif, index }) => (
  <div onClick={() => history.push(`/stamp/${notif.parent_stamp.transaction_id}`)}>
    {index !== 0 ? <Divider/> : null}
    <PostContents api={api} showReplyHint={false} replyingTo={notif.parent_stamp.author} stamp={notif.stamp}/>
  </div>
);

const RenderTipNotification = ({ api, history, notif, index }) =>
  notif.tip.to.reply_to === null ? (
    <div onClick={() => history.push(`/stamp/${notif.tip.to.transaction_id}`)}>
      {index !== 0 ? <Divider/> : null}
      <PostContents api={api} stamp={notif.tip.to} tip={notif.tip}/>
    </div>
  ) : (
    <div onClick={() => history.push(`/stamp/${notif.tip.to.reply_to}`)}>
      {index !== 0 ? <Divider/> : null}
      <PostContents api={api} stamp={notif.tip.to} tip={notif.tip}/>
    </div>
  );

const RenderNotification = ({
                              api,
                              history,
                              hasMore,
                              pageIsLoading,
                              loadPage,
                              notifications,
                            }) => (
  <InfiniteScroll
    pageStart={0}
    loadMore={loadPage}
    hasMore={hasMore && !pageIsLoading}
    loader={
      <SpinnerContainer key={0}>
        <Spinner/>
      </SpinnerContainer>
    }
  >
    {notifications.map((notif, i) => {
      switch (notif.action_type) {
        case 'STAMP':
          return <RenderReplyNotification key={i} api={api} history={history} notif={notif} index={i}/>;
        case 'TIP':
          return <RenderTipNotification key={i} api={api} history={history} notif={notif} index={i}/>;
        default:
          console.error('Unknown notification type', notif);
          break;
      }
    })}
  </InfiniteScroll>
);

const RenderNotifications = ({
                               api,
                               history,
                               hasMore,
                               pageIsLoading,
                               notifications,
                               loadPage,
                             }) => (
  <RepliesContainer>
    {!api.isLoggedIn || (!hasMore && notifications.length === 0) ? (
      <NoRepliesContainer>No new notifications!</NoRepliesContainer>
    ) : (
      <RenderNotification
        api={api}
        history={history}
        hasMore={hasMore}
        pageIsLoading={pageIsLoading}
        notifications={notifications}
        loadPage={loadPage}
      />
    )}
  </RepliesContainer>
);

export default class NotificationPage extends React.Component {
  state = {
    notifications: [],
    hasMore: true,
    pageIsLoading: false,
  };

  componentWillMount() {
    this.props.changeTab("none");
    document.addEventListener('renderNotification', this.handleNewNotification);
  }

  componentWillUnmount() {
    document.removeEventListener(
      'renderNotification',
      this.handleNewNotification,
    );
  }

  // marknotif hides the bell
  handleNewNotification = evt => {
    this.setState(oldState => {
      const oldNotifications = oldState.notifications;
      oldNotifications.unshift(evt.data);
      return {
        notifications: oldNotifications,
      };
    });

    evt.preventDefault();
  };

// pageIsLoading ensures loadPage is called one page at a time
  loadPage = page => {
    this.setState({pageIsLoading: true})
    const myUsername = this.props.api.getUsername();
    getNotifications(myUsername, page)
      .then(json => {
        this.setState(oldState => {
          return {
            notifications: oldState.notifications.concat(json.notifications),
            hasMore: json.page_info.has_next_page,
          };
        });
        markNotificationsRead(myUsername);
        this.setState({pageIsLoading: false})
      })
      .catch(error => {
        console.error('Get notifications error', error);
        this.setState({pageIsLoading: false})
      });
  };

  componentDidMount() {
    markNotificationsRead();
  }

  render() {
    const { hasMore, pageIsLoading, notifications } = this.state;
    const { api, history, currentTab } = this.props;
    return (
      <div>
        <NavBar api={api} currentTab={currentTab}/>
        <Banner/>
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            <StyledProfileSummaryCard api={api} username={api.getUsername()}/>
            <FeedContainer>
              <StyledCard>
                <b>Notifications</b>
              </StyledCard>
              <StyledCard>
                <RenderNotifications
                  api={api}
                  hasMore={hasMore}
                  pageIsLoading={pageIsLoading}
                  history={history}
                  loadPage={this.loadPage}
                  notifications={notifications}
                />
              </StyledCard>
              {!hasMore && notifications.length === 0 ? (
                <FooterContainer>That is it folks!</FooterContainer>
              ) : null}
            </FeedContainer>
            <StyledRulesCard/>
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
