/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { FormattedMessage } from 'react-intl';
import Logo from '../../images/bullstamplogo.png';
import messages from './messages';

const ContentContainer = styled.div`
  text-align: center;
`;

/* eslint-disable react/prefer-stateless-function */
export default class NotFound extends React.PureComponent {
  render() {
    return (
      <ContentContainer>
        <Link to={{ pathname: `/home` }}>
          <img src={Logo} alt="Bullstamp Logo" width="500" height="500" />
        </Link>
        <h1>404: Not Found</h1>
        <h2>
          <FormattedMessage {...messages.header} />
        </h2>
      </ContentContainer>
    );
  }
}
