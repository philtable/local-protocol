/*
 * RequestPasswordResetPage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import { requestPasswordReset } from '../../utils/api';
import Button from '../../components/BigButton';
import logo from '../../images/bullstamplogo.png';
import jumbo from '../../images/jumbo.png';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const PageContainer = styled.div`
  height: 100vh;
  background-color: #458f70;
`;

const ContentContainer = styled.div`
  background-image: url(${jumbo});
  background-size: cover;
  padding-top: 100px;
  padding-bottom: 300px;
`;

const TextContainer = styled.div`
  color: white;
  padding-left: 10%;
`;

const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em 0em 0.5em 0em;
  width: 300px;
  color: black;
  background: white;
  border: none;
  border-radius: 3px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class RequestPasswordResetPage extends React.Component {
  state = {
    email: '',
  };

  render() {
    return (
      <PageContainer>

        <ToastContainer />

        <ContentContainer>
          <TextContainer>
          <h1 onClick={() => this.props.history.push('/')}>
            <img
              src={logo}
              width="80px"
              height="80px"
            />BullStamp
          </h1>
            <h3>Request Password Reset</h3>
            <form onSubmit={this.handleSubmit}>
              <label>
                <Input
                  type="text"
                  placeholder="Email Address"
                  value={this.state.email}
                  onChange={event =>
                    this.setState({ email: event.target.value })
                  }
                />
              </label>
              <br />
              <Button type="submit" text="Request" isDisabled={!this.isValid()}>
              </Button>
            </form>
          </TextContainer>
        </ContentContainer>
      </PageContainer>
    );
  }

  isValid = () => {
    if (!this.state.email) return false;
    return true;
  };

  handleSubmit = event => {
    event.preventDefault();

    if (!this.state.email) {
      toast.error("No email specified!", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 3000,
        hideProgressBar: true,
      });
      return;
    }

    requestPasswordReset(this.state.email)
      .then(success => {
        toast.success('If the email you provided was valid, your password reset request has been submitted', {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 5000,
          hideProgressBar: true,
        });
      })
      .catch(error => {
        toast.success('If the email you provided was valid, your password reset request has been submitted', {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 5000,
          hideProgressBar: true,
        });
      });
  };
}
