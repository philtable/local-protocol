/*
 * RegisterPage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import { login } from '../../utils/api';
import Button from '../../components/BigButton';
import logo from '../../images/bullstamplogo.png';
import jumbo from '../../images/jumbo.png';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PageContainer = styled.div`
  height: 100vh;
  background-color: #458f70;
`;

const ContentContainer = styled.div`
  background-image: url(${jumbo});
  background-size: cover;
  padding-top: 100px;
  padding-bottom: 300px;
`;

const TextContainer = styled.div`
  color: white;
  padding-left: 10%;
`;

const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em 0em 0.5em 0em;
  width: 300px;
  color: black;
  background: white;
  border: none;
  border-radius: 3px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class LoginPage extends React.Component {
  state = {
    email: '',
    password: '',
  };

  render() {
    return (
      <PageContainer>

        <ToastContainer />

        <ContentContainer>
          <TextContainer>
            <h1 onClick={() => this.props.history.push('/')}>
              <img
                src={logo}
                width="80px"
                height="80px"
              />BullStamp
            </h1>
            <h3>Login</h3>

            <form onSubmit={this.handleSubmit}>
              <label>
                <Input
                  placeholder="Email"
                  type="text"
                  value={this.state.email}
                  onChange={event =>
                    this.setState({ email: event.target.value })
                  }
                />
              </label>
              <br />
              <label>
                <Input
                  placeholder="Password"
                  type="password"
                  value={this.state.password}
                  onChange={event =>
                    this.setState({ password: event.target.value })
                  }
                />
              </label>
              <br />
              <Button type="submit" text="Submit" isDisabled={!this.isValid()} />
            </form>
          </TextContainer>
        </ContentContainer>
      </PageContainer>
    );
  }

  isValid = () => {
    if (!this.state.email) return false;
    if (!this.state.password) return false;
    return true;
  };

  handleSubmit = event => {
    event.preventDefault();

    login(this.state)
      .then(response => {
        this.props.onLogin(
          response.username,
          response.needs_username,
          response.admin,
        );
      })
      .catch(e => {
        console.trace();
        toast.error(`An error occurred while logging in: ${JSON.stringify(e, null, " ")}`, {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 5000,
          hideProgressBar: true,
        })

      });
  };
}
