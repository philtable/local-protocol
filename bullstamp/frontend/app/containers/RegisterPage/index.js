/*
 * RegisterPage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import { register } from '../../utils/api';
import Button from '../../components/BigButton';
import logo from '../../images/bullstamplogo.png';
import jumbo from '../../images/jumbo.png';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const PageContainer = styled.div`
  height: 100vh;
  background-color: #458f70;
`;

const ContentContainer = styled.div`
  background-image: url(${jumbo});
  background-size: cover;
  padding-top: 100px;
  padding-bottom: 300px;
`;

const TextContainer = styled.div`
  color: white;
  padding-left: 10%;
`;

const Input = styled.input`
  padding: 0.5em;
  margin: 0.5em 0em 0.5em 0em;
  width: 300px;
  color: black;
  background: white;
  border: none;
  border-radius: 3px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class RegisterPage extends React.Component {
  state = {};

  render() {
    return (
      <PageContainer>

        <ToastContainer />

        <ContentContainer>
          <TextContainer>
            <h1 onClick={() => this.props.history.push('/')}>
              <img
                src={logo}
                width="80px"
                height="80px"
              />BullStamp
            </h1>
            <h3>Register</h3>

            <form onSubmit={this.handleSubmit}>
              <label>
                <Input
                  placeholder="Email"
                  type="email"
                  value={this.state.email}
                  onChange={event =>
                    this.setState({ email: event.target.value })
                  }
                />
              </label>
              <br />
              <label>
                <Input
                  placeholder="Password"
                  type="password"
                  value={this.state.password}
                  onChange={event =>
                    this.setState({ password: event.target.value })
                  }
                />
              </label>
              <br />
              <label>
                <Input
                  placeholder="Confirm Password"
                  type="password"
                  value={this.state.confirmPassword}
                  onChange={event =>
                    this.setState({ confirmPassword: event.target.value })
                  }
                />
              </label>
              <br />
              <Button onClick={this.handleSubmit} text={"Submit"} isDisabled={!this.isValid()} />
              <br />
              <br />
              <br />
              <a href="https://discord.gg/NAGcGqS" target="_blank" style={{ textDecoration: 'none' }}>
                Join the Discord for Approval
              </a>
            </form>
          </TextContainer>
        </ContentContainer>
      </PageContainer>
    );
  }

  isValid = () => {
    if (!this.state.email) return false;
    if (!this.state.password) return false;
    if (this.state.password !== this.state.confirmPassword) return false;
    return true;
  };

  handleSubmit = event => {
    event.preventDefault();

    if (!this.state.email) {
      toast.error("No email specified!", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 3000,
        hideProgressBar: true,
      });
      return;
    }

    if (!this.state.password) {
      toast.error("No password specified!", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 3000,
        hideProgressBar: true,
      });
      return;
    }

    if (this.state.password !== this.state.confirmPassword) {
      toast.error("Your passwords don't match", {
        position: toast.POSITION.TOP_CENTER,
        autoClose: 3000,
        hideProgressBar: true,
      });
      return;
    }

    register(this.state)
      .then(success => {
        toast.success('Your registration request has been submitted', {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 3000,
          hideProgressBar: true,
        });
      })
      .catch(e => {
        toast.error(`An error occurred while submitting registration: ${JSON.stringify(e, null, " ")}`, {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 5000,
          hideProgressBar: true,
        })
      });
  };
}
