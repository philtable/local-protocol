/*
 * HomePage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import ComposerCard from 'components/ComposerCard';
import ProfileSummaryCard from 'components/ProfileSummaryCard';
import RulesCard from 'components/RulesCard';
import DiscordCard from 'components/DiscordCard';
import PostCard from 'components/PostCard';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import InfiniteScroll from 'react-infinite-scroller';
import Spinner from 'components/Spinner';
import { getStamps, getTrendingStamps } from '../../utils/api';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -60px;
`;

const StyledProfileSummaryCard = styled(ProfileSummaryCard)`
  margin-right: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledDiscordCard = styled(DiscordCard)`
  margin-right: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledRulesCard = styled(RulesCard)`
  margin-left: 12px;
  margin-top: 12px;
  flex: 0 0 auto;
`;

const StyledPostCard = styled(PostCard)`
  margin-top: 12px;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class HomePage extends React.Component {
  state = {
    defaultStamps: [],
    hasMoreDefault: true,
    pageIsLoading: false,
  };

  componentWillMount() {
    this.props.changeTab("new");
    // When stamp arrives, add it to homepage feed
    this.listenerId = this.props.api.addStampListener(newStamp => {
      if (newStamp.reply_to) {
        return;
      }
      this.setState(prevState => {
        const newStamps = prevState.defaultStamps.slice(0);
        newStamps.unshift(newStamp);
        return {
          defaultStamps: newStamps,
        };
      });
    });
  }

  componentWillUnmount() {
    this.props.api.removeStampListener(this.listenerId);
  }

// pageIsLoading ensures _loadDefault is called one page at a time
  _loadDefault = page => {
    this.setState({pageIsLoading: true})
    getStamps(page).then(json => {
      this.setState(prevState => ({
        defaultStamps: prevState.defaultStamps.concat(json.stamps),
        hasMoreDefault: json.page_info.has_next_page,
      }));
      this.setState({pageIsLoading: false})
    });
  };

  render() {
    return (
      <div>
        <NavBar
          api={this.props.api}
          currentTab={this.props.currentTab}
        />
        <Banner />
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            {this.props.api.isLoggedIn
              ? ( <StyledProfileSummaryCard
                    username={this.props.api.getUsername()}
                    api={this.props.api}
                  /> )
              : ( <StyledDiscordCard /> )
            }
          <FeedContainer>
            {this.props.api.isLoggedIn && (
              <ComposerCard
                api={this.props.api}
                onPost={this.onPost}
                rows={4}
                placeholder="Write a stamp!"
                text="Post"
              />
            )}
              <InfiniteScroll
                  pageStart={0}
                  loadMore={this._loadDefault}
                  hasMore={this.state.hasMoreDefault && !this.state.pageIsLoading}
                  loader={
                    <SpinnerContainer key={0}>
                      <Spinner />
                    </SpinnerContainer>
                  }>
                  {this.state.defaultStamps.map((stamp, i) => (
                    <StyledPostCard
                      key={stamp.transaction_id}
                      stamp={stamp}
                      api={this.props.api}
                    />
                  ))}
                </InfiniteScroll>
              {!this.state.hasMoreDefault ? (
                <FooterContainer>That is it folks!</FooterContainer>
              ) : null}
            </FeedContainer>
            <StyledRulesCard />
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
