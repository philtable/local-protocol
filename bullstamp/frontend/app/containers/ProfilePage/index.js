/*
 * ProfilePage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import ProfileSummaryCard from 'components/ProfileSummaryCard';
import RulesCard from 'components/RulesCard';
import PostCard from 'components/PostCard';
import ListCard from 'components/ListCard';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import InfiniteScroll from 'react-infinite-scroller';
import Spinner from 'components/Spinner';
import { getStampsByAuthor, getFollowing, getFollowers } from '../../utils/api';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -60px;
`;

const LeftContainer = styled.div`
  margin-right: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
  @media (max-width: 880px) {
    display: none;
  }
`;

const FollowersList = styled(ListCard)`
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const RightContainer = styled.div`
  margin-left: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
  @media (max-width: 1000px) {
    display: none;
  }
`;

const FollowingList = styled(ListCard)`
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const StyledPostCard = styled(PostCard)`
  margin-top: 12px;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

const RenderStamp = ({ history, index, stamp, api }) => (
  stamp.reply_to === null
  ? (
    <div onClick={() => history.push(`/stamp/${stamp.transaction_id}`)}>
      <StyledPostCard key={stamp.transaction_id} api={api} stamp={stamp}/>
    </div>
  ) : (
    <div onClick={() => history.push(`/stamp/${stamp.reply_to}`)}>
      <StyledPostCard key={stamp.transaction_id} api={api} showReplyHint={false} replyingTo={stamp.parent_stamp.author} stamp={stamp}/>
    </div>
  )
);

/* eslint-disable react/prefer-stateless-function */
export default class ProfilePage extends React.Component {
  state = {
    stamps: [],
    followers: [],
    following: [],
    hasMore: true,
    hasMoreFollowers: true,
    hasMoreFollowing: true,
    pageIsLoading: false,
    followersAreLoading: false,
    followingAreLoading: false,
  };

  componentWillMount() {
    this.props.changeTab("stamps")
  }

// pageIsLoading ensures _loadPage is called one page at a time
  _loadPage = page => {
    this.setState({pageIsLoading: true})
    getStampsByAuthor(this.props.match.params.username, page).then(json => {
      this.setState(prevState => ({
        stamps: prevState.stamps.concat(json.stamps),
        hasMore: json.page_info.has_next_page,
      }));
      this.setState({pageIsLoading: false})
    });
  };

  _loadFollowers = page => {
    this.setState({followersAreLoading: true})
    getFollowers(this.props.match.params.username, page).then(json => {
      this.setState(prevState => ({
        followers: prevState.followers.concat(json.followers),
        hasMoreFollowers: json.page_info.has_next_page,
      }));
    });
    this.setState({followersAreLoading: true})
  };

  _loadFollowing = page => {
    this.setState({followingAreLoading: true})
    getFollowing(this.props.match.params.username, page).then(json => {
      this.setState(prevState => ({
        following: prevState.following.concat(json.following),
        hasMoreFollowing: json.page_info.has_next_page,
      }));
    });
    this.setState({followingAreLoading: true})
  };

  render() {
    return (
      <div>
        <NavBar api={this.props.api} bull={this.props.match.params.username} currentTab={this.props.currentTab} />
        <Banner/>
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            <LeftContainer>
              <ProfileSummaryCard
                username={this.props.match.params.username}
                api={this.props.api}
              />
              <FollowersList
                title={"Followers"}
                loadMore={this._loadFollowers}
                hasMore={this.state.hasMoreFollowers}
                isLoading={this.state.followersAreLoading}
                users={this.state.followers} />
            </LeftContainer>
            <FeedContainer>
              <InfiniteScroll
                pageStart={0}
                loadMore={this._loadPage}
                hasMore={this.state.hasMore && !this.state.pageIsLoading}
                loader={
                  <SpinnerContainer key={0}>
                    <Spinner />
                  </SpinnerContainer>
                }
              >
                {this.state.stamps.map((stamp, i) => (
                  <RenderStamp
                    history={this.props.history}
                    index={i}
                    stamp={stamp}
                    api={this.props.api}/>
                ))}
              </InfiniteScroll>
              {!this.state.hasMore ? (
                <FooterContainer>That is it folks!</FooterContainer>
              ) : null}
            </FeedContainer>
            <RightContainer>
              <RulesCard />
              <FollowingList
                title={"Following"}
                loadMore={this._loadFollowing}
                hasMore={this.state.hasMoreFollowing}
                isLoading={this.state.followingAreLoading}
                users={this.state.following} />
            </RightContainer>
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
