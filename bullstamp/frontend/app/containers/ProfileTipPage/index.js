/*
 * ProfileTipPage
 *
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import styled from 'styled-components';
import ProfileSummaryCard from 'components/ProfileSummaryCard';
import RulesCard from 'components/RulesCard';
import PostCard from 'components/PostCard';
import PostContents from 'components/PostContents';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import InfiniteScroll from 'react-infinite-scroller';
import Spinner from 'components/Spinner';
import { getTipsByCreator } from '../../utils/api';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const Divider = styled.div`
  align-self: stretch;
  background-color: #e8e8e8;
  height: 1px;
  margin-top: 8px;
  margin-bottom: 8px;
  margin-left: -16px;
  margin-right: -16px;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -60px;
`;

const StyledProfileSummaryCard = styled(ProfileSummaryCard)`
  margin-right: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const StyledRulesCard = styled(RulesCard)`
  margin-left: 12px;
  margin-top: 12px;
  width: 260px;
  flex: 0 0 auto;
`;

const StyledPostCard = styled(PostCard)`
  margin-top: 12px;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class ProfileTipPage extends React.Component {
  state = {
    tips: [],
    hasMore: true,
    pageIsLoading: false,
  };

  componentWillMount() {
    this.props.changeTab("tips")
  }

// pageIsLoading ensures _loadPage is called one page at a time
  _loadPage = page => {
    this.setState({pageIsLoading: true})
    getTipsByCreator(this.props.match.params.username, page).then(json => {
      this.setState(prevState => ({
        tips: prevState.tips.concat(json.tips),
        hasMore: json.page_info.has_next_page,
      }));
      this.setState({pageIsLoading: false})
    });
  };

  render() {
    return (
      <div>
        <NavBar api={this.props.api} bull={this.props.match.params.username} currentTab={this.props.currentTab}/>
        <Banner/>
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            <StyledProfileSummaryCard
              username={this.props.match.params.username}
              api={this.props.api}
            />
            <FeedContainer>
              <InfiniteScroll
                pageStart={0}
                loadMore={this._loadPage}
                hasMore={this.state.hasMore && !this.state.pageIsLoading}
                loader={
                  <SpinnerContainer key={0}>
                    <Spinner />
                  </SpinnerContainer>
                }
              >
                {this.state.tips.map((tip, i) => (
                  <StyledPostCard api={this.props.api} stamp={tip.to} tip={tip}/>
                ))}
              </InfiniteScroll>
              {!this.state.hasMore ? (
                <FooterContainer>That is it folks!</FooterContainer>
              ) : null}
            </FeedContainer>
            <StyledRulesCard />
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
