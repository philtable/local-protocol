/* eslint-disable react/prefer-stateless-function */
/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import LandingPage from 'containers/LandingPage/Loadable';
import HomePage from 'containers/HomePage/Loadable';
import TrendingPage from 'containers/TrendingPage/Loadable';
import PostPage from 'containers/PostPage/Loadable';
import NotificationsPage from 'containers/NotificationsPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import ProfilePage from 'containers/ProfilePage/Loadable';
import ProfileTipPage from 'containers/ProfileTipPage/Loadable';
import RegisterPage from 'containers/RegisterPage/Loadable';
import LoginPage from 'containers/LoginPage/Loadable';
import UsersPage from 'containers/UsersPage/Loadable';
import SetUsernamePage from 'containers/SetUsernamePage/Loadable';
import RequestPasswordResetPage from 'containers/RequestPasswordResetPage/Loadable';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { MANAGEDEOS_ENDPOINT } from '../../constants/api';
import { WebSocketClient } from '../../utils/ws';
import { ManagedEOSClient } from '../../utils/clients/managedeos';
import { ScatterClassicClient } from '../../utils/clients/scatterclassic';

const stampCallbacks = {};
let stampCallbackId = 1;
const tipCallbacks = {};
let tipCallbackId = 1;
const addStampListener = callback => {
  const curCallbackId = stampCallbackId++;
  stampCallbacks[curCallbackId] = callback;
  return curCallbackId;
};
const addTipListener = callback => {
  const curCallbackId = tipCallbackId++;
  tipCallbacks[curCallbackId] = callback;
  return curCallbackId;
};
const removeStampListener = id => {
  delete stampCallbacks[id];
};
const removeTipListener = id => {
  delete tipCallbacks[id];
};
const handleMessage = data => {
  console.log('received message');
  console.log(data);
  const msg = JSON.parse(data);
  switch (msg.type) {
    case 'STAMP':
      Object.values(stampCallbacks).forEach(fn => fn(msg.data));
      break;
    case 'TIP':
      Object.values(tipCallbacks).forEach(fn => fn(msg.data));
      break;
  }
};

export default class App extends React.Component {
  state = {
    currentTab: 'none',
    api: {
      isLoggedIn: false,
      addStampListener,
      addTipListener,
      removeStampListener,
      removeTipListener,
      isAdmin: () => false,
      getUsername: () =>
        localStorage.getItem('cached-username') || 'usernamefake',
      stamp: msg => {},
      reply: (id, msg) => {},
      tip: (author, id) => {},
      postAvatarLink: link => {},
      getAppTokenBalance: () => Promise.resolve('0.0000 APP'),
    },
  };

  onSuccessfulLogin = (clientType, username) => {
    console.log('successfully logged in', { clientType, username });

    // todo(mitsui): this logic shouldn't be here
    if (clientType === 'scatter-classic') {
      this.setState(
        {
          api: {
            isLoggedIn: true,
            ws: new WebSocketClient(
              this.scatterclassic.getUsername(),
              handleMessage,
            ),
            addStampListener,
            addTipListener,
            removeStampListener,
            removeTipListener,
            isAdmin: () => false,
            getUsername: this.scatterclassic.getUsername,
            stamp: this.scatterclassic.stamp,
            reply: this.scatterclassic.stamp,
            tip: this.scatterclassic.tip,
            postAvatarLink: this.scatterclassic.avatar,
            getAppTokenBalance: this.scatterclassic.getBalance,
          },
        },
        () => {
          const e = new Event('reloadBalances');
          document.dispatchEvent(e);
        },
      );
    } else if (clientType === 'managedeos') {
      this.setState(
        {
          needsUsername: false,
          api: {
            isLoggedIn: true,
            ws: new WebSocketClient(
              this.managedeos.getUsername(),
              handleMessage,
            ),
            addStampListener,
            addTipListener,
            removeStampListener,
            removeTipListener,
            getUsername: () => this.managedeos.getUsername(),
            isAdmin: () => this.managedeos.isAdmin(),
            stamp: this.managedeos.stamp,
            reply: this.managedeos.stamp,
            tip: this.managedeos.tip,
            postAvatarLink: this.managedeos.avatar,
            getAppTokenBalance: this.managedeos.getBalance,
          },
        },
        () => {
          const e = new Event('reloadBalances');
          document.dispatchEvent(e);
        },
      );
    }
  };

  scatterclassicAvailable = () => {
    this.setState({ scatterAvailable: true });
  };

  managedeosSetUsername = () => {
    this.setState({
      needsUsername: true,
    });
  };

  managedeosVerifyEmail = () => {
    toast.error("You have not verified your account yet! Check your email!", {
      position: toast.POSITION.TOP_CENTER,
      autoClose: 3000,
      hideProgressBar: true,
    });
  };

  managedeosNoUsername = () => {
    toast.error("You have not been approved yet! Join the Discord!", {
      position: toast.POSITION.TOP_CENTER,
      autoClose: 3000,
      hideProgressBar: true,
    });
  };

  componentDidMount() {
    fetch(`${MANAGEDEOS_ENDPOINT}/api/v1/config`)
      .then(response => response.json())
      .then(response => {
        if (!response.ok) {
          throw new Error('response not ok');
        }
        return response.config;
      })
      .then(response => {
        console.log('loaded network config', response);
        this.scatterclassic = new ScatterClassicClient(
          response,
          this.onSuccessfulLogin,
          this.scatterclassicAvailable,
        );
        this.managedeos = new ManagedEOSClient(
          response,
          this.onSuccessfulLogin,
          this.managedeosSetUsername,
          this.managedeosVerifyEmail,
          this.managedeosNoUsername,
        );
      })
      .catch(err => {
        console.log('failed to fetch config', err);
      });
  }

  changeTab = tab => {
    this.setState({
      currentTab: tab,
    });
  };

  render() {
    if (this.state.needsUsername) {
      return (
        <SetUsernamePage
          {...this.props}
          onLogin={() => this.managedeos.doLogin()}
        />
      );
    }

    if (this.redirectTo) {
      return (
        <Route
          render={props => {
            const redirectTo = this.redirectTo;
            this.redirectTo = undefined;
            return <Redirect to={redirectTo} />;
          }}
        />
      );
    }

    return (
      <div>
        <ToastContainer />
        <Switch>
          <Route
            exact
            path="/"
            render={props =>
              !this.state.api.isLoggedIn
              ? <LandingPage
                {...props}
                client={this.state.scatterAvailable ? this.scatterclassic : undefined}
                />
              : <Redirect to="/home"/>}/>
          <Route
exact path="/home" render={props => (
              <HomePage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            )}
          />
          <Route
exact path="/trending" render={props => (
              <TrendingPage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            )}
          />
          <Route
exact path="/register" render={props => !this.state.api.isLoggedIn
              ? <RegisterPage {...props} />
              : <Redirect to={'/home'} />
            }
          />
          <Route
exact path="/login" render={props => (!this.state.api.isLoggedIn)
              ?
                <LoginPage
                  {...props}
                  onLogin={() => this.managedeos.doLogin()}
                />
              : <Redirect to={'/home'} />
            }
          />
          <Route
exact path="/requestpasswordreset" render={props => (!this.state.api.isLoggedIn)
              ? <RequestPasswordResetPage {...props} />
              : <Redirect to={'/home'} />
            }
          />
          <Route
exact path="/notifications" render={props => (
              <NotificationsPage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            )}
          />
          <Route
exact path="/stamp/:stampId" render={props => (
              <PostPage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            )}
          />
          <Route
exact path="/bull/:username" render={props =>
              <ProfilePage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            }
          />
          <Route
exact path="/bull/:username/tips" render={props =>
              <ProfileTipPage
                {...props}
                api={this.state.api}
                currentTab={this.state.currentTab}
                changeTab={this.changeTab}
              />
            }
          />
          <Route
exact path="/users" render={props =>
            <UsersPage {...props} api={this.state.api}/>}
          />
          <Route component={NotFoundPage} />
        </Switch>
      </div>
    );
  }
}
