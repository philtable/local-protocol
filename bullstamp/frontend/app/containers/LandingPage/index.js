/*
 * LandingPage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React, { Fragment } from 'react';
import styled from 'styled-components';
import jumbo from '../../images/jumbo.png';
import logo from '../../images/bullstamplogo.png';
import scattersignin from '../../images/scattersignin.png';

import Button from '../../components/BigButton';

const PageContainer = styled.div`
  height: 100vh;
  background-color: #458f70;
`;

const ContentContainer = styled.div`
  background-image: url(${jumbo});
  background-size: cover;
  padding-top: 100px;
  padding-bottom: 300px;
`;

const TextContainer = styled.div`
  color: white;
  padding-left: 10%;
`;

const LoginContainer = styled.div`
  :hover {
    cursor: ${'pointer'};
  }
  width: 300px;
  height: 50px;
`;

/* eslint-disable react/prefer-stateless-function */
export default class LandingPage extends React.Component {
  state = {
    loggedIn: false,
  };

  render() {
    const webButtons = (
      <LoginContainer>
        <Button
          text="Login"
          onClick={() => this.props.history.push('/login')}
        />
        <Button
          text="Register"
          onClick={() => this.props.history.push('/register')}
        />
        <Button
          text="View Demo"
          onClick={() => (window.location.href = '/home')}
        />
        <Button
          text="Reset Password"
          onClick={() => (window.location.href = '/requestpasswordreset')}
        />
      </LoginContainer>
    );

    const loginMethod = this.props.client ? (
      <Fragment>
        <LoginContainer>
          <img
            src={scattersignin}
            width="300px"
            height="50px"
            onClick={() => this.props.client.doLogin()}
          />
        </LoginContainer>
        {webButtons}
      </Fragment>
    ) : (
      webButtons
    );

    return (
      <PageContainer>
        <ContentContainer>
          <TextContainer>
            <h1>
              <img src={logo} width="80px" height="80px" />BullStamp
            </h1>
            <h3>
              In Beta: Do NOT share on Reddit, Telegram or any other public
              forum
            </h3>

            {loginMethod}
          </TextContainer>
        </ContentContainer>
      </PageContainer>
    );
  }
}
