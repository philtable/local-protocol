/*
 * The post detail page with
 */

import React from 'react';
import styled from 'styled-components';
import Button from 'components/Button';
import PostContents from 'components/PostContents';
import NavBar from 'components/NavBar';
import Banner from 'components/Banner';
import ResponsiveContainer from 'components/ResponsiveContainer';
import Themes from 'constants/themes';
import { getUsers, resetPassword, updateUser, updateUserBeta, updateUserUsername } from '../../utils/api';

const ContentContainer = styled.div`
  display: flex;
  flex-direction: row;
`;

const FeedContainer = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
  flex: 1 1 auto;
  position: relative;
  top: -28px;
`;

const MainPostContents = styled(PostContents)`
  padding-top: 14px;
  padding-bottom: 14px;
`;

const TextArea = styled.textarea`
  border-width: ${props => (props.isFocused ? '2px' : '1px')};
  border-style: solid;
  border-color: ${props =>
  props.isFocused ? props.theme.primaryColorDark : '#dcdcdc'};
  border-radius: 6px;
  margin-top: 10px;
  outline: none;
  padding-left: ${props => (props.isFocused ? '7px' : '8px')};
  padding-right: ${props => (props.isFocused ? '7px' : '8px')};
  padding-top: ${props => (props.isFocused ? '5px' : '6px')};
  padding-bottom: ${props => (props.isFocused ? '5px' : '6px')};
  color: ${props => (props.disabled ? '#dcdcdc' : 'initial')};
`;

TextArea.defaultProps = {
  theme: Themes.main,
};

const StyledButton = styled(Button)`
  margin-left: 14px;
`;

const HorizontalContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-top: 12px;
`;

const RepliesContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: stretch;
`;

const NoRepliesContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

const PublicKey = styled.span`
  font-size: 8px;
`;

const NameSpan = styled.span`
  font-weight: bold;
`;

const FooterContainer = styled.div`
  display: flex;
  justify-content: center;
  color: #9c9c9c;
  font-style: italic;
  padding-top: 14px;
  padding-bottom: 14px;
`;

export default class UsersPage extends React.Component {
  state = {
    users: [],
  };

  componentDidMount() {
    this.reloadUsers();
  }

  reloadUsers = () => {
    getUsers().then(users => {
      this.setState({ users });
    });
  };

  resetPasswordHelper = (id) => {
    const newPassword = prompt(`Enter new password for user ${id}`);
    if (!newPassword) return;

    resetPassword(id, newPassword).then(() => {
      alert(`Successfully reset password for user ${id}`);
      this.reloadUsers();
    })
      .catch(err => alert('Error resetting password: ' + JSON.stringify(err)));
  };

  updateUsernameHelper = (id) => {
    const newUsername = prompt(`Enter new username for user ${id}`);
    if (!newUsername) return;

    updateUserUsername(id, newUsername).then(() => {
      alert(`Successfully updated username for user ${id}`);
      this.reloadUsers();
    })
      .catch(err => alert('Error updating username: ' + JSON.stringify(err)));
  };

  render() {
    const users = this.state.users.map(ent => {
      return (
        <tr key={ent.id.toString()}>
          <th>{ent.id}</th>
          <th>{ent.username || 'NA'}</th>
          <th>{ent.email}</th>
          <th><PublicKey>{ent.public_key}</PublicKey></th>
          <th>{ent.admin === true ? 'true' : '0'}</th>
          <th>{ent.beta_user === true ? 'true' : '0'}</th>
          <th>
            <button
              onClick={() => updateUserBeta(ent.id, !ent.beta_user).then(() => this.reloadUsers()).catch(() => alert(`Error toggling beta user status`))}>Toggle
            </button>
          </th>
          <th>
            <button onClick={() => this.resetPasswordHelper(ent.id)}>Change</button>
          </th>
          <th>
            <button disabled={ent.username} onClick={() => this.updateUsernameHelper(ent.id)}>Update</button>
          </th>
        </tr>
      );
    });

    return (
      <div>
        <NavBar
          api={this.props.api}
        />
        <Banner/>
        <ResponsiveContainer flexDirection="column">
          <ContentContainer>
            <FeedContainer>
              <table>
                <tbody>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Public Key</th>
                    <th>Is Admin</th>
                    <th>Is Beta</th>
                    <th>Toggle Beta</th>
                    <th>Reset Pass</th>
                    <th>Update Name</th>
                  </tr>
                  {users}
                </tbody>
              </table>
            </FeedContainer>
          </ContentContainer>
        </ResponsiveContainer>
      </div>
    );
  }
}
