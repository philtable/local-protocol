/*
 * Basic themed Big button.
 */

import React from 'react';
import styled from 'styled-components';
import Themes from 'constants/themes';

const Container = styled.button`
  /* Adapt the colours based on primary prop */
  background: ${props => (props.primary ? 'black' : 'white')};
  color: ${props => (props.primary ? 'white' : 'black')};

  font-size: 1.5em;
  width: 300px;
  margin: 10px 0px 0px 0px;
  padding: 0.5em 1.45em;
  border: 2px solid black;
  border-radius: 3px;

  :hover {
    background-color: ${props =>
    props.isDisabled ? '#c7c7c7' : props.theme.primaryColorDark};
    cursor: ${props => (props.isDisabled ? 'not-allowed' : 'pointer')};
  }
`;
const DisabledContainer = styled.button`
  /* Adapt the colours based on primary prop */
  background: lightgray;
  color: ${props => (props.primary ? 'white' : 'black')};

  font-size: 1.5em;
  width: 300px;
  margin: 10px 0px 0px 0px;
  padding: 0.5em 1.45em;
  border: 2px solid black;
  border-radius: 3px;
`;

Container.defaultProps = {
  theme: Themes.main,
};

export default class Button extends React.Component {
  static defaultProps = {
    isDisabled: false,
  };

  _onClick = () => {
    if (!this.props.isDisabled && this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {
    const { text, className, isDisabled } = this.props;

    if (isDisabled) {
      return (
        <DisabledContainer
          className={className}
          disabled={isDisabled}
          isDisabled={isDisabled}
        >
          {text}
        </DisabledContainer>
      );
    }

    return (
      <Container
        {...this.props}
        className={className}
        disabled={isDisabled}
        isDisabled={isDisabled}
        onClick={this._onClick}
      >
        {text}
      </Container>
    );
  }
}
