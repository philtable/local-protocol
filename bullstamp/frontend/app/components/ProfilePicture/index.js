import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { getAvatar } from '../../utils/api';
import Logo from '../../images/bullstamplogo.png';
import styled from 'styled-components';

const Container = styled.div`
`;

const StyledImg = styled.img`
  border-radius: 50%;

  transition-timing-function: ease;
  transition: 0.6s;

  :hover {
    border-radius: 10%;
  }
`;

const StyledLetterProfilePic = styled.div`
  border-radius: 50%;

  width: ${props => props.size}px;
  height: ${props => props.size}px;

  background-color: #${props => props.color};
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${props => props.size / 32}em;
  font-family: Arial, Helvetica, sans-serif;
  text-transform: capitalize;
  padding-top: 5%;

  transition-timing-function: ease;
  transition: 0.6s;

  :hover {
    border-radius: 10%;
  }
`;

const COLORS = [
  'f44336',
  'E91E63',
  '9C27B0',
  '673AB7',
  '3F51B5',
  '2196F3',
  '03A9F4',
  '00BCD4',
  '009688',
  '4CAF50',
  '8BC34A',
  'CDDC39',
  'FFEB3B',
  'FFC107',
  'FF9800',
  'FF5722',
  '795548',
  '607D8B',
];
const DEFAULT_SIZE = 125;

const hashUsername = (username) => {
  // ¯\_(ツ)_/¯ https://stackoverflow.com/a/7616484
  var hash = 0, i, chr;
  if (username.length === 0) return hash;
  for (i = 0; i < username.length; i++) {
    chr   = username.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return Math.abs(hash);
};

export default class ProfilePicture extends React.Component {
  state = {
    src: null,
    isCancelled: false,
  }

  componentDidMount() {
    getAvatar(this.props.author).then(res => {
      if (res && !this.isCancelled) {
        this.setState({ src: res.url });
      }
    });
  }

  componentWillUnmount() {
    this.setState({ isCancelled: true });
  }

  render() {
    const { size } = this.props;
    const author = this.props.author || 'username';
    const color = COLORS[hashUsername(author) % COLORS.length];

    const styledLetterProfilePic = (
      <StyledLetterProfilePic size={ size || DEFAULT_SIZE } color={color}>
        {author[0]}
      </StyledLetterProfilePic>
    );

    const styledImg = (
      <StyledImg
        height={ size || DEFAULT_SIZE }
        width={ size || DEFAULT_SIZE }
        src={ this.state.src || Logo }
        onError={(e)=>{e.target.onerror = null; e.target.src=Logo}} />
    );

    return (
      <Link
        to={{ pathname: `/bull/${author}`}}
        style={{ textDecoration: 'none' }}>
        <Container>
          { this.state.src ? styledImg : styledLetterProfilePic }
        </Container>
      </Link>
    );
  }
};

ProfilePicture.propTypes = {
  size: PropTypes.number,
}
