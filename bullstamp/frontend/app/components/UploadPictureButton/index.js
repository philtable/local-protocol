/*
 * Upload Picture Button.
 */

import React from 'react';
import styled from 'styled-components';
import Themes from 'constants/themes';
import CameraImage from '../../images/camera.png';
import { uploadAvatar } from '../../utils/api';

const ChangeProfilePicLogo = styled.div`
  height: 30px;
`;

const UploadPicture = styled.input`
  display: none;
`;

const CameraLogo = styled.img`
  cursor: pointer;
  float: left;
  width: 25px;
  height: 25px;
`;

const UploadTitle = styled.span`
  height: 30px;
  cursor: pointer;
  float: right;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  margin-bottom: 12px;
  align-self: flex-start;
`;

const HiddenSubmit = styled.input`
  display: none;
`;

export default class UploadPictureButton extends React.Component {
  static defaultProps = {
    isDisabled: false,
  };

  _changeProfilePicture(file, author) {
    // Upload avatar to S3
    uploadAvatar(file, author);
    // Post avatar link to the blockchain
    this.props.api.postAvatarLink(
      `https://apptoken-avatars.nyc3.digitaloceanspaces.com/${author}.png`,
    );
    alert('Profile picture uploaded, it will take a few moments to load.');
    return false;
  }

  render() {

    return (

      // This input posts an image file to the backend and sends a link to the blockchain
      <ChangeProfilePicLogo>
        <label htmlFor="profile-picture-input">
          <CameraLogo src={CameraImage} />
          <UploadTitle>Upload Avatar</UploadTitle>
        </label>
        <UploadPicture
          type="file"
          name="avatar"
          accept="image/x-png,image/gif,image/jpeg"
          id="profile-picture-input"
          onChange={e => {
            e.preventDefault();
            e.target.files[0].size > 102400
              ? alert('Your image is too big, it must be under 100 kb')
              : this._changeProfilePicture(
                e.target.files[0],
                this.props.api.getUsername(),
              );
          }}
        />
        <HiddenSubmit
          id="profile-picture-submit"
          type="submit"
          value="Send file"
        />
        <iframe
          style={{ display: 'none' }}
          name="profile-picture-target"
          id="profile-picture-target"
        />
      </ChangeProfilePicLogo>

    );
  }
}
