import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';
import ProfilePicture from 'components/ProfilePicture';

const StyledDiv = styled.div`
  border: 2px solid lightgrey;
  border-radius: 10px;
  width: 230px;
  margin: 5px 0px 5px 0px;
  cursor: pointer;
`;

const ProfileContainer = styled.div`
  display: flex;
  margin: 10px;
`;

const NameContainer = styled.div`
  margin: 15px;
`;

export default class ProfileTag extends React.Component {

  clickProfile = event => {
    window.open(`/bull/${this.props.author}`, "_blank")
  };

  render() {
    return (
      <StyledDiv>
        <ProfileContainer onClick={this.clickProfile}>
          <ProfilePicture
            size={60}
            author={this.props.author}
             />
          <NameContainer>{this.props.author}</NameContainer>
        </ProfileContainer>
      </StyledDiv>
    );
  }
}
