
import React from 'react';
import styled from 'styled-components';
import PostContents from 'components/PostContents';
import Card from 'components/Card';
import Themes from 'constants/themes';

const StyledSpinner = styled.div`
  position: relative;
  height: ${props => props.size}px;
  width: ${props => props.size}px;

  @keyframes spinner {
    to {transform: rotate(360deg);}
  }

  :before {
    content: '';
    box-sizing: border-box;
    position: absolute;
    top: 50%;
    left: 50%;
    width: ${props => props.size}px;
    height: ${props => props.size}px;
    margin-top: -${props => props.size / 2}px;
    margin-left: -${props => props.size / 2}px;
    border-radius: 50%;
    border: 2px solid #d8d8d8;
    border-top-color: ${props => props.theme.primaryColor};
    animation: spinner .6s linear infinite;
  }
`;

StyledSpinner.defaultProps = {
  theme: Themes.main,
  size: 25,
};


class Spinner extends React.Component {

  render() {
    return <StyledSpinner className={this.props.className} />;
  }
};

export default Spinner;
