/*
 * Basic card container component.
 */


import React from 'react';
import styled from 'styled-components';
import ResponsiveContainer from 'components/ResponsiveContainer';

const Container = styled.div`
  background-color: white;
  border-radius: 10px;
  box-shadow: 0 2px 3px rgba(0,0,0,0.1);
  display: flex;
  padding: 12px 16px 12px 16px;
`;

export default class Card extends React.Component {
  render() {
    const {children, ...OtherProps} = this.props;
    return (
      <Container {...OtherProps}>
        {children}
      </Container>
    );
  }
};
