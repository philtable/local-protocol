import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';
import ProfilePicture from 'components/ProfilePicture';
import ProfileTag from 'components/ProfileTag';
import InfiniteScroll from 'react-infinite-scroller';
import Spinner from 'components/Spinner';

const StyledCard = styled(Card)`
  width: 260px;
  align-items: stretch;
  align-self: flex-start;
  flex-direction: column;
  @media (max-width: 1000px) {
    display: none;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  margin: auto;
`;

const SpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 20px;
`;

const Title = styled.span`
  font-weight: bold;
  align-self: flex-start;
`;

export default class ListCard extends React.Component {

  render() {
    return (
      <StyledCard className={this.props.className}>
        <Container>
          <Title>{this.props.title}</Title>
          <InfiniteScroll
            pageStart={0}
            loadMore={this.props.loadMore}
            hasMore={this.props.hasMore && !this.props.isLoading}
            loader={
              <SpinnerContainer key={0}>
                <Spinner />
              </SpinnerContainer>
            }
          >
            {this.props.users.map((user, i) => (
              <ProfileTag
                key={user.id}
                author={user.username}
                size={50} />
            ))}
          </InfiniteScroll>
        </Container>
      </StyledCard>
    );
  }
}
