/*
 * Post composer card component.
 */

import React from 'react';
import styled from 'styled-components';
import Button from 'components/Button';
import Card from 'components/Card';
import Spinner from 'components/Spinner';
import Themes from 'constants/themes';
import { handleError } from '../../utils/eos';

const Container = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: column;
`;

const StyledCard = styled(Card)`
  align-items: stretch;
  flex-direction: column;
`;

const NameSpan = styled.span`
  font-weight: bold;
`;

const TextArea = styled.textarea`
  border-width: ${props => (props.isFocused ? '2px' : '1px')};
  border-style: solid;
  border-color: ${props =>
    props.isFocused ? props.theme.primaryColorDark : '#dcdcdc'};
  border-radius: 6px;
  margin-top: 10px;
  outline: none;
  padding-left: ${props => (props.isFocused ? '7px' : '8px')};
  padding-right: ${props => (props.isFocused ? '7px' : '8px')};
  padding-top: ${props => (props.isFocused ? '5px' : '6px')};
  padding-bottom: ${props => (props.isFocused ? '5px' : '6px')};
  color: ${props => (props.disabled ? '#dcdcdc' : 'initial')};
`;

TextArea.defaultProps = {
  theme: Themes.main,
};

const StyledButton = styled(Button)`
  margin-left: 14px;
`;

const HorizontalContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-top: 12px;
`;

export default class Composer extends React.Component {
  state = {
    message: '',
    isFocused: false,
    isLoading: false,
  };

  _onTextAreaChange = event => {
    this.setState({
      message: event.target.value,
    });
  };

  _onTextAreaFocus = event => {
    this.setState({
      isFocused: true,
    });
  };

  _onTextAreaBlur = event => {
    this.setState({
      isFocused: false,
    });
  };

  _onPost = () => {
    this.setState({ isLoading: true });

    const message = this.state.message;

    this.props.api
      .stamp(message)
      .then(r => {
        console.log('stamped!', r);
        this.setState({ isLoading: false, message: '' });
      })
      .catch(e => {
        console.log('error stamping', e);
        handleError(e);
        this.setState({ isLoading: false, message: '' });
      });
  };

  render() {
    return (
      <StyledCard className={this.props.className}>
        <Container>
          <NameSpan>@{this.props.api.getUsername()}</NameSpan>
          <TextArea
            disabled={this.state.isLoading}
            isFocused={this.state.isFocused}
            onBlur={this._onTextAreaBlur}
            onChange={this._onTextAreaChange}
            onFocus={this._onTextAreaFocus}
            placeholder={this.props.placeholder}
            rows={this.props.rows}
            value={this.state.message}
          />
          <HorizontalContainer>
            {<div>{this.state.message.length}/280</div>}
            <span>&nbsp;&nbsp;&nbsp;</span>
            {this.state.isLoading ? <Spinner /> : null}
            <StyledButton
              isDisabled={
                this.state.isLoading ||
                this.state.message.length === 0 ||
                this.state.message.length > 280
              }
              text={this.props.text}
              onClick={this._onPost}
            />
          </HorizontalContainer>
        </Container>
      </StyledCard>
    );
  }
}
