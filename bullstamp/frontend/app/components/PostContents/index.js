import React, { Fragment } from 'react';
import Linkify from 'react-linkify';
import Moment from 'react-moment';
import styled from 'styled-components';
import ProfilePicture from 'components/ProfilePicture';
import Themes from 'constants/themes';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import StampImage from '../../images/stamp.png';

import { APP_TOKEN_UNIT } from '../../utils/constants';
import { getTipsByStamp } from '../../utils/api';

const Container = styled.div`
  display: flex;
  flex: 1 1 auto;
  align-items: start;
`;

const StyledProfilePicture = styled(ProfilePicture)`
  margin-top: 8px;
`;

const ContentContainer = styled.div`
  padding-left: 14px;
  flex: 1 1 auto;
`;

const NameContainer = styled.div`
  display: flex;
`;

const ProfileContainer = styled.div`
  display: flex;
`;

const NameTitle = styled.span`
  font-weight: bold;
`;

const UsernameTitle = styled.span`
  color: #9c9c9c;
  margin-left: 6px;
`;

const DescriptionText = styled.div`
  margin-top: 4px;
  word-break: break-word;
`;

const PostHeaderText = styled.span`
  color: #9c9c9c;
  font-style: italic;
`;

const ReplyHintText = styled.span`
  color: #9c9c9c;
  font-size: small;
  margin-top: 4px;
`;

const StyledA = styled.a`
  text-decoration: none;
  color: #9c9c9c;
  font-size: small;

  transition-timing-function: ease;
  transition: 0.15s;
`;

const VoteContainer = styled.div`
  align-items: center;
  flex-direction: column;
  padding-top: 4px;
  padding-left: 4px;
  padding-right: 4px;
  padding-bottom: 4px;
  margin-right: -4px;
  flex: 0 0 auto;
  text-align: center;
  cursor: pointer;
  width: 45px;
`;

const ArrowBlock = styled.div``;

const StyledPolygonEnabled = styled.polygon`
  fill: black;

  ${VoteContainer}:hover & {
    fill: ${props => props.theme.primaryColor};
  }
`;

StyledPolygonEnabled.defaultProps = {
  theme: Themes.main,
};

const StyledPolygonDisabled = styled.polygon`
  fill: lightgrey;
`;

StyledPolygonDisabled.defaultProps = {
  theme: Themes.main,
};

const StyledPolygonSelf = styled.polygon`
  fill: white;
`;

StyledPolygonSelf.defaultProps = {
  theme: Themes.main,
};

class PostContents extends React.Component {
  state = {
    stamp: this.props.stamp,
    tipping: this.props.stamp.author === this.props.api.getUsername(),
  };

  componentWillMount() {
    this.listenerId = this.props.api.addTipListener(tip => {
      if (tip.to.transaction_id !== this.state.stamp.transaction_id) {
        return;
      }
      if (tip.from !== this.props.api.getUsername()) {
        this.setState({ stamp: tip.to });
        return;
      }

      const e = new Event('reloadBalances');
      e.amount = -1;
      document.dispatchEvent(e);
      this.setState({ stamp: tip.to, tipping: false });
    });
  }

  componentWillUnmount() {
    this.props.api.removeTipListener(this.listenerId);
  }

  _getReplyHintText() {
    const replyCount = this.state.stamp.num_replies;
    return `${replyCount === 0 ? 'No' : replyCount} ${
      replyCount === 1 ? 'reply' : 'replies'
    }`;
  }

  _onTip = event => {
    event.stopPropagation();
    if (this.state.tipping) {
      return;
    }

    this.setState({ tipping: true }, () => {
      if (this.state.stamp.author === this.props.api.getUsername()) {
        toast.error("you can't tip yourself!", {
          position: toast.POSITION.TOP_CENTER,
          autoClose: 3000,
          hideProgressBar: true,
        });
        return;
      }
      this.props.api
        .tip(this.state.stamp.author, this.state.stamp.transaction_id)
        .catch(e => {
          console.log(e);
          if (e.json.error && e.json.error.name === 'eosio_assert_message_exception') {
            toast.error("You need APP tokens to tip a stamp", {
              position: toast.POSITION.TOP_CENTER,
              autoClose: 3000,
              hideProgressBar: true,
            });
          } else {
            toast.error(`an error occured while tipping: ${JSON.stringify(e, null, " ")}`, {
              position: toast.POSITION.TOP_CENTER,
              autoClose: 5000,
              hideProgressBar: true,
            });
          }
          this.setState({ tipping: false });
        });
    });
  };

  _hasUserTippedStamp = (stamp_id, username) => {
    getTipsByStamp(stamp_id).then(json => {
      var result = json.tips.filter(tip =>
        tip.creator === username
      )
      if (result.length !== 0) {
        this.setState({ tipping: true })
      }
    })
  }

  /* clickName = event => {
    event.stopPropagation();

    this.props.history.push(`/bull/${this.state.stamp.author}`);
  }; */

  clickProfile = event => {
    event.stopPropagation();
    // this.props.history.push(`/bull/${this.state.stamp.author}`);
  };

  render() {
    const stamp = this.state.stamp || {};
    const author = stamp.author || 'username';
    const content = stamp.content.substring(0, 280) || '';
    const contentHtml = content.split(/[\n\u2028]/).map(
      line =>
        line.length > 0 && (
          <Fragment key={line}>
            <span>{line}</span>
            <br />
          </Fragment>
        ),
    );
    const tip = (stamp.tips || 0) / APP_TOKEN_UNIT;
    if (!this.state.stamp) {
      return null;
    }

    let tipIcon = this.state.tipping ? (
      <StyledPolygonDisabled points="0,15 15,15 7.5,0" />
    ) : (
      <StyledPolygonEnabled points="0,15 15,15 7.5,0" />
    );
    if (this.state.stamp.author === this.props.api.getUsername()) {
      tipIcon = <StyledPolygonSelf points="0,15 15,15 7.5,0" />;
    }

    return (
      <Container className={this.props.className}>

        <ToastContainer />

        <ProfileContainer onClick={this.clickProfile}>
          <StyledProfilePicture size={60} author={author} />
        </ProfileContainer>
        <ContentContainer>
          <NameContainer>
            <NameTitle /* onClick={this.clickName} */>{`@${author}`}</NameTitle>
            {/* <UsernameTitle>{`@${author}`}</UsernameTitle> */}
            <span>&nbsp;</span>
            {/* <img src={FollowImage} width="20" height="20"/> */}
            {/* <img src={UnfollowImage} width="20" height="20" /> */}
          </NameContainer>
          {this.props.replyingTo && (
            <PostHeaderText>{`replying to @${
              this.props.replyingTo
            }`}</PostHeaderText>
          )}
          {this.props.tip && (
            <PostHeaderText>{`@${this.props.tip.creator} tipped ${this.props.tip
              .amount / APP_TOKEN_UNIT} APP`}</PostHeaderText>
          )}
          <DescriptionText>
            <Linkify
              properties={{
                target: '_blank',
                onClick: function onClick(e) {
                  e.stopPropagation();
                },
              }}
            >
              {contentHtml}
            </Linkify>
          </DescriptionText>
          {this.props.showReplyHint ? (
            <ReplyHintText>
              {this._getReplyHintText()}
              <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
            </ReplyHintText>
          ) : null}
          <StyledA
            href={`https://bloks.io/transaction/${stamp.transaction_id}`}
            target="_blank"
            onClick={e => e.stopPropagation()}
          >
            <img src={StampImage} width="20" height="20" />
            Stamped
            <span>&nbsp;</span>
            <Moment format="hh:mm A on MMM DD, YYYY ">
              {new Date(`${stamp.timestamp}Z`)}
            </Moment>
          </StyledA>
        </ContentContainer>
        {this.props.api.isLoggedIn
          ?
          <VoteContainer onClick={this._onTip}>
            <ArrowBlock>
              <svg height="15" width="15">
                // Disable if user has already tipped this stamp
                {this._hasUserTippedStamp(this.state.stamp.transaction_id, this.props.api.getUsername())}
                {tipIcon}
              </svg>
            </ArrowBlock>
            {tip}
          </VoteContainer>
          :
          <VoteContainer>
            <ArrowBlock>
              <svg height="15" width="15">
                <StyledPolygonDisabled points="0,15 15,15 7.5,0" />
              </svg>
            </ArrowBlock>
            {tip}
          </VoteContainer>
        }
      </Container>
    );
  }
}

PostContents.defaultProps = {
  showReplyHint: true,
};

export default PostContents;
