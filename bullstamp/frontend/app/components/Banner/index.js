/*
 * Main background banner below nav bar across most screens.
 */


import React from 'react';
import styled from 'styled-components';
import ResponsiveContainer from 'components/ResponsiveContainer';
import Themes from 'constants/themes';

const Container = styled.div`
  align-items: center;
  background-color: ${props => props.theme.primaryColor};
  display: flex;
  height: 200px;
  width: 100%;
`;

Container.defaultProps = {
  theme: Themes.main,
};

export default class Banner extends React.PureComponent {
 render() {
   return <Container />;
 }
};
