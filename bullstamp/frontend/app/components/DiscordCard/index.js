import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';
import discordlogo from '../../images/discordlogo.png';

const StyledCard = styled(Card)`
  height: 300px;
  width: 260px;
  align-items: stretch;
  align-self: flex-start;
  flex-direction: column;
  @media (max-width: 1000px) {
    display: none;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  margin: auto;
`;

const Title = styled.span`
  font-weight: bold;
  align-self: flex-start;
`;

export default class DiscordCard extends React.Component {
  render() {
    return (
      <StyledCard className={this.props.className}>
        <Container>
          <Title>Sign Up</Title>
          <ImageContainer>
            <a href="https://discord.gg/qv7dNJX" target="_blank">
              <img src={discordlogo} width="150" height="150" />
            </a>
          </ImageContainer>
          <h5 align="center">Join the Discord <br/> and Ask to Sign Up</h5>
        </Container>
      </StyledCard>
    );
  }
}
