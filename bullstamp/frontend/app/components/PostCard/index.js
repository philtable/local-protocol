import React from 'react';
import {withRouter} from 'react-router';
import styled from 'styled-components';
import PostContents from 'components/PostContents';
import Card from 'components/Card';
import Themes from 'constants/themes';

const StyledCard = styled(Card)`
  :hover {
    background-color: #fafafa;
    cursor: pointer;
  }
`;

class PostCard extends React.Component {

  _onClick = (event) => {
    this.props.stamp.reply_to === null
      ? window.open(`/stamp/${this.props.stamp.transaction_id}`, "_blank")
      : window.open(`/stamp/${this.props.stamp.reply_to}`, "_blank")
  };

  render() {
    const {className, ...otherProps} = this.props;
    return (
      <StyledCard className={className} onClick={this._onClick}>
        <PostContents {...otherProps} />
      </StyledCard>
    );
  }
};

export default withRouter(PostCard);
