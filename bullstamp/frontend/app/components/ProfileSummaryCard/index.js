/*
 * Profile summary card component.
 */

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import ProfilePicture from 'components/ProfilePicture';
import UploadPictureButton from 'components/UploadPictureButton';
import Button from 'components/Button'
import Card from 'components/Card';
import { followUser, unfollowUser, getUserFollowingTargetUser } from '../../utils/api';

const StyledCard = styled(Card)`
  height: 300px;
  width: 260px;
  align-items: stretch;
  align-self: flex-start;
  flex-direction: column;
  @media (max-width: 880px) {
    display: none;
  }
`;

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
`;

const ProfileTitle = styled.span`
  font-weight: bold;
  margin-bottom: 12px;
  align-self: flex-start;
`;

const UsernameTitle = styled.span`
  margin-top: 12px;
`;

const Divider = styled.div`
  align-self: stretch;
  background-color: #e8e8e8;
  height: 1px;
  margin-top: 8px;
  margin-bottom: 8px;
`;

const StatsContainer = styled.div`
  align-self: stretch;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

export default class ProfileSummaryCard extends React.Component {
  state = {
    balance: '0.0000 APP',
    isFollowingUser: false,
  };

  componentWillMount() {
    document.addEventListener('reloadBalances', this.updateBalances);
  }

  componentDidMount() {
    this.updateBalances();
  }

  componentWillUnmount() {
    document.removeEventListener('reloadBalances', this.updateBalances);
  }

  updateBalances = e => {
    if (e && e.amount) {
      const currentBalance = parseFloat(this.state.balance.split(' ')[0]);
      const newBalance = (currentBalance + e.amount).toFixed(4);
      this.setState({ balance: `${newBalance} APP` });
    }
    this.props.api.getAppTokenBalance().then(balance => {
      this.setState({
        balance,
      });
    });
  };

  _getPostsStatText() {
    const count = this.props.postCount || 10;
    return `${count} stamp${count === 1 ? '' : 's'}`;
  }

  _getTipsStatText() {
    const count = this.props.postCount || 20;
    return `${count} tip${count === 1 ? '' : 's'}`;
  }

  _isUserFollowing(user, target_user) {
    getUserFollowingTargetUser(user, target_user).then(json => {
      if (json.following == "true") {
        this.setState(prevState => ({
          isFollowingUser: true,
        }));
      }
    });
  }

  _followTargetUser(username) {
    followUser(username);
    this.setState({ isFollowingUser: true });
  }

  _unfollowTargetUser(username) {
    unfollowUser(username);
    this.setState({ isFollowingUser: false });
  }

  render() {
    const { api, className, username } = this.props;
    const userInfo = <Fragment>{this.state.balance}</Fragment>;

    const FollowButton = () => (
      <Button
        text="Follow"
        onClick={ () => this._followTargetUser(username) }>
      </Button>
    )

    const UnfollowButton = () => (
      <Button
        text="Unfollow"
        onClick={ () => this._unfollowTargetUser(username) }>
      </Button>
    )

    // https://stackoverflow.com/questions/2855589/replace-input-type-file-by-an-image
    return (
      <StyledCard className={className}>
        <Container>
          <ProfileTitle>Profile</ProfileTitle>
          <ProfilePicture author={username} />
          {api.getUsername() === username && <UploadPictureButton api={api} className={className}/> }
          <Link
            to={{ pathname: `/bull/${username}` }}
            style={{ textDecoration: 'none' }}
          >
            <UsernameTitle>@{username}</UsernameTitle>
          </Link>
          <Divider />
          {this._isUserFollowing(api.getUsername(), username)}
          {api.getUsername() === username
            ? userInfo
            : this.props.api.isLoggedIn
              ? this.state.isFollowingUser
                ? <UnfollowButton />
                : <FollowButton />
              : null }
          <StatsContainer>
            {/*
              <span>{this._getPostsStatText()}</span>
              <span>{this._getTipsStatText()}</span>
            */}
          </StatsContainer>
        </Container>
      </StyledCard>
    );
  }
}

ProfileSummaryCard.propTypes = {
  api: PropTypes.object,
  username: PropTypes.string,
  className: PropTypes.string,
};
