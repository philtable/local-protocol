/*
 * Main nav bar shared across most screens on the site.
 */

import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import ResponsiveContainer from 'components/ResponsiveContainer';
import Button from 'components/Button';
import UploadPictureButton from 'components/UploadPictureButton';
import Themes from 'constants/themes';
import Logo from '../../images/bullstamplogo.png';
import Settings from '../../images/settings.png';
import NotificationNew from '../../images/notification-new.png';
import NotificationNone from '../../images/notification-none.png';
import { getNotifications } from '../../utils/api';

const Container = styled.div`
  align-items: stretch;
  background-color: white;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25);
  display: flex;
  height: 45px;
  position: fixed;
  top: 0px;
  width: 100%;
  z-index: 10;
`;

const StyledResponsiveContainer = styled(ResponsiveContainer)`
  display: flex;
  justify-content: space-between;
  align-items: stretch;
`;

const LogoName = styled.span`
  font-size: 18px;
  @media (max-width: 880px) {
    display: none;
  }
`;

const StyledLink = styled(Link)`
  display: flex;
  align-items: center;
  text-decoration: initial;
  color: initial;
  font-size: 35px;

  padding: 7px;

  transition-timing-function: ease;
  transition: 0.15s;

  :hover {
    background-color: #dcdcdc;
  }
`;

const StyledNavGroup = styled.div`
  margin: 0.1em;
  margin-top: 8px;
  border: 2px solid ${props => props.theme.primaryColor};
  border-radius: 6px;
  width: 130px;
  height: 30px;
`;

StyledNavGroup.defaultProps = {
  theme: Themes.main,
};

const StyledNewButton = styled(Link)`
  text-decoration: initial;
  text-align: center;
  float: left;
  padding: 0.4em;
  width: 50%;
  height: 100%;
  font-size: 12px;

  color: ${props => props.colorone};
  border-right: 2px solid ${Themes.main.primaryColor};
  background-color: ${props => props.colortwo};

  transition-timing-function: ease;
  transition: 0.15s;

  :hover {
    color: white;
    background-color: ${Themes.main.primaryColor};
  }
`;

const StyledTrendButton = styled(Link)`
  text-decoration: initial;
  text-align: center;
  float: left;
  padding: 0.4em;
  width: 50%;
  height: 100%;
  font-size: 12px;

  color: ${props => props.colorone};
  background-color: ${props => props.colortwo};

  transition-timing-function: ease;
  transition: 0.15s;

  :hover {
    color: white;
    background-color: ${Themes.main.primaryColor};
  }
`;

const TokenBalance = styled.div`
  padding-right: 5px;

  @media (max-width: 880px) {
    display: none;
  }
`;

const StyledLoginButton = styled(Button)`
  background-color: red;
`;

const VerticalCenterContainer = styled.div`
  float: right;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const SettingsTab = styled.div`
  color: white;
  padding: 7px;
  font-size: 16px;
  border: none;
  cursor: pointer;
`;

const Dropdown = styled.div`
  position: relative;
  display: inline-block;
`;

const DropdownContent = styled.div`
  display: none;
  position: absolute;
  right: 0px;
  background-color: #f9f9f9;
  min-width: 170px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;

  ${Dropdown}:hover & {
   display: block;
  }
`;

const DropdownStatic = styled.div`
  color: black;
  padding: 12px 16px;
  text-align: center;
  display: none;

  @media (max-width: 880px) {
    display: block;
  }
  `;

const DropdownItem = styled.div`
  color: black;
  padding: 12px 16px;
  display: block;

  :hover {
    background-color: #dcdcdc;
  }
  `;

export default class NavBar extends React.Component {
  state = {
    balance: '0.0000 APP',
    hasNotifications: false,
  };

  handleNewStamp = newStamp => {
    if (newStamp.author === this.props.api.getUsername()) {
      return; // don't notify on self actions
    }
    this.setState({ hasNotifications: true });
    const notif = { type: 'STAMP', data: newStamp };
    const evt = new Event('renderNotification');
    evt.data = notif;
    document.dispatchEvent(evt);
  };

  handleNewTip = newTip => {
    if (newTip.author === this.props.api.getUsername()) {
      return; // don't notify on self actions
    }
    this.setState({ hasNotifications: true });
    const notif = { type: 'TIP', data: newTip };
    const evt = new Event('renderNotification');
    evt.data = notif;
    document.dispatchEvent(evt);
  };

  componentWillMount() {
    document.addEventListener('reloadBalances', this.updateBalances);
    document.addEventListener('notificationsRead', this.hideNotifications);
    this.stampListenerId = this.props.api.addStampListener(this.handleNewStamp);
    this.tipListenerId = this.props.api.addTipListener(this.handleNewTip);
  }

  componentDidMount() {
    this.updateBalances();
  }

  componentWillUnmount() {
    this.props.api.removeStampListener(this.stampListenerId);
    document.removeEventListener('reloadBalances', this.updateBalances);
    document.removeEventListener('notificationsRead', this.hideNotifications);
  }

  // Check for unread notifications to trigger notificationImage
  checkNotifications() {
    const myUsername = this.props.api.getUsername();
    getNotifications(myUsername, 1).then(json => {
      json.notifications[0] === undefined ||
      json.notifications[0].is_read === true
        ? null
        : this.setState({ hasNotifications: true });
    });
  }

  hideNotifications = () => {
    this.setState({ hasNotifications: false });
  };

  updateBalances = e => {
    if (e && e.amount) {
      const currentBalance = parseFloat(this.state.balance.split(' ')[0]);
      const newBalance = (currentBalance + e.amount).toFixed(4);
      this.setState({ balance: `${newBalance} APP` });
    }
    this.props.api.getAppTokenBalance().then(balance => {
      console.log('got balance', balance);
      this.setState({
        balance,
      });
    });
  };

  render() {
    // this.props.api.isLoggedIn
    //   ? this.checkNotifications()
    //   : null
    const notificationImage = this.state.hasNotifications
      ? NotificationNew
      : NotificationNone;

    let firstButton;

    if (this.props.currentTab === 'new') {
      firstButton = (
        <StyledNewButton
          colorone="white"
          colortwo={Themes.main.primaryColor}
          to={{ pathname: `/home` }}
        >
          New
        </StyledNewButton>
      );
    } else if (this.props.currentTab === 'stamps') {
      firstButton = (
        <StyledNewButton
          colorone="white"
          colortwo={Themes.main.primaryColor}
          to={{ pathname: `/bull/${this.props.bull}` }}
        >
          Stamps
        </StyledNewButton>
      );
    } else if (this.props.currentTab === 'tips') {
      firstButton = (
        <StyledNewButton
          colorone={Themes.main.primaryColor}
          colortwo="transparent"
          to={{ pathname: `/bull/${this.props.bull}` }}
        >
          Stamps
        </StyledNewButton>
      );
    } else {
      firstButton = (
        <StyledNewButton
          colorone={Themes.main.primaryColor}
          colortwo="transparent"
          to={{ pathname: `/home` }}
        >
          New
        </StyledNewButton>
      );
    }

    let lastButton;

    if (this.props.currentTab === 'trending') {
      lastButton = (
        <StyledTrendButton
          colorone="white"
          colortwo={Themes.main.primaryColor}
          to={{ pathname: `/trending` }}
        >
          Trending
        </StyledTrendButton>
      );
    } else if (this.props.currentTab === 'stamps') {
      lastButton = (
        <StyledTrendButton
          colorone={Themes.main.primaryColor}
          colortwo="transparent"
          to={{ pathname: `/bull/${this.props.bull}/tips` }}
        >
          Tips
        </StyledTrendButton>
      );
    } else if (this.props.currentTab === 'tips') {
      lastButton = (
        <StyledTrendButton
          colorone="white"
          colortwo={Themes.main.primaryColor}
          to={{ pathname: `/bull/${this.props.bull}/tips` }}
        >
          Tips
        </StyledTrendButton>
      );
    } else {
      lastButton = (
        <StyledTrendButton
          colorone={Themes.main.primaryColor}
          colortwo="transparent"
          to={{ pathname: `/trending` }}
        >
          Trending
        </StyledTrendButton>
      );
    }

    const FinalNavGroup = (
      <StyledNavGroup>
        {firstButton}
        {lastButton}
      </StyledNavGroup>
    );

    return (
      <Container>
        <StyledResponsiveContainer>
          <StyledLink
            onClick={
              window.location.pathname === '/' ? this.forceUpdate : () => {}
            }
            to={{ pathname: `/home` }}
          >
            <img src={Logo} alt="Bullstamp Logo" width="50" height="50" />
            <LogoName>BullStamp BETA</LogoName>
          </StyledLink>

          {FinalNavGroup}

          {!this.props.api.isLoggedIn ? (
            <VerticalCenterContainer>
              <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
              <StyledLoginButton
                text="Login"
                onClick={() => (window.location.href = '/')}
              />
            </VerticalCenterContainer>
          ) : (
            <VerticalCenterContainer>

              <TokenBalance>
                {this.state.balance}
              </TokenBalance>

              <Fragment>

                <Dropdown>
                  <SettingsTab>
                    <img src={Settings} width="30" height="30" />
                  </SettingsTab>
                  <DropdownContent>
                    <DropdownStatic>{this.state.balance}</DropdownStatic>
                    <DropdownItem>
                      <UploadPictureButton api={this.props.api} />
                    </DropdownItem>
                  </DropdownContent>
                </Dropdown>


                <StyledLink to={{ pathname: `/notifications` }}>
                  <img src={notificationImage} width="30" height="30" />
                </StyledLink>
              </Fragment>
            </VerticalCenterContainer>
          )}
        </StyledResponsiveContainer>
      </Container>
    );
  }
}
