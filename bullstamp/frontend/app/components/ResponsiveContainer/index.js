/*
 * Basic responsive container that constrains width based on device width.
 */

import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
  align-items: stretch;
  display: flex;
  flex-direction: ${props => props.flexDirection};
  padding-left: 14px;
  padding-right: 14px;
  width: 100%;

  @media (min-width: 768px) {
    width: 90%;
    margin-right: auto;
    margin-left: auto;
  }
`;

Container.defaultProps = {
  flexDirection: 'row',
};

export default class ResponsiveContainer extends React.Component {
  render() {
    return (
      <Container {...this.props}>
        {this.props.children}
      </Container>
    );
  }
}
