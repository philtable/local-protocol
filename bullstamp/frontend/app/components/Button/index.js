/*
 * Basic themed button.
 */

import React from 'react';
import styled from 'styled-components';
import Themes from 'constants/themes';

const Container = styled.div`
  background-color: ${props =>
    props.isDisabled ? '#bdbdbd' : props.theme.primaryColor};
  border-radius: 50px;
  color: white;
  padding-left: 14px;
  padding-right: 14px;
  padding-top: 4px;
  padding-bottom: 4px;
  user-select: none;

  :hover {
    background-color: ${props =>
    props.isDisabled ? '#c7c7c7' : props.theme.primaryColorDark};
    cursor: ${props => (props.isDisabled ? 'not-allowed' : 'pointer')};
  }
`;

Container.defaultProps = {
  theme: Themes.main,
};

export default class Button extends React.Component {
  static defaultProps = {
    isDisabled: false,
  };

  _onClick = () => {
    if (!this.props.isDisabled && this.props.onClick) {
      this.props.onClick();
    }
  };

  render() {
    const { text, className, isDisabled } = this.props;
    return (
      <Container
        className={className}
        isDisabled={isDisabled}
        onClick={this._onClick}
      >
        {text}
      </Container>
    );
  }
}
