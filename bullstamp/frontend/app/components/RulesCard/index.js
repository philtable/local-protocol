import React from 'react';
import styled from 'styled-components';
import Card from 'components/Card';

const StyledCard = styled(Card)`
  height: 300px;
  width: 260px;
  align-items: stretch;
  align-self: flex-start;
  flex-direction: column;
  @media (max-width: 1000px) {
    display: none;
  }
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const ImageContainer = styled.div`
  margin: auto;
`;

const Title = styled.span`
  font-weight: bold;
  align-self: flex-start;
`;

export default class RulesCard extends React.Component {
  render() {
    return (
      <StyledCard className={this.props.className}>
        <Container>
          <Title>Rules</Title>
          <ul>
            <li>NO flooding or pointless posts</li>
            <li>Use cashtags like $BTC, $ETH, $EOS</li>
            <li>Do NOT mention BullStamp, we want slow and steady growth. </li>
          </ul>
        </Container>
      </StyledCard>
    );
  }
}
