#!/usr/bin/env bash

# set up bash to handle errors more aggressively - a "strict mode" of sorts
set -e # give an error if any command finishes with a non-zero exit code
set -u # give an error if we reference unset variables
set -o pipefail # for a pipeline, if any of the commands fail with a non-zero exit code, fail the entire pipeline with that exit code

pushd ~/frontend

git reset --hard
git clean -fd
git fetch origin
git reset --hard origin/master

npm install
npm run build

tmux kill-session -t frontend || true # kill old tmux session, if present
tmux new-session -d -s frontend 'PORT=10000 npm run start:prod' # start a new tmux session, but don't attach to it just yet

popd

echo 'BULLSTAMP FRONTEND SERVER STARTED, MONITOR WITH tmux attach -t frontend'
