Bullstamp and Apptoken
======================

Important links:

* [Architecture diagram](https://docs.google.com/drawings/d/1qcJeL2kR491B9KbfjQvJHypBcxUh7fVvchNxMbX1IPQ/edit) ([mirror](bullstamp-data-flow.pdf))

Bullstamp: Crypto Microblogging (Built on App Protocol)

* [bullstamp frontend](https://gitlab.com/bullstamp/frontend) 104.248.208.47 bullstamp.co
* [bullstamp backend](https://gitlab.com/bullstamp/backend) 206.189.219.234 api.bullstamp.co

App Protocol: Data Protocol for Microblogging (Neutral to any clients built on top)

* [apptokenserver](https://gitlab.com/apptoken/apptokenserver) 142.93.21.161 api.apptoken.co
* [managedeos](https://gitlab.com/apptoken/managedeos) 104.248.208.210 managedeos.apptoken.co

Project Management
* [Trello](https://trello.com/b/ke9thp8M/apptoken)

Developing Bullstamp and Apptoken
---------------------------------

Developing on Ubuntu 18.04.

### Setting up your environment

```bash
# First time
git clone https://gitlab.com/bullstamp/bullstamp.git
git config user.name 'YOUR_GITLAB_BURNER_ACCOUNT_USERNAME'
git config user.email 'john@doe.com'
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# Every time you start developing
source venv/bin/activate
```

Sanitize author and commiter information in repository (use this command in case a developer accidentally commits under their real name): `git filter-branch -f --env-filter 'export GIT_AUTHOR_NAME="dev-eos" GIT_AUTHOR_EMAIL="39096428+dev-eos@users.noreply.github.com" export GIT_COMMITTER_NAME="dev-eos" export GIT_COMMITTER_EMAIL="39096428+dev-eos@users.noreply.github.com"'`

Deploying Bullstamp and Apptoken
--------------------------------

### Credentials

Get the following credentials from a developer:

* Namecheap.com account.
* DigitalOcean.com account.

Generate a SSH key and give it to one of the developers so you can get SSH access

### Servers

Check out DigitalOcean for a list of servers and what domains resolve to

### Bullstamp Frontend

Frontend in user's browser connects to Bullstamp backend server via HTTP (for most requests) and via websockets using Socket.IO (for realtime updates).

This is hosted on a Ubuntu 18.04 DigitalOcean droplet named `frontend-01`. Here's an outline of the setup procedure:

* SSH into the machine: `ssh root@bullstamp.co`.
* Copy bullstamp/frontend/init.sh and run it
* Start/restart the server: `~/frontend/start.sh`

Useful commands when SSH'd in:

* Monitor Bullstamp frontend output: `tmux attach -t frontend`.

### Bullstamp Backend

This serves `https://api.bullstamp.co` as well as `ws://api.bullstamp.co:5557` (using Socket.IO), and makes requests to `https://api.apptoken.co`.

Bullstamp backend server connects to Apptoken server via ZeroMQ to receive Bullstamp updates.

This is hosted on a Ubuntu 18.04 DigitalOcean droplet named `backend`. Here's an outline of the setup procedure:

* SSH into the machine: `ssh root@api.bullstamp.co`.
* Copy bullstamp/backend/init.sh and run it
* Start/restart the server: `bash ~/backend/start.sh`

Useful commands when SSH'd in:

* Monitor Bullstamp backend output: `tmux attach -t backend`.
* Database console: `sudo -u postgres psql -d bullstamp`.

#### Run Bullstamp Backend Locally

Run a Virtual Environment and Install Dependencies
* `python3 -m pip install --user virtualenv`
* `python3 -m virtualenv env`
* `source env/bin/activate`
* `pip install -r requirements.txt`

Setup PostgreSQL
* `brew services start postgresql`
* `psql postgres`
* `\du` (use this to find your role name)
* Enter CTRL-D
* `psql postgres -U <role name>`
* `create role postgres;`
* `create database bullstamp;`
* `alter role postgres with login;`
* `grant all privileges on database bullstamp to postgres;`
* `grant select, insert, update, delete on all tables in schema public to postgres;`
* `grant usage, select on all sequences in schema public to postgres;`
* Enter CTRL-D

Import Models, Create Test Data and Start the Server
* `python3 -c 'from models import db; db.create_all()'`
* `python3 setup_db.py`
* `python3 server.py`


### ManagedEOS 

This serves `https://managedeos.apptoken.co` and handles user accounts for those who don't have Scatter installed (managed EOS accounts, essentially)

This is hosted on a Ubuntu 18.04 DigitalOcean droplet named `managedeos`. Here's an outline of the setup procedure:

* SSH into the machine: `ssh root@managedeos.apptoken.co`.
* Copy apptoken/managedeos/init.sh and run it
* Start/restart the server: `bash ~/managedeos/start.sh`

Useful commands when SSH'd in:

* Monitor Bullstamp backend output: `tmux attach -t managedeos`.
* Database console: `sudo -u postgres psql -d managedeos`.

#### Run ManagedEOS Locally

Install Golang
* https://golang.org/doc/install

Setup PostgreSQL
* `brew services start postgresql`
* `psql postgres`
* `\du` (use this to find your role name)
* Enter CTRL-D
* `psql postgres -U <role name>`
* `create role postgres;`
* `create database managedeos;`
* `alter role postgres with login;`
* `grant all privileges on database managedeos to postgres;`
* `grant select, insert, update, delete on all tables in schema public to postgres;`
* `grant usage, select on all sequences in schema public to postgres;`
* Enter CTRL-D

Run on Jungle Testnet
* `go run cmd/managedeos/main.go`

Run on EOS Mainnet
* `ENVIRONMENT=production go run cmd/managedeos/main.go`


### Apptoken Server

This serves `https://api.apptoken.co` and `tcp://api.apptoken.co:5556` (ZeroMQ broadcast).

Apptoken server consists of `blockchain_listener.py`, which:

* Listens to the EOS blockchain for Bullstamp-related events.
* Stores those events in the Apptoken DB.
* Broadcasts those events on ZeroMQ.

This is hosted on a Ubuntu 18.04 DigitalOcean droplet named `apptokenserver`. Here's an outline of the setup procedure:

* SSH into the machine: `ssh root@api.apptoken.co`.
* Copy apptoken/apptokenserver/init.sh and run it
* Start/restart blockchain listener and server: `bash ~/apptokenserver/start.sh`

Useful commands when SSH'd in:

* Monitor blockchain listener output: `tmux attach -t blockchain_listener`
* Monitor apptoken server output: `tmux attach -t apptokenserver`
* Database console: `sudo -u postgres psql -d apptoken`

#### Run Apptoken Server Locally

Run a Virtual Environment and Install Dependencies
* `python3 -m pip install --user virtualenv`
* `python3 -m virtualenv env`
* `source env/bin/activate`
* `pip install -r requirements.txt`

Setup PostgreSQL
* `brew services start postgresql`
* `psql postgres`
* `\du` (use this to find your role name)
* Enter CTRL-D
* `psql postgres -U <role name>`
* `create role postgres;`
* `create database apptoken;`
* `alter role postgres with login;`
* `grant all privileges on database apptoken to postgres;`
* `grant select, insert, update, delete on all tables in schema public to postgres;`
* `grant usage, select on all sequences in schema public to postgres;`
* Enter CTRL-D

Import Models, Create Test Data and Start the Server
* `python3 -c 'from models import db; db.create_all()'`
* `python3 setup_db.py`
* `python3 server.py`