module gitlab.com/apptoken/managedeos

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/eoscanada/eos-go v3.0.0+incompatible
	github.com/facebookgo/ensure v0.0.0-20160127193407-b4ab57deab51 // indirect
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/facebookgo/subset v0.0.0-20150612182917-8dac2c3c4870 // indirect
	github.com/golang-migrate/migrate/v3 v3.5.2
	github.com/gorilla/mux v1.6.2
	github.com/gorilla/securecookie v1.1.1
	github.com/gorilla/sessions v1.1.3
	github.com/jmoiron/sqlx v0.0.0-20180614180643-0dae4fefe7c0
	github.com/jteeuwen/go-bindata v3.0.7+incompatible // indirect
	github.com/lib/pq v1.0.0
	github.com/mailgun/mailgun-go v1.1.1
	github.com/pborman/uuid v1.2.0
	github.com/pkg/errors v0.8.0
	github.com/rs/cors v1.6.0
	github.com/tidwall/gjson v1.1.3 // indirect
	github.com/tidwall/match v0.0.0-20171002075945-1731857f09b1 // indirect
	github.com/tidwall/sjson v1.0.2 // indirect
	golang.org/x/crypto v0.0.0-20181001203147-e3636079e1a4
	gopkg.in/yaml.v2 v2.2.1
)
