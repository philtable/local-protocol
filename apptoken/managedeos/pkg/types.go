package pkg

type User struct {
	Id        		uint64  `json:"id"`
	Username  		*string `json:"username"`
	Email     		string  `json:"email"`
	Admin     		bool    `json:"admin"`
	BetaUser  		bool    `json:"beta_user"`
	PublicKey 		string  `json:"public_key"`
	EmailVerified bool 		`json:"email_verified"`
}

type EmailVerification struct {
	Id					uint64	`json:"id"`
	Timestamp		string	`json:"timestamp"`
	UserId			uint64	`json:"user_id"`
	Email				string 	`json:"email"`
	Token				string	`json:"token"`
	Valid				bool		`json:"valid"`
}

type PasswordReset struct {
	Id					uint64	`json:"id"`
	Timestamp		string	`json:"timestamp"`
	UserId			uint64	`json:"user_id"`
	Email				string 	`json:"email"`
	Token				string	`json:"token"`
	Valid				bool		`json:"valid"`
}
