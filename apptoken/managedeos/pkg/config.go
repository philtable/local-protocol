package pkg

import (
	"gitlab.com/apptoken/managedeos/configs"
	"gopkg.in/yaml.v2"
	"os"
)

type AccountCreator struct {
	Username   string  `yaml:"username"`
	PrivateKey string  `yaml:"private_key"`
	RAM        uint32  `yaml:"ram"`
	CPU        float64 `yaml:"cpu"`
	Net        float64 `yaml:"net"`
}

type Network struct {
	TokenAccount string `yaml:"token_account"`
	AppAccount   string `yaml:"app_account"`
	APIUrl       string `yaml:"api_url"`
	ChainID      string `yaml:"chain_id"`
}

type Mailer struct {
	APIKey       string `yaml:"api_key"`
	PublicAPIKey string `yaml:"public_api_key"`
	Domain       string `yaml:"domain"`
	From         string `yaml:"from"`
}

type Database struct {
	User       string `yaml:"user"`
	Password   string `yaml:"password"`
	Host       string `yaml:"host"`
	Port       uint   `yaml:"port"`
	Database   string `yaml:"database"`
	ExtraFlags string `yaml:"extra_flags"`
}

type Config struct {
	AccountCreator AccountCreator `yaml:"account_creator"`
	Network        Network        `yaml:"network"`
	Mailer         Mailer         `yaml:"mailer"`
	Database       Database       `yaml:"database"`
	SecretKey      string         `yaml:"secret_key"`
}

func LoadConfig() *Config {
	env := os.Getenv("ENVIRONMENT")
	if env == "" {
		env = "development"
	}

	var config map[string]*Config
	err := yaml.Unmarshal(configs.MustAsset("config.yaml"), &config)
	if err != nil {
		panic(err)
	}

	return config[env]
}
