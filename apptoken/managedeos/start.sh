#!/usr/bin/env bash

# set up bash to handle errors more aggressively - a "strict mode" of sorts
set -e # give an error if any command finishes with a non-zero exit code
set -u # give an error if we reference unset variables
set -o pipefail # for a pipeline, if any of the commands fail with a non-zero exit code, fail the entire pipeline with that exit code

pushd ~/go/src/gitlab.com/apptoken/managedeos

git reset --hard
git clean -fd
git fetch origin
git reset --hard origin/master

GOPATH="/root/go" GO111MODULE=off go build cmd/managedeos/main.go

export ENVIRONMENT=production
tmux send-keys C-c -t managedeos || true # kill old tmux session, if present
tmux new-session -d -s managedeos './main 2>&1 | tee -a /root/console.log' # start a new tmux session, but don't attach to it just yet

popd

echo 'MANAGED EOS SERVER STARTED, MONITOR WITH tmux attach -t managedeos'
