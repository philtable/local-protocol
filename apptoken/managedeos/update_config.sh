#!/bin/bash

pushd configs
go-bindata -pkg configs .
popd

pushd internal/database/migrations
go-bindata -pkg migrations .
popd
