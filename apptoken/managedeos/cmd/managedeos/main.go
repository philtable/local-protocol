package main

import (
	"gitlab.com/apptoken/managedeos/internal"
	"gitlab.com/apptoken/managedeos/pkg"
	"log"
	"net/http"
)

func main() {
	log.Println("starting server")
	server := internal.NewServer(pkg.LoadConfig())

	log.Println("listening")
	log.Fatal(http.ListenAndServe("127.0.0.1:8080", server.Mux))
}
