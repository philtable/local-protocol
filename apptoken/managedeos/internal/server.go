package internal

import (
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"gitlab.com/apptoken/managedeos/internal/v1"
	"gitlab.com/apptoken/managedeos/pkg"
	"net/http"
)

type Server struct {
	config *pkg.Config

	Mux http.Handler
}

type a struct {
	f http.HandlerFunc
}

func (aa *a) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	aa.f(w, r)
}

func NewServer(config *pkg.Config) *Server {
	server := &Server{
		config: config,
	}

	c := cors.New(cors.Options{
		AllowedOrigins: []string{"https://bullstamp.co", "http://127.0.0.1:3000"},
		AllowedHeaders: []string{"*"},
		ExposedHeaders: []string{"Set-Authorization"},
		AllowCredentials: true,
	})

	r := mux.NewRouter()
	r.NewRoute().PathPrefix("/api/v1").Handler(http.StripPrefix("/api/v1", v1.NewApiV1(config).Mux))

	server.Mux = c.Handler(r)

	return server
}
