package database

import (
	"errors"
	"github.com/eoscanada/eos-go/ecc"
	"gitlab.com/apptoken/managedeos/pkg"
)

type User struct {
	Id              		uint64 `json:"id" db:"id"`
	Username        		*string `json:"username" db:"username"`
	Password        		string `json:"password,omitempty" db:"password"`
	Email           		string `json:"email" db:"email"`
	PrivateKey      		string `json:"private_key" db:"private_key"`
	Admin           		bool   `json:"admin" db:"admin"`
	BetaUser        		bool   `json:"beta_user" db:"beta_user"`
	SettingUsername 		bool   `json:"setting_username" db:"setting_username"`
	EmailVerified				bool 	 `json:"email_verified" db:"email_verified"`
}

type EmailVerification struct {
	Id					uint64	`json:"id" db:"id"`
	Timestamp		string	`json:"timestamp" db:"timestamp"`
	UserId			uint64	`json:"user_id" db:"user_id"`
	Email				string	`json:"email" db:"email"`
	Token				string	`json:"token" db:"token"`
	Valid				bool		`json:"valid" db:"valid"`
}

type PasswordReset struct {
	Id					uint64	`json:"id" db:"id"`
	Timestamp		string	`json:"timestamp" db:"timestamp"`
	UserId			uint64	`json:"user_id" db:"user_id"`
	Email				string	`json:"email" db:"email"`
	Token				string	`json:"token" db:"token"`
	Valid				bool		`json:"valid" db:"valid"`
}

func (u *User) GetPrivateKey() *ecc.PrivateKey {
	k, e := ecc.NewPrivateKey(u.PrivateKey)
	if e != nil {
		panic(e)
	}
	return k
}

func (u *User) GetPublicKey() ecc.PublicKey {
	return u.GetPrivateKey().PublicKey()
}

func (u *User) ToExternalUser() *pkg.User {
	return &pkg.User{
		Id: u.Id,
		Username: u.Username,
		Email: u.Email,
		Admin: u.Admin,
		BetaUser: u.BetaUser,
		EmailVerified: u.EmailVerified,
		PublicKey: u.GetPublicKey().String(),
	}
}

func (e *EmailVerification) ToExternalEmailVerification() *pkg.EmailVerification {
	return &pkg.EmailVerification{
		Id: e.Id,
		Timestamp: e.Timestamp,
		UserId: e.UserId,
		Email: e.Email,
		Token: e.Token,
		Valid: e.Valid,
	}
}

func (p *PasswordReset) ToExternalRequestPasswordReset() *pkg.PasswordReset {
	return &pkg.PasswordReset{
		Id: p.Id,
		Timestamp: p.Timestamp,
		UserId: p.UserId,
		Email: p.Email,
		Token: p.Token,
		Valid: p.Valid,
	}
}

var ErrUserNotFound = errors.New("user not found")
var ErrEmailVerificationNotFound = errors.New("email verification not found")
var ErrPasswordResetNotFound = errors.New("password reset not found")
var ErrUserExists = errors.New("user exists")
var ErrNoRowsUpdated = errors.New("no rows updated")
var ErrEmailVerificationExists = errors.New("email verification exists")
