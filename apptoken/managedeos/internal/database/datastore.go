package database

type Datastore interface {
	ListUsers() ([]*User, error)

	CreateUser(email string, password string, private_key string) (*User, error)
	CreateEmailVerification(user_id uint64, email string, token string) (*EmailVerification, error)
	CreatePasswordReset(user_id uint64, email string, token string) (*PasswordReset, error)

	UpdateUsername(id uint64, username string) (*User, error)
	UpdateBeta(id uint64, beta bool) (*User, error)
	UpdatePassword(uid uint64, password string) (*User, error)
	UpdateEmailVerified(uid uint64, email_verified bool) (*User, error)
	UpdateEmailVerificationValid(eid uint64, email_verified bool) (*EmailVerification, error)

	GetUserByEmail(email string) (*User, error)
	GetUserByUsername(username string) (*User, error)
	GetUserById(id uint64) (*User, error)
	GetEmailVerificationById(id uint64) (*EmailVerification, error)
	GetEmailVerificationByToken(token string) (*EmailVerification, error)
	GetPasswordResetByToken(token string) (*PasswordReset, error)

	SetIsSettingUsername(uid uint64, isSettingUsername bool, oldValue bool) error
}
