package migrations

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"strings"
)

func bindata_read(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	return buf.Bytes(), nil
}

var __1_init_up_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x94\x91\x31\x4f\xc3\x30\x10\x85\x77\xff\x8a\x1b\x63\x89\x81\x22\xc1\x92\x09\x41\x06\x96\x22\x21\x06\xb6\xe8\xd2\x5c\xc1\xaa\xe3\x84\xf3\xb9\x34\xff\x1e\xc5\x25\x26\x31\x03\xaa\x27\xeb\xf9\x7b\xcf\xbe\xe7\x1d\x13\x0a\x81\x60\x63\x09\x82\x27\xf6\x50\x28\x00\xd3\xc2\x7a\x79\x62\x83\x16\x06\x36\x1d\xf2\x08\x07\x1a\xaf\x14\x44\x83\xc3\x8e\x12\x76\x44\xde\x7d\x20\x17\x9b\x1b\x0d\xc1\x99\xcf\x40\x13\x36\xa0\xf7\x5f\x3d\xff\x66\x46\xe6\xee\x5a\x4f\x7b\xd7\x0b\xb8\x60\xed\x04\x52\x87\xc6\x2e\xaf\x15\x3a\xc9\x4f\xd0\x0a\x1c\xd8\x1c\x51\xa8\x3e\xd0\xb8\x48\xbc\xdd\xfc\x49\xc4\xb6\x33\x6e\x99\xd8\xf4\xbd\x25\x3c\x4b\x33\x08\x2d\xed\x31\x58\x81\x3d\x5a\x1f\x5f\xdc\x90\x60\x3d\x4d\x77\x99\xcd\x93\x88\x71\xef\x75\xea\xe5\x7f\x9b\xd2\xa5\x52\x0f\x2f\xd5\xfd\x6b\x05\x4f\xdb\xc7\xea\x2d\x95\x5a\x1b\xd7\xd2\x09\x9e\xb7\xe7\x7f\x29\x66\x5d\x97\x6b\x3e\x96\x96\xc3\x51\xcc\xc9\x58\x46\x4e\x46\x31\x27\xd3\xfc\x39\x9d\x0e\x74\xf9\x1d\x00\x00\xff\xff\x47\xa4\xa3\x62\x3b\x02\x00\x00")

func _1_init_up_sql() ([]byte, error) {
	return bindata_read(
		__1_init_up_sql,
		"1_init.up.sql",
	)
}

var __2_verifications_up_sql = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\xe4\xd2\x4d\x6a\xc3\x30\x10\x05\xe0\xbd\x4e\xf1\x96\x0e\xf4\x06\x59\x4d\xec\x71\x6b\x2a\xdb\x41\x1e\x97\xa6\x1b\x23\x6a\x15\x44\xfd\x13\x2c\x37\xb9\x7e\x89\x08\x84\xd0\xd2\x0b\x74\xad\x4f\x8f\xc7\x63\x52\xc3\x24\x0c\xa1\x9d\x66\xb8\xd1\xfa\xa1\x3b\xb9\xc5\x7f\xf8\x77\xbb\xfa\x79\x0a\x48\x14\xe0\x7b\x04\xb7\x78\x3b\x60\x6f\x8a\x92\xcc\x01\xcf\x7c\x78\x50\xc0\xea\x47\x17\x56\x3b\x1e\x21\x45\xc9\x8d\x50\xb9\x97\x37\x54\xb5\xa0\x6a\xb5\x46\xc6\x39\xb5\x5a\x30\xcd\xe7\x64\x73\xf1\x5f\xc1\x2d\x9d\xef\x51\x54\xc2\x8f\x6c\x6e\xd2\x70\xce\x86\xab\x94\x9b\x68\x42\xe2\xfb\xf8\x21\x36\x82\xf0\xab\xfc\x6a\xaf\x38\xaa\xe8\xd7\xf9\xd3\x4d\x78\x21\x93\x3e\xd1\x2d\xfe\xf2\x72\xb2\x83\xef\xb1\xab\x6b\xcd\x54\xfd\xac\x28\xa6\x65\xb5\xd9\x2a\x75\x37\xc8\xd1\x86\x70\x9e\x97\xbe\x5b\x5c\x70\xeb\xff\x1b\x83\xb4\xb0\xb9\x6e\x11\xd3\x41\x59\x76\x77\x26\xee\x8f\x98\x9c\x74\xc3\x5b\xf5\x1d\x00\x00\xff\xff\x70\x77\x56\xa6\x63\x02\x00\x00")

func _2_verifications_up_sql() ([]byte, error) {
	return bindata_read(
		__2_verifications_up_sql,
		"2_verifications.up.sql",
	)
}

var _bindata_go = []byte("\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xff\x01\x00\x00\xff\xff\x00\x00\x00\x00\x00\x00\x00\x00")

func bindata_go() ([]byte, error) {
	return bindata_read(
		_bindata_go,
		"bindata.go",
	)
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		return f()
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() ([]byte, error){
	"1_init.up.sql": _1_init_up_sql,
	"2_verifications.up.sql": _2_verifications_up_sql,
	"bindata.go": bindata_go,
}
// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for name := range node.Children {
		rv = append(rv, name)
	}
	return rv, nil
}

type _bintree_t struct {
	Func func() ([]byte, error)
	Children map[string]*_bintree_t
}
var _bintree = &_bintree_t{nil, map[string]*_bintree_t{
	"1_init.up.sql": &_bintree_t{_1_init_up_sql, map[string]*_bintree_t{
	}},
	"2_verifications.up.sql": &_bintree_t{_2_verifications_up_sql, map[string]*_bintree_t{
	}},
	"bindata.go": &_bintree_t{bindata_go, map[string]*_bintree_t{
	}},
}}
