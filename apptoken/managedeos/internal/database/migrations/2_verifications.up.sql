CREATE TABLE email_verifications (
  id serial PRIMARY KEY,
  timestamp TIMESTAMPTZ NOT NULL DEFAULT now(),
  user_id INTEGER NOT NULL REFERENCES users(id),
  email TEXT NOT NULL REFERENCES  users(email),
  token VARCHAR NOT NULL,
  valid BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE password_resets (
  id serial PRIMARY KEY,
  timestamp TIMESTAMPTZ NOT NULL DEFAULT now(),
  user_id INTEGER NOT NULL REFERENCES users(id),
  email TEXT NOT NULL REFERENCES  users(email),
  token VARCHAR NOT NULL,
  valid BOOLEAN NOT NULL DEFAULT TRUE
);

ALTER TABLE users ADD email_verified BOOLEAN NOT NULL DEFAULT FALSE;
