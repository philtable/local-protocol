create table users (
  id               serial primary key,
  username         varchar(12) unique,
  password         char(60)    not null,
  email            text unique not null,
  private_key      char(51)    not null,
  admin            boolean     not null default false,
  beta_user        boolean     not null default false,
  setting_username boolean     not null default false
);

CREATE INDEX username_index ON users(username);
CREATE INDEX email_index ON users(email);
CREATE INDEX admin_index ON users(admin);
CREATE INDEX beta_user_index ON users(beta_user);