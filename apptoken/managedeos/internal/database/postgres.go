package database

import (
	"fmt"
	"github.com/golang-migrate/migrate/v3"
	"github.com/golang-migrate/migrate/v3/database/postgres"
	_ "github.com/golang-migrate/migrate/v3/database/postgres"
	"github.com/golang-migrate/migrate/v3/source/go_bindata"
	_ "github.com/golang-migrate/migrate/v3/source/go_bindata"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	"gitlab.com/apptoken/managedeos/internal/database/migrations"
)


type postgresDatastore struct {
	db *sqlx.DB
}


func NewPostgresDatastore(user string, password string, ip string, port uint, dbname string, flags string) (Datastore, error) {
	s := bindata.Resource(migrations.AssetNames(),
		func(name string) ([]byte, error) {
			return migrations.Asset(name)
		})

	d, err := bindata.WithInstance(s)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create bindata")
	}

	db, err := sqlx.Open("postgres",
		fmt.Sprintf("user=%s password=%s host=%s port=%v dbname=%s sslmode=disable %s", user, password, ip, port, dbname, flags))
	if err != nil {
		return nil, errors.Wrap(err, "failed to open database")
	}

	err = db.Ping()
	if err != nil {
		return nil, errors.Wrap(err, "failed to ping db")
	}

	m_db, err := postgres.WithInstance(db.DB, &postgres.Config{
		MigrationsTable: postgres.DefaultMigrationsTable,
		DatabaseName:    dbname,
	})
	if err != nil {
		return nil, errors.Wrap(err, "failed to open migration database")
	}

	m, err := migrate.NewWithInstance(
		"go-bindata",
		d,
		"postgres",
		m_db,
	)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create migrator")
	}

	err = m.Up()
	if err != nil && err != migrate.ErrNoChange { // why is this even an error
		return nil, errors.Wrap(err, "failed to apply migrations")
	}

	return &postgresDatastore{db: db}, nil
}


func (db *postgresDatastore) ListUsers() ([]*User, error) {
	var users []*User
	err := db.db.Select(&users, `SELECT id, username, password, email, private_key, admin, beta_user, setting_username FROM users ORDER BY id ASC`)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get users")
	}
	return users, nil
}


func (db *postgresDatastore) CreateUser(email string, password string, private_key string) (*User, error) {
	var id []uint64
	err := db.db.Select(&id, `INSERT INTO users (email, password, private_key) VALUES ($1, $2 ,$3) RETURNING id`, email, password, private_key)
	if err != nil {
		if v, ok := err.(*pq.Error); ok {
			if v.Code.Name() == "unique_violation" && v.Constraint == "users_email_key" {
				return nil, ErrUserExists
			}
		}
		return nil, err
	}
	return &User{
		Id:         id[0],
		Email:      email,
		Password:   password,
		PrivateKey: private_key,
	}, nil
}


func (db *postgresDatastore) CreateEmailVerification(user_id uint64, email string, token string) (*EmailVerification, error) {
	var id []uint64
	err := db.db.Select(&id, `INSERT INTO email_verifications (user_id, email, token) VALUES ($1, $2, $3) RETURNING id`, user_id, email, token)
	if err != nil {
		if v, ok := err.(*pq.Error); ok {
			if v.Code.Name() == "unique_violation" && v.Constraint == "email_verifications_email_key" {
				return nil, ErrEmailVerificationExists
			}
		}
		return nil, err
	}
	return &EmailVerification{
		Id:         id[0],
		UserId:   	user_id,
		Email:			email,
		Token: 			token,
	}, nil
}

func (db *postgresDatastore) CreatePasswordReset(user_id uint64, email string, token string) (*PasswordReset, error) {
	var id []uint64
	err := db.db.Select(&id, `INSERT INTO password_resets (user_id, email, token) VALUES ($1, $2, $3) RETURNING id`, user_id, email, token)
	if err != nil {
		return nil, err
	}
	return &PasswordReset{
		Id:         id[0],
		UserId:   	user_id,
		Email:			email,
		Token: 			token,
	}, nil
}


func (db *postgresDatastore) UpdateUsername(uid uint64, username string) (user *User, err error) {
	tx, err := db.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "could not begin tx")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			if err != nil {
				user = nil
				err = errors.Wrap(err, "failed to commit tx")
			}
		} else {
			rollbackErr := tx.Rollback()
			if rollbackErr != nil {
				user = nil
				err = errors.Wrapf(err, "failed to rollback tx: %v", rollbackErr)
			}
		}
	}()

	rows, err := tx.Exec(`UPDATE users SET username = $1 WHERE id = $2`, username, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update username")
	}
	modified, err := rows.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "failed to check modified rows")
	}
	if modified != 1 {
		return nil, errors.Wrapf(err, "unexpected modifications %d", modified)
	}

	user, err = db.getUserById(uid, tx)
	return
}


func (db *postgresDatastore) UpdateBeta(uid uint64, beta bool) (user *User, err error) {
	tx, err := db.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "could not begin tx")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			if err != nil {
				user = nil
				err = errors.Wrap(err, "failed to commit tx")
			}
		} else {
			rollbackErr := tx.Rollback()
			if rollbackErr != nil {
				user = nil
				err = errors.Wrapf(err, "failed to rollback tx: %v", rollbackErr)
			}
		}
	}()

	rows, err := tx.Exec(`UPDATE users SET beta_user = $1 WHERE id = $2`, beta, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update username")
	}
	modified, err := rows.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "failed to check modified rows")
	}
	if modified != 1 {
		return nil, errors.Wrapf(err, "unexpected modifications %d", modified)
	}

	user, err = db.getUserById(uid, tx)
	return
}

func (db *postgresDatastore) UpdatePassword(uid uint64, password string) (user *User, err error) {
	tx, err := db.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "could not begin tx")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			if err != nil {
				user = nil
				err = errors.Wrap(err, "failed to commit tx")
			}
		} else {
			rollbackErr := tx.Rollback()
			if rollbackErr != nil {
				user = nil
				err = errors.Wrapf(err, "failed to rollback tx: %v", rollbackErr)
			}
		}
	}()

	rows, err := tx.Exec(`UPDATE users SET password = $1 WHERE id = $2`, password, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update password")
	}
	modified, err := rows.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "failed to check modified rows")
	}
	if modified != 1 {
		return nil, errors.Wrapf(err, "unexpected modifications %d", modified)
	}

	user, err = db.getUserById(uid, tx)
	return
}

func (db *postgresDatastore) UpdateEmailVerified(uid uint64, email_verified bool) (user *User, err error) {
	tx, err := db.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "could not begin tx")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			if err != nil {
				user = nil
				err = errors.Wrap(err, "failed to commit tx")
			}
		} else {
			rollbackErr := tx.Rollback()
			if rollbackErr != nil {
				user = nil
				err = errors.Wrapf(err, "failed to rollback tx: %v", rollbackErr)
			}
		}
	}()

	rows, err := tx.Exec(`UPDATE users SET email_verified = $1 WHERE id = $2`, email_verified, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update username")
	}
	modified, err := rows.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "failed to check modified rows")
	}
	if modified != 1 {
		return nil, errors.Wrapf(err, "unexpected modifications %d", modified)
	}

	user, err = db.getUserById(uid, tx)
	return
}

func (db *postgresDatastore) UpdateEmailVerificationValid(eid uint64, email_verified bool) (email_verification *EmailVerification, err error) {
	tx, err := db.db.Beginx()
	if err != nil {
		return nil, errors.Wrap(err, "could not begin tx")
	}
	defer func() {
		if err == nil {
			err = tx.Commit()
			if err != nil {
				email_verification = nil
				err = errors.Wrap(err, "failed to commit tx")
			}
		} else {
			rollbackErr := tx.Rollback()
			if rollbackErr != nil {
				email_verification = nil
				err = errors.Wrapf(err, "failed to rollback tx: %v", rollbackErr)
			}
		}
	}()

	rows, err := tx.Exec(`UPDATE email_verifications SET valid = $1 WHERE id = $2`, false, eid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to update email verification")
	}
	modified, err := rows.RowsAffected()
	if err != nil {
		return nil, errors.Wrap(err, "failed to check modified rows")
	}
	if modified != 1 {
		return nil, errors.Wrapf(err, "unexpected modifications %d", modified)
	}

	email_verification, err = db.getEmailVerificationById(eid, tx)
	return
}


func (db *postgresDatastore) GetUserById(uid uint64) (*User, error) {
	return db.getUserById(uid, db.db)
}


func (db *postgresDatastore) getUserById(uid uint64, queryer sqlx.Queryer) (*User, error) {
	rows, err := queryer.Queryx(`SELECT id, username, password, email, private_key, admin, beta_user, setting_username, email_verified FROM users WHERE id = $1`, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user")
	}

	if !rows.Next() {
		return nil, ErrUserNotFound
	}

	var users User
	err = rows.StructScan(&users)
	if err != nil {
		return nil, errors.Wrap(err, "failed to scan user")
	}
	err = rows.Close()
	if err != nil {
		return nil, errors.Wrap(err, "failed to close rows")
	}
	return &users, nil
}

func (db *postgresDatastore) GetEmailVerificationById(eid uint64) (*EmailVerification, error) {
	return db.getEmailVerificationById(eid, db.db)
}

func (db *postgresDatastore) getEmailVerificationById(eid uint64, queryer sqlx.Queryer) (*EmailVerification, error) {
	rows, err := queryer.Queryx(`SELECT id, timestamp, user_id, email, token, valid FROM email_verifications WHERE id = $1`, eid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get email verification")
	}

	if !rows.Next() {
		return nil, ErrEmailVerificationNotFound
	}

	var email_verifications EmailVerification
	err = rows.StructScan(&email_verifications)
	if err != nil {
		return nil, errors.Wrap(err, "failed to scan email verification")
	}
	err = rows.Close()
	if err != nil {
		return nil, errors.Wrap(err, "failed to close rows")
	}
	return &email_verifications, nil
}



func (db *postgresDatastore) GetUserByUsername(uid string) (*User, error) {
	var users []*User
	err := db.db.Select(&users, `SELECT id, username, password, email, private_key, admin, beta_user, setting_username, email_verified FROM users WHERE username = $1`, uid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user")
	}
	if len(users) == 0 {
		return nil, ErrUserNotFound
	}
	return users[0], nil
}


func (db *postgresDatastore) GetUserByEmail(email string) (*User, error) {
	var users []*User
	err := db.db.Select(&users, `SELECT id, username, password, email, private_key, admin, beta_user, setting_username, email_verified FROM users WHERE email = $1`, email)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user")
	}
	if len(users) == 0 {
		return nil, ErrUserNotFound
	}
	return users[0], nil
}



func (db *postgresDatastore) GetEmailVerificationByToken(token string) (*EmailVerification, error) {
	var email_verifications []*EmailVerification
	err := db.db.Select(&email_verifications, `SELECT id, timestamp, user_id, email, token, valid FROM email_verifications WHERE token = $1`, token)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get email verification")
	}
	if len(email_verifications) == 0 {
		return nil, ErrEmailVerificationNotFound
	}
	return email_verifications[0], nil
}

func (db *postgresDatastore) GetPasswordResetByToken(token string) (*PasswordReset, error) {
	var password_resets []*PasswordReset
	err := db.db.Select(&password_resets, `SELECT id, timestamp, user_id, email, token, valid FROM password_resets WHERE token = $1`, token)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get password reset")
	}
	if len(password_resets) == 0 {
		return nil, ErrPasswordResetNotFound
	}
	return password_resets[0], nil
}

func (db *postgresDatastore) SetIsSettingUsername(uid uint64, newValue bool, expectedOldValue bool) error {
	res, err := db.db.Exec(`UPDATE users SET setting_username = $1 WHERE id = $2 AND setting_username = $3`, newValue, uid, expectedOldValue)
	if err != nil {
		return errors.Wrap(err, "failed to update")
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return errors.Wrap(err, "failed to get rows affected")
	}

	if rowsAffected == 0 {
		return ErrNoRowsUpdated
	}

	return nil
}
