package v1

import (
	"encoding/hex"
	"encoding/json"
	"github.com/eoscanada/eos-go"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"log"
	"net/http"
	"strings"
)

type SignTransactionRequest struct {
	ChainID               string     `json:"chainId"`
	RequiredKeys          []string   `json:"requiredKeys"`
	SerializedTransaction []byte     `json:"serializedTransaction"`
	ABIs                  []*eos.ABI `json:"abis"`
}

func (a *ApiV1) isAccountAllowed(an eos.AccountName) bool {
	return an == eos.AN(a.config.Network.AppAccount) || an == eos.AN(a.config.Network.TokenAccount)
}

func (a *ApiV1) isActionAllowed(an eos.AccountName, ac eos.ActionName) bool {
	return a.isAccountAllowed(an)
}

func (a *ApiV1) SignTransaction(w http.ResponseWriter, r *http.Request) {
	var signTransactionRequest SignTransactionRequest
	err := json.NewDecoder(r.Body).Decode(&signTransactionRequest)
	if err != nil {
		log.Println(err)
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad transaction"))
		return
	}

	if strings.ToLower(a.config.Network.ChainID) != strings.ToLower(signTransactionRequest.ChainID) {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("illegal transaction"))
		return
	}

	chainIdBytes, err := hex.DecodeString(signTransactionRequest.ChainID)
	if err != nil {
		log.Println(err)
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad transaction"))
		return
	}

	var tx eos.Transaction
	err = eos.NewDecoder(signTransactionRequest.SerializedTransaction).Decode(&tx)
	if err != nil {
		log.Println(err)
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad transaction"))
		return
	}

	for _, action := range tx.Actions {
		if !a.isAccountAllowed(action.Account) || !a.isActionAllowed(action.Account, action.Name) {
			internalerror.WriteError(w, internalerror.NewInvalidArgumentError("illegal transaction"))
			return
		}
	}

	session := middleware.GetSession(r)

	user, err := a.datastore.GetUserById(session.UserID)
	if err != nil {
		log.Printf("failed to get user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	keybag := eos.NewKeyBag()
	err = keybag.Add(user.PrivateKey)
	if err != nil {
		log.Printf("failed to construct signer: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	signedTx, err := keybag.Sign(eos.NewSignedTransaction(&tx), chainIdBytes, user.GetPublicKey())
	if err != nil {
		log.Printf("failed to sign tx: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":        true,
		"signed_tx": signedTx,
	})
}
