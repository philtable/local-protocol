package v1

import (
	"encoding/json"
	"fmt"
	"github.com/eoscanada/eos-go"
	"github.com/eoscanada/eos-go/system"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"log"
	"net/http"
	"regexp"
	"strings"
)

var validUsernamePattern = regexp.MustCompile("^[a-z1-5]{12}$")

type CreateAccountRequest struct {
	Username string `json:"username"`
}

func (a *ApiV1) createAccount(w http.ResponseWriter, r *http.Request) {
	session := middleware.GetSession(r)
	user, err := a.datastore.GetUserById(session.UserID)
	if err != nil {
		log.Printf("error getting user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to load user"))
		return
	}

	if user.Username != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("account already created"))
		return
	}

	if !user.BetaUser {
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotBetaUser, "not beta user"))
		return
	}

	if !user.EmailVerified {
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotEmailVerified, "email not verified"))
		return
	}



	keybag := eos.NewKeyBag()
	err = keybag.Add(a.config.AccountCreator.PrivateKey)
	if err != nil {
		log.Printf("failed to construct signer: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}
	api := a.newEosApi()
	api.SetSigner(keybag)


	// todo(mitsui): check if they already have an account (needs upstream patch)
	// api.GetKeyAccounts()

	var createAccountRequest CreateAccountRequest
	err = json.NewDecoder(r.Body).Decode(&createAccountRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("malformed body"))
		return
	}

	if !validUsernamePattern.MatchString(createAccountRequest.Username) {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("invalid username requested"))
		return
	}

	if user.SettingUsername {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("currently creating account"))
		return
	}

	err = a.datastore.SetIsSettingUsername(session.UserID, true, false)
	if err != nil {
		log.Printf("error updating isSettingUsername for %v: %v\n", session.UserID, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to toggle account creation state"))
		return
	}

	defer func() {
		err = a.datastore.SetIsSettingUsername(session.UserID, false, true)
		if err != nil {
			internalerror.WriteError(w, internalerror.NewInternalServerError("failed to toggle account creation state"))
			return
		}
	}()

	out, err := api.SignPushActions(
		system.NewNewAccount(eos.AN(a.config.AccountCreator.Username), eos.AN(createAccountRequest.Username), user.GetPublicKey()),
		system.NewBuyRAMBytes(eos.AN(a.config.AccountCreator.Username), eos.AN(createAccountRequest.Username), a.config.AccountCreator.RAM),
		system.NewDelegateBW(
			eos.AN(a.config.AccountCreator.Username),
			eos.AN(createAccountRequest.Username),
			eos.NewEOSAsset(int64(a.config.AccountCreator.CPU*10000)),
			eos.NewEOSAsset(int64(a.config.AccountCreator.Net*10000)),
			false,
		),
	)
	if err != nil {
		// todo(mitsui): this is ugly but needs an upstream patch
		errStr := err.Error()
		if strings.Contains(errStr, "that name is already taken") {
			internalerror.WriteError(w, internalerror.NewInvalidArgumentError("username_taken"))
			return
		}
		log.Printf("failed to send tx: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	str, _ := json.Marshal(out)
	fmt.Println(string(str))

	newUser, err := a.datastore.UpdateUsername(user.Id, createAccountRequest.Username)
	if err != nil {
		log.Printf("failed to update username in database for %v to %v: %v\n", user.Id, createAccountRequest.Username, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": newUser.ToExternalUser(),
	})
}
