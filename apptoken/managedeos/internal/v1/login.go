package v1

import (
	"encoding/json"
	"gitlab.com/apptoken/managedeos/internal/database"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (a *ApiV1) login(w http.ResponseWriter, r *http.Request) {
	session := middleware.GetSession(r)

	if session != nil && session.IsLoggedIn() {
		user, err := a.datastore.GetUserById(session.UserID)
		if err != nil {
			log.Printf("error getting user: %v\n", err)
			internalerror.WriteError(w, internalerror.NewInternalServerError("failed to load user"))
			return
		}
		json.NewEncoder(w).Encode(map[string]interface{}{
			"ok":     true,
			"result": user.ToExternalUser(),
		})
		return
	}

	var loginRequest LoginRequest
	err := json.NewDecoder(r.Body).Decode(&loginRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("malformed body"))
		return
	}

	if loginRequest.Email == "" || loginRequest.Password == "" {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("missing required fields"))
		return
	}

	user, err := a.datastore.GetUserByEmail(loginRequest.Email)
	if err != nil {
		if err == database.ErrUserNotFound {
			internalerror.WriteError(w, internalerror.NewInvalidArgumentError("invalid user"))
		} else {
			log.Printf("error getting user: %v\n", err)
			internalerror.WriteError(w, internalerror.NewInternalServerError("failed to load user"))
		}
		return
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginRequest.Password))
	if err != nil {
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeLoginFailed, "bad credentials"))
		return
	}


	session = middleware.NewSession(user.Id)
	session.Save(a.config.SecretKey, w)

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": user.ToExternalUser(),
	})
}
