package v1

import (
	"encoding/json"
	"net/http"
)

func (a *ApiV1) GetConfig(w http.ResponseWriter, r *http.Request) {
	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok": true,
		"config": map[string]interface{}{
			"api_endpoint":  a.config.Network.APIUrl,
			"app_account":   a.config.Network.AppAccount,
			"token_account": a.config.Network.TokenAccount,
			"chain_id":      a.config.Network.ChainID,
			"abis":          a.abis,
		},
	})
}
