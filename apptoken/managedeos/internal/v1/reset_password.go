package v1

import (
	"fmt"
	"github.com/gorilla/mux"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"log"
	"net/http"
)

func (a *ApiV1) resetPassword(w http.ResponseWriter, r *http.Request) {

  vars := mux.Vars(r)
  token := vars["token"]

  targetPasswordReset, err := a.datastore.GetPasswordResetByToken(string(token))
	if err != nil {
		log.Printf("failed to get target password reset: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
	}
  if targetPasswordReset.Valid == false {
    log.Printf("password reset is not valid: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
  }

  targetUser, err := a.datastore.GetUserById(uint64(targetPasswordReset.UserId))
	if err != nil {
		log.Printf("failed to get target user %v: %v\n", targetUser, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

  fmt.Fprintf(w,
				"<p><b>Reset Password</b></p>"+
        "<form action=\"/api/v1/submit_password_reset/%s\" method=\"POST\">"+
	        "<input name=\"password_new\" placeholder=\"New Password\" required=\"required\" type=\"password\" id=\"password\" style=\"width: 200px;\"></input><br>"+
					"<input name=\"password_confirm\" placeholder=\"Confirm Password\" required=\"required\" type=\"password\" id=\"password_confirm\" oninput=\"check(this)\" style=\"width: 200px;\" />"+
					"<script language=\"javascript\" type=\"text/javascript\"> function check(input) { if (input.value != document.getElementById(\"password\").value) { input.setCustomValidity(\"Password Must be Matching.\"); } else { input.setCustomValidity(\"\"); } } </script>"+
					"<br><br><input type=\"submit\" value=\"Reset\" style=\"width: 200px;\">"+
        "</form>",
				token)

}
