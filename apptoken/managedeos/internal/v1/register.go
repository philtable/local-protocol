package v1

import (
	"encoding/json"
	"github.com/eoscanada/eos-go/ecc"
	"gitlab.com/apptoken/managedeos/internal/database"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"golang.org/x/crypto/bcrypt"
	"log"
	"net/http"
)

type RegisterRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func (a *ApiV1) register(w http.ResponseWriter, r *http.Request) {
	session := middleware.GetSession(r)

	if session != nil && session.IsLoggedIn() {
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusBadRequest, internalerror.ErrTypeAlreadyLoggedIn, "already logged in"))
		return
	}

	var registerRequest RegisterRequest
	err := json.NewDecoder(r.Body).Decode(&registerRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("malformed body"))
		return
	}

	if registerRequest.Email == "" || registerRequest.Password == "" {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("missing required fields"))
		return
	}

	hashed, err := bcrypt.GenerateFromPassword([]byte(registerRequest.Password), 12)
	if err != nil {
		log.Printf("failed to hash password: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to hash password"))
		return
	}

	pkey, err := ecc.NewRandomPrivateKey()
	if err != nil {
		log.Printf("failed to generate private key: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to generate private key"))
		return
	}

	user, err := a.datastore.CreateUser(registerRequest.Email, string(hashed), pkey.String())
	if err != nil {
		if err == database.ErrUserExists {
			internalerror.WriteError(w, internalerror.NewInvalidArgumentError("user exists"))
			return
		}
		log.Printf("error creating user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to create user"))
		return
	}

	session = middleware.NewSession(user.Id)
	session.Save(a.config.SecretKey, w)

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": user.ToExternalUser(),
	})
}
