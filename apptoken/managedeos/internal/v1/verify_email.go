package v1

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"log"
	"net/http"
)

func (a *ApiV1) verifyEmail(w http.ResponseWriter, r *http.Request) {

  vars := mux.Vars(r)
  token := vars["token"]

  targetEmailVerification, err := a.datastore.GetEmailVerificationByToken(string(token))
	if err != nil {
		log.Printf("failed to get target email verification: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
	}
  if targetEmailVerification.Valid == false {
    log.Printf("email verification is not valid: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
  }

  targetUser, err := a.datastore.GetUserById(uint64(targetEmailVerification.UserId))
	if err != nil {
		log.Printf("failed to get target user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

  returnedEmailVerification, err := a.datastore.UpdateEmailVerificationValid(uint64(targetEmailVerification.Id), false)
	if err != nil {
		log.Printf("failed to update valid for Email Verification in database for %v: %v\n", targetEmailVerification.Id, err)
    log.Printf("Email Verification returned: %v\n", returnedEmailVerification)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

  updatedEmailVerified, err := a.datastore.UpdateEmailVerified(uint64(targetUser.Id), true)
  if err != nil {
    log.Printf("failed to update email_verified in database for %v: %v\n", targetUser.Id, err)
    internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
  }

  http.Redirect(w, r, "http://www.bullstamp.co/login", 301)

  json.NewEncoder(w).Encode(map[string]interface{}{
    "ok":     true,
    "result": updatedEmailVerified.ToExternalUser(),
  })
}
