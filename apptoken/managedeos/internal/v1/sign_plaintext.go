package v1

import (
	"crypto/sha256"
	"encoding/json"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"log"
	"net/http"
)

type SignPlaintextRequest struct {
	Message string `json:"message"`
}

func (a *ApiV1) SignPlaintext(w http.ResponseWriter, r *http.Request) {
	var signPlaintextRequest SignPlaintextRequest
	err := json.NewDecoder(r.Body).Decode(&signPlaintextRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad message"))
		return
	}

	var ignore interface{}
	err = json.Unmarshal([]byte(signPlaintextRequest.Message), &ignore)
	if err != nil {
		// no signing arbitrary blobs!
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad message"))
		return
	}

	session := middleware.GetSession(r)

	user, err := a.datastore.GetUserById(session.UserID)
	if err != nil {
		log.Printf("failed to get user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	h := sha256.New()
	h.Write([]byte(signPlaintextRequest.Message))
	hashed := h.Sum(nil)

	sig, err := user.GetPrivateKey().Sign(hashed)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("failed to sign"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{} {
		"ok": true,
		"message": signPlaintextRequest.Message,
		"signature": sig.String(),
	})
}
