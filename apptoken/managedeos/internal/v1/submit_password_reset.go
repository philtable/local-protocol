package v1

import (
	"encoding/json"
  "github.com/gorilla/mux"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"log"
	"net/http"
  "strings"
  "golang.org/x/crypto/bcrypt"
)

func (a *ApiV1) submitPasswordReset(w http.ResponseWriter, r *http.Request) {

  vars := mux.Vars(r)
  token := vars["token"]

  targetPasswordReset, err := a.datastore.GetPasswordResetByToken(string(token))
	if err != nil {
		log.Printf("failed to get target password reset: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
	}
  if targetPasswordReset.Valid == false {
    log.Printf("password reset is not valid: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
    return
  }

  r.ParseForm()
  newPassword := strings.Join(r.Form["password_new"], "")

  if newPassword == "" {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("missing password"))
		return
	}

	hashed, err := bcrypt.GenerateFromPassword([]byte(newPassword), 12)
	if err != nil {
		log.Printf("failed to hash password: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to hash password"))
		return
	}

  targetUser, err := a.datastore.GetUserById(uint64(targetPasswordReset.UserId))
	if err != nil {
		log.Printf("failed to get target user %v: %v\n", targetUser, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

  updatedUser, err := a.datastore.UpdatePassword(targetUser.Id, string(hashed))
	if err != nil {
		log.Printf("failed to update password in database for %v: %v\n", targetUser.Id, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

  log.Printf("reset password for user with User ID: %v", targetUser.Id)

  http.Redirect(w, r, "http://www.bullstamp.co/login", 301)

  json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": updatedUser.ToExternalUser(),
	})

}
