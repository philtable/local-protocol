package v1

import (
	"encoding/json"
	"github.com/pborman/uuid"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"log"
	"net/http"
)

type PasswordResetRequest struct {
	Email    string `json:"email"`
}

func (a *ApiV1) requestPasswordReset(w http.ResponseWriter, r *http.Request) {

  session := middleware.GetSession(r)

  if session != nil && session.IsLoggedIn() {
    internalerror.WriteError(w, internalerror.NewHttpError(http.StatusBadRequest, internalerror.ErrTypeAlreadyLoggedIn, "already logged in"))
    return
  }

  var passwordResetRequest PasswordResetRequest
	err := json.NewDecoder(r.Body).Decode(&passwordResetRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("malformed body"))
		return
	}

	if passwordResetRequest.Email == "" {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("missing required fields"))
		return
	}

	user, err := a.datastore.GetUserByEmail(passwordResetRequest.Email)
	if err != nil {
		log.Printf("failed to get user by email: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	token := uuid.NewRandom().String()

	passwordReset, err := a.datastore.CreatePasswordReset(user.Id, user.Email, token)
	if err != nil {
		log.Printf("error creating password reset: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("failed to create password reset"))
		return
	}

	err = a.mailer.SendPasswordResetEmail(user.Email, token)
	if err != nil {
		log.Printf("failed to send password reset email for %v: %v\n", user.Email, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": passwordReset.ToExternalRequestPasswordReset(),
	})
}
