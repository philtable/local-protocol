package v1

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/pborman/uuid"
	"gitlab.com/apptoken/managedeos/internal/database"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"gitlab.com/apptoken/managedeos/pkg"
	"log"
	"net/http"
	"strconv"
)

func (a *ApiV1) ListUsers(w http.ResponseWriter, r *http.Request) {
	users, err := a.datastore.ListUsers()
	if err != nil {
		log.Printf("failed to list users: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	publicUsers := make([]*pkg.User, 0)
	for _, internalUser := range users {
		publicUsers = append(publicUsers, internalUser.ToExternalUser())
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": publicUsers,
	})
}



type UpdateUserBetaRequest struct {
	BetaUser bool `json:"beta_user"`
}

func (a *ApiV1) UserUpdateBeta(w http.ResponseWriter, r *http.Request) {
	var updateUserBetaRequest UpdateUserBetaRequest
	err := json.NewDecoder(r.Body).Decode(&updateUserBetaRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad request"))
		return
	}

	session := middleware.GetSession(r)

	user, err := a.datastore.GetUserById(session.UserID)
	if err != nil {
		log.Printf("failed to get user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	if !user.Admin {
		log.Printf("non-admin %v tried to do admin things!\n", user.Id)
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotAdmin, "not allowed!"))
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad user id"))
		return
	}

	targetUser, err := a.datastore.GetUserById(uint64(id))
	if err != nil {
		log.Printf("failed to get target user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	if targetUser.BetaUser == false {

		token := uuid.NewRandom().String()

		emailVerification, err := a.datastore.CreateEmailVerification(targetUser.Id, targetUser.Email, token)
		if err != nil {
			if err == database.ErrEmailVerificationExists {
				internalerror.WriteError(w, internalerror.NewInvalidArgumentError("email verification exists"))
				return
			}
			log.Printf("error creating email verification: %v\n", err)
			log.Printf("email verification was: %v\n", emailVerification)
			internalerror.WriteError(w, internalerror.NewInternalServerError("failed to create email verification"))
			return
		}

		err = a.mailer.SendVerificationEmail(targetUser.Email, token)
		if err != nil {
			log.Printf("failed to send verification email for %v: %v\n", id, err)
			internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
			return
		}
	}

	updatedUser, err := a.datastore.UpdateBeta(uint64(id), updateUserBetaRequest.BetaUser)
	if err != nil {
		log.Printf("failed to update beta_user in database for %v to %v: %v\n", id, updateUserBetaRequest.BetaUser, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": updatedUser.ToExternalUser(),
	})
}

type UpdateUserUsernameRequest struct {
	Username string `json:"username"`
}



func (a *ApiV1) UserUpdateUsername(w http.ResponseWriter, r *http.Request) {
	var updateUserUsernameRequest UpdateUserUsernameRequest
	err := json.NewDecoder(r.Body).Decode(&updateUserUsernameRequest)
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad request"))
		return
	}

	session := middleware.GetSession(r)

	user, err := a.datastore.GetUserById(session.UserID)
	if err != nil {
		log.Printf("failed to get user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	if !user.Admin {
		log.Printf("non-admin %v tried to do admin things!\n", user.Id)
		internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotAdmin, "not allowed!"))
		return
	}

	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		internalerror.WriteError(w, internalerror.NewInvalidArgumentError("bad user id"))
		return
	}

	updatedUser, err := a.datastore.UpdateUsername(uint64(id), updateUserUsernameRequest.Username)
	if err != nil {
		log.Printf("failed to update username in database for %v to %v: %v\n", id, updateUserUsernameRequest.Username, err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":     true,
		"result": updatedUser.ToExternalUser(),
	})
}

func (a *ApiV1) UserGetSelf(w http.ResponseWriter, r *http.Request) {
	session := middleware.GetSession(r)
	user, err := a.datastore.GetUserById(session.UserID)

	if err != nil {
		log.Printf("failed to get user: %v\n", err)
		internalerror.WriteError(w, internalerror.NewInternalServerError("internal error"))
		return
	}

	json.NewEncoder(w).Encode(map[string]interface{}{
		"ok":   true,
		"user": user.ToExternalUser(),
	})
}
