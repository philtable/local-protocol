package v1

import (
	"fmt"
	"github.com/eoscanada/eos-go"
	"github.com/gorilla/mux"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/apptoken/managedeos/internal/database"
	"gitlab.com/apptoken/managedeos/internal/email"
	"gitlab.com/apptoken/managedeos/internal/middleware"
	"gitlab.com/apptoken/managedeos/pkg"
	"log"
	"net/http"
)

type ApiV1 struct {
	Mux http.Handler

	config    *pkg.Config
	datastore database.Datastore
	session   *sessions.CookieStore

	abis map[eos.AccountName]*eos.ABI

	mailer email.Sender
}

func (a *ApiV1) newEosApi() *eos.API {
	api := eos.New(a.config.Network.APIUrl)
	api.Debug = true
	return api
}

// todo(mitsui): this isn't ApiV1's responsibility
func (a *ApiV1) loadAbi(account string) {
	eosApi := a.newEosApi()
	abi, err := eosApi.GetABI(eos.AN(account))
	if err != nil {
		panic(fmt.Sprintf("failed to load abi of %v: %v\n", account, err))
	}

	a.abis[abi.AccountName] = &abi.ABI

	for _, action := range abi.ABI.Actions {
		log.Printf("registering action %v.%v\n", abi.AccountName, action.Name)
		eos.RegisterAction(abi.AccountName, action.Name, map[string]interface{}{})
	}
}

func NewApiV1(config *pkg.Config) *ApiV1 {
	pqdb, err := database.NewPostgresDatastore(config.Database.User, config.Database.Password, config.Database.Host, config.Database.Port, config.Database.Database, config.Database.ExtraFlags)
	if err != nil {
		panic(err)
	}
	api := &ApiV1{
		config:    config,
		session:   sessions.NewCookieStore(securecookie.GenerateRandomKey(32)),
		datastore: pqdb,
		abis:      make(map[eos.AccountName]*eos.ABI),
		mailer:    email.NewMailgunSender(config.Mailer.Domain, config.Mailer.APIKey, config.Mailer.From),
	}

	handler := mux.NewRouter()
	handler.Use(mux.CORSMethodMiddleware(handler))
	handler.Use(middleware.SessionHandlingMiddleware(config.SecretKey))
	handler.HandleFunc("/config", api.GetConfig).Methods("GET")
	handler.HandleFunc("/login", api.login).Methods("POST")
	handler.HandleFunc("/createaccount", middleware.AuthenticatedMiddleware(api.createAccount)).Methods("POST")
	handler.HandleFunc("/register", api.register).Methods("POST")
	handler.HandleFunc("/sign_plaintext", middleware.AuthenticatedMiddleware(api.SignPlaintext)).Methods("POST")
	handler.HandleFunc("/sign_transaction", middleware.AuthenticatedMiddleware(api.SignTransaction)).Methods("POST")
	handler.HandleFunc("/users/self", middleware.AuthenticatedMiddleware(api.UserGetSelf)).Methods("GET")
	handler.HandleFunc("/users/{id}/beta_user", middleware.AdminMiddleware(api.datastore, api.UserUpdateBeta)).Methods("POST")
	handler.HandleFunc("/users/{id}/username", middleware.AdminMiddleware(api.datastore, api.UserUpdateUsername)).Methods("POST")
	handler.HandleFunc("/users", middleware.AdminMiddleware(api.datastore, api.ListUsers)).Methods("GET")
	handler.HandleFunc("/verify_email/{token}", api.verifyEmail).Methods("GET")
	handler.HandleFunc("/request_password_reset", api.requestPasswordReset).Methods("POST")
	handler.HandleFunc("/reset_password/{token}", api.resetPassword).Methods("GET")
	handler.HandleFunc("/submit_password_reset/{token}", api.submitPasswordReset).Methods("POST")

	api.Mux = handler

	return api
}
