package email

type Sender interface {
	SendVerificationEmail(to string, token string) error
	SendPasswordResetEmail(to string, token string) error 
}
