package email

import (
	"fmt"
	"github.com/mailgun/mailgun-go"
	"github.com/pkg/errors"
	"log"
)

type MailgunSender struct {
	Mg   mailgun.Mailgun
	from string
}

func NewMailgunSender(domain string, key string, from string) Sender {
	mg := mailgun.NewMailgun(domain, key, "")
	return &MailgunSender{
		Mg:   mg,
		from: from,
	}
}

func (m *MailgunSender) SendVerificationEmail(to string, token string) error {
	msg := m.Mg.NewMessage(m.from, "Bullstamp Account Approval Verification",
		fmt.Sprintf(`Hey there,

Congratulations, your Bullstamp account has been approved!

Please verify your email by clicking on the link below:

https://managedeos.apptoken.co/api/v1/verify_email/%s

If this wasn't you, feel free to ignore this message.`, token), to)

	mes, id, err := m.Mg.Send(msg)
	if err != nil {
		return errors.Wrap(err, "failed to send verification email")
	}

	log.Printf("sent verification email to %v with mes=%s id=%s\n", to, mes, id)

	return nil
}


func (m *MailgunSender) SendPasswordResetEmail(to string, token string) error {
	msg := m.Mg.NewMessage(m.from, "Bullstamp Password Reset",
		fmt.Sprintf(`Hey there,

Someone requested a password reset for your Bullstamp account. If that wasn't you, please ignore this email.

Reset your password by clicking on the link below:

https://managedeos.apptoken.co/api/v1/reset_password/%s `, token), to)

	mes, id, err := m.Mg.Send(msg)
	if err != nil {
		return errors.Wrap(err, "failed to send reset password email")
	}

	log.Printf("sent reset password email to %v with mes=%s id=%s\n", to, mes, id)

	return nil
}
