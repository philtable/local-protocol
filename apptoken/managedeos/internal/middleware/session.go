package middleware

import (
	"context"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"gitlab.com/apptoken/managedeos/internal/database"
	"gitlab.com/apptoken/managedeos/internal/internalerror"
	"log"
	"net/http"
)

type Session struct {
	token  *jwt.Token
	UserID uint64
}

func (s *Session) IsLoggedIn() bool {
	return s.UserID != 0
}

func (s *Session) Save(secretKey string, w http.ResponseWriter) error {
	s.token.Claims.(jwt.MapClaims)["user-id"] = s.UserID

	tokenString, err := s.token.SignedString([]byte(secretKey))
	if err != nil {
		return errors.Wrap(err, "failed to sign")
	}

	fmt.Printf("token: %v\n", tokenString)
	w.Header().Set("Set-Authorization", tokenString)
	return nil
}

func NewSession(uid uint64) *Session {
	session := &Session{
		token: jwt.NewWithClaims(jwt.SigningMethodHS512, jwt.MapClaims{
			"user-id": uid,
		}),
		UserID: uid,
	}
	return session
}

func SessionHandlingMiddleware(key string) mux.MiddlewareFunc {
	return func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			defer func() {
				h.ServeHTTP(w, r)
			}()

			tokenString := r.Header.Get("Authorization")
			if tokenString == "" {
				return
			}

			token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				// Don't forget to validate the alg is what you expect:
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}

				// hmacSampleSecret is a []byte containing your secret, e.g. []byte("my_secret_key")
				return []byte(key), nil
			})

			if err == nil {
				if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid && claims.Valid() == nil {
					if value, ok := claims["user-id"]; ok {
						realSession := &Session{token: token, UserID: 0}
						realSession.UserID = uint64(value.(float64))
						r = r.WithContext(context.WithValue(r.Context(), "session", realSession))
					}
				}
			}
		})
	}
}

func AuthenticatedMiddleware(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		session := GetSession(r)
		if session == nil || !session.IsLoggedIn() {
			internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotLoggedIn, "not logged in"))
			return
		}

		h.ServeHTTP(w, r)
	}
}

func AdminMiddleware(d database.Datastore, h http.HandlerFunc) http.HandlerFunc {
	return AuthenticatedMiddleware(func(w http.ResponseWriter, r *http.Request) {
		session := GetSession(r)
		user, err := d.GetUserById(session.UserID)
		if err != nil {
			internalerror.WriteError(w, internalerror.NewInternalServerError("failed to load user"))
			return
		}

		if !user.Admin {
			log.Printf("non-admin %v tried to do admin things!\n", user.Id)
			internalerror.WriteError(w, internalerror.NewHttpError(http.StatusUnauthorized, internalerror.ErrTypeNotAdmin, "not admin"))
			return
		}

		h.ServeHTTP(w, r)
	})
}

func GetSession(r *http.Request) *Session {
	session := r.Context().Value("session")
	if session != nil {
		return session.(*Session)
	}
	return nil
}
