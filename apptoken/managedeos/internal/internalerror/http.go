package internalerror

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type ErrorType string

const ErrTypeInvalidArgument = "invalid_argument"
const ErrTypeInternalError = "internal_error"
const ErrTypeLoginFailed = "login_failed"
const ErrTypeAlreadyLoggedIn = "already_logged_in"
const ErrTypeNotLoggedIn = "not_logged_in"
const ErrTypeNotBetaUser = "not_beta_user"
const ErrTypeNotEmailVerified = "not_email_verified"
const ErrTypeNotAdmin = "not_admin"

type HttpError struct {
	ErrCode int
	ErrType ErrorType
	Message string
}

func (h HttpError) Error() string {
	return fmt.Sprintf("code=%d type=%s", h.ErrCode, h.ErrType)
}

func NewInvalidArgumentError(why string) error {
	return &HttpError{ErrCode: http.StatusBadRequest, ErrType: ErrTypeInvalidArgument, Message: why}
}

func NewInternalServerError(why string) error {
	return &HttpError{ErrCode: http.StatusInternalServerError, ErrType: ErrTypeInternalError, Message: why}
}

func NewHttpError(code int, errType ErrorType, why string) error {
	return &HttpError{ErrCode: code, ErrType: errType, Message: why}
}

func WriteError(w http.ResponseWriter, err error) {
	switch v := err.(type) {
	case *HttpError:
		w.WriteHeader(v.ErrCode)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"ok":      false,
			"error":   v.ErrType,
			"message": v.Message,
		})
	default:
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(map[string]interface{}{
			"ok":      false,
			"error":   "internal_error",
			"message": err.Error(),
		})
	}
}
