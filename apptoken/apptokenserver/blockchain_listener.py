import argparse
import requests
import datetime
import time
import logging
import dateutil.parser
import zmq
import random

from app import db
import models
import queries
import constants

context = zmq.Context()
socket = context.socket(zmq.PUB)

SMART_CONTRACT_ACCOUNT = 'apptokendoco'
TOKEN_CONTRACT_ACCOUNT = 'socialhoster'

STAMP_ACTION_NAME = 'stamp'
AVATAR_ACTION_NAME = 'avatar'
TIP_ACTION_NAME = 'transfer'

MAX_ACTION_BATCH_SIZE = 10000
POLLING_WAIT_TIME_SECONDS = 2

BP_API_URLS = [
    'http://eos.greymass.com',
    'http://api.eosnewyork.io',
    'http://api1.eosasia.one',
]


def get_actions(account_name, pos, offset):
    data = {
        'account_name': account_name,
        'pos': pos,
        'offset': offset
    }

    for url in BP_API_URLS:
        r = requests.post(url + '/v1/history/get_actions', json=data)
        if r.status_code == requests.codes.ok:
            return r.json()

    raise Exception('Unable to reach any of the block producers')


def is_hex(str):
    try:
        int(str, 16)
        return True
    except ValueError:
        return False


def process_stamp(action):
    # Double check that it is made to the correct account.
    if action['account'] != SMART_CONTRACT_ACCOUNT:
        return

    # Soft ignore duplicate submissions.
    stamp = models.Stamp.query.get(action['trx_id'])
    if stamp:
        logging.info('Stamp {} already exists'.format(action['trx_id']))
        return

    reply_to_id = action['data']['reply_to']
    if reply_to_id is None or len(reply_to_id) != 64 or not is_hex(reply_to_id):
        reply_to = None
    else:
        reply_to = models.Stamp.query.get(reply_to_id)

    db.session.add(models.Stamp(
        transaction_id=action['trx_id'],
        seq=action['global_action_seq'],
        timestamp=action['timestamp'],
        author=action['authorization'][0]['actor'],
        reply_to=reply_to,
        content=action['data']['content']
    ))

    db.session.commit()

    socket.send_json({
        'type': 'STAMP',
        'data': queries.get_stamp(action['trx_id']),
    })


def process_avatar(action):
    # Double check that it is made to the correct account.
    if action['account'] != SMART_CONTRACT_ACCOUNT:
        return

    db.session.add(models.Avatar(
        transaction_id=action['trx_id'],
        seq=action['global_action_seq'],
        timestamp=action['timestamp'],
        username=action['authorization'][0]['actor'],
        url=action['data']['url']
    ))
    db.session.commit()

    socket.send_json({
        'type': 'AVATAR',
        'data': queries.get_avatar(action['authorization'][0]['actor'])
    })


def transaction_id_from_memo(memo):
    if not memo.startswith('tip:'):
        return '', False
    return memo[4:], True


def valid_tip(action, stamp_or_reply):
    to = action['data']['to']
    frm = action['data']['from']

    if to == frm:
        return False
    if to != stamp_or_reply.author:
        return False
    return True


def parse_amount(quantity):
    return int(float(quantity.split(' ')[0]) * constants.APP_TOKEN_UNIT)


def process_tip(action):
    if action['account'] != TOKEN_CONTRACT_ACCOUNT:
        return

    destination_id, success = transaction_id_from_memo(action['data']['memo'])
    if not success:
        return
    amount = parse_amount(action['data']['quantity'])

    stamp = models.Stamp.query.get(destination_id)
    if not stamp:
        return

    db.session.add(models.Tip(
        transaction_id=action['trx_id'],
        seq=action['global_action_seq'],
        timestamp=action['timestamp'],
        creator=action['authorization'][0]['actor'],
        stamp=stamp,
        amount=amount
    ))
    db.session.commit()

    socket.send_json({
        'type': 'TIP',
        'data': queries.get_tip(action['trx_id'])
    })


action_name_to_processor = {
    STAMP_ACTION_NAME: process_stamp,
    AVATAR_ACTION_NAME: process_avatar,
    TIP_ACTION_NAME: process_tip,
}


def parse_action(raw_action):
    timestamp = dateutil.parser.parse(raw_action['block_time'])

    return {
        'account_action_seq': raw_action['account_action_seq'],
        'global_action_seq': raw_action['global_action_seq'],
        'timestamp': timestamp,
        'trx_id': raw_action['action_trace']['trx_id'],
        'account': raw_action['action_trace']['act']['account'],
        'name': raw_action['action_trace']['act']['name'],
        'authorization': raw_action['action_trace']['act']['authorization'],
        'data': raw_action['action_trace']['act']['data'],
    }


def process_action(action, account):
    if action['account'] != account:
        return

    if action['name'] in action_name_to_processor:
        action_name_to_processor[action['name']](action)
        logging.info('Processed action {}: {}'.format(action['name'], action['trx_id']))
    else:
        logging.info('Invalid action name {}'.format(action['name']))
        pass


def get_new_actions_for_account(account):
    last_processed_seq = -1
    last_action = models.LastAction.query.get(account)
    if last_action:
        last_processed_seq = last_action.last_processed_seq

    data = get_actions(
        account_name=account,
        pos=last_processed_seq + 1,
        offset=MAX_ACTION_BATCH_SIZE
    )
    return data['actions']


def process_new_actions_for_account(new_actions, account):
    for raw_action in new_actions:
        action = parse_action(raw_action)
        if type(action['data']) != dict:
            # old stamp format
            continue

        process_action(action, account)

        updates = {
            'last_processed_seq': action['account_action_seq'],
            'timestamp': datetime.datetime.utcnow()
        }
        last_action = models.LastAction.query.get(account)
        if last_action is None:
            db.session.add(models.LastAction(id=account, timestamp=updates['timestamp'],
                                             last_processed_seq=updates['last_processed_seq']))
        else:
            last_action.last_processed_seq = updates['last_processed_seq']
            last_action.timestamp = updates['timestamp']
        db.session.commit()
        logging.info('Updated last action for {}: {}'.format(account, updates))


def process_new_actions():
    # IMPORTANT: The order in which actions are retrieved then processed.

    new_token_contract_actions = get_new_actions_for_account(TOKEN_CONTRACT_ACCOUNT)
    new_smart_contract_actions = get_new_actions_for_account(SMART_CONTRACT_ACCOUNT)

    has_new_actions = len(new_token_contract_actions) > 0 or \
        len(new_smart_contract_actions) > 0

    # If there are no actions, sample the logs.
    if has_new_actions or random.random() < 0.1:
        logging.info('Polled for new actions')
        logging.info('Found {} new actions for {}'.format(len(new_token_contract_actions), TOKEN_CONTRACT_ACCOUNT))
        logging.info('Found {} new actions for {}'.format(len(new_smart_contract_actions), SMART_CONTRACT_ACCOUNT))

    try:
        process_new_actions_for_account(
            new_smart_contract_actions, SMART_CONTRACT_ACCOUNT)
        process_new_actions_for_account(
            new_token_contract_actions, TOKEN_CONTRACT_ACCOUNT)
    except Exception as e:
        logging.exception(e, exc_info=True)


def poll_for_new_actions():
    logging.info('Starting blockchain listener...')

    while True:
        logging.info('Polling...')
        process_new_actions()
        time.sleep(POLLING_WAIT_TIME_SECONDS)
        logging.info('Done polling...')


parser = argparse.ArgumentParser(description='EOS blockchain listener.')
parser.add_argument('--log_to_file', type=bool, default=False)

if __name__ == '__main__':
    args = parser.parse_args()

    if args.log_to_file:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s',
                            filename='/var/log/apptoken/poller.log',
                            filemode='w')
    else:
        logging.basicConfig(level=logging.INFO,
                            format='%(asctime)s %(name)-8s %(levelname)-8s %(message)s')

    # logging.getLogger('requests').setLevel(logging.WARNING)
    # logging.getLogger('urllib3').setLevel(logging.WARNING)

    socket.bind("tcp://*:{}".format(5556))
    poll_for_new_actions()
