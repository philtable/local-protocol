from app import db
from datetime import datetime
from models import Stamp, Tip

db.create_all()

stamp1 = Stamp(
    transaction_id="9e131d99439c4bb13ecced83c129193b4ec4c27d1e99d623330ff04ff40e7738",
    seq=23452,
    timestamp=datetime.fromtimestamp(1537332655),
    author="thistestdude",
    reply_to=None,
    content="Welcome everyone!"
)

reply1 = Stamp(
    transaction_id="e73937330b835ccdf399bd858bf0a54917ddac973d1f6abe118c6dbe95f1b65d",
    seq=23454,
    timestamp=datetime.fromtimestamp(1537332677),
    author="coolcoolcool",
    reply_to=stamp1,
    content="Hey dude!"
)

tip1 = Tip(
    transaction_id="a7219c668e1a0db5144e365bfdb2f8d810ac0633002d905bde8a383e7e30f3dd",
    seq=23555,
    timestamp=datetime.fromtimestamp(1537335888),
    creator="verycooldude",
    stamp=stamp1,
    amount=2
)


db.session.add(stamp1)
db.session.add(reply1)
db.session.add(tip1)
db.session.commit()
