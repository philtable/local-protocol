# Smallest denomination of the APP token.
APP_TOKEN_UNIT = 10000
# The number of rows allowed for backfill per call
BACKFILL_BATCH_LIMIT = 1000
