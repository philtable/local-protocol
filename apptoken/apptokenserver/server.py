import argparse
import json
from flask import request, jsonify
from app import app
import constants
import queries

DEFAULT_PAGE_SIZE = 25
MAX_PAGE_SIZE = 100


class APIError(Exception):
    def __init__(self, message, status_code=None):
        Exception.__init__(self)
        self.message = message
        self.status_code = status_code if status_code is not None else 400

    def to_dict(self):
        return {'error': self.message}


@app.errorhandler(APIError)
def handle_api_error(e):
    response = jsonify(e.to_dict())
    response.status_code = e.status_code
    return response, e.status_code


@app.route('/', methods=['GET'])
def hello():
    return 'Welcome to the Apptoken API'


# returns page size, offset, and limit
def get_page_info_from_request():
    page = request.args.get('page', default=1, type=int)
    page_size = request.args.get(
        'page_size', default=DEFAULT_PAGE_SIZE, type=int)
    page_size = min(page_size, MAX_PAGE_SIZE)

    if page < 1:
        raise APIError('Page has to be positive')
    if page_size < 1:
        raise APIError('Page size has to be positive')

    return page_size, (page - 1) * page_size, page_size + 1


@app.route('/stamps', methods=['GET'])
def get_stamps():
    page_size, offset, limit = get_page_info_from_request()

    stamps = queries.get_stamps(offset, limit)

    return jsonify({
        'stamps': stamps[:page_size],
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/stamps/after/<seq>', methods=['GET'])
def get_stamps_after_seq(seq):
    page_size, offset, limit = get_page_info_from_request()
    stamps = queries.get_stamps_after_seq(seq, offset, limit)

    return jsonify({
        'stamps': stamps[:page_size],
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/authors/<author>/stamps', methods=['GET'])
def get_stamps_for_author(author):
    page_size, offset, limit = get_page_info_from_request()

    stamps = queries.get_stamps_by_author(author, offset, limit)

    return jsonify({
        'stamps': stamps[:page_size],
        'page_info': {
            'has_next_page': len(stamps) > page_size,
        }
    })


@app.route('/authors/<author>/avatar', methods=['GET'])
def get_avatar_for_author(author):
    avatar = queries.get_avatar(author)

    return jsonify({
        'username': author,
        'url': avatar["url"] if avatar else None,
    })


@app.route('/stamps/<stamp_id>', methods=['GET'])
def get_stamp(stamp_id):
    stamp = queries.get_stamp(stamp_id)
    if not stamp:
        raise APIError("unable to find stamp", status_code=404)

    return jsonify(stamp)


@app.route('/stamps/<stamp_id>/replies', methods=['GET'])
def get_replies_for_stamp(stamp_id):
    page_size, offset, limit = get_page_info_from_request()

    replies = queries.get_replies(stamp_id, offset, limit)

    return jsonify({
        'replies': replies[:page_size],
        'page_info': {
            'has_next_page': len(replies) > page_size,
        }
    })

@app.route('/tips/<tip_id>', methods=['GET'])
def get_tip(tip_id):
    tip = queries.get_tip(tip_id)
    if not tip:
        raise APIError("unable to find tip", status_code=404)

    return jsonify(tip)

@app.route('/tips', methods=['GET'])
def get_tips():
    page_size, offset, limit = get_page_info_from_request()

    tips = queries.get_tips(offset, limit)

    return jsonify({
        'tips': tips[:page_size],
        'page_info': {
            'has_next_page': len(tips) > page_size,
        }
    })

@app.route('/<creator>/tips', methods=['GET'])
def get_tips_for_creator(creator):
    page_size, offset, limit = get_page_info_from_request()

    tips = queries.get_tips_by_creator(creator, offset, limit)

    return jsonify({
        'tips': tips[:page_size],
        'page_info': {
            'has_next_page': len(tips) > page_size,
        }
    })

# backfill for backend end-points
@app.route('/tips/backfill/<seq>', methods=['GET'])
def get_tips_backfill_after_seq(seq):
    if seq is None:
        return jsonify({})

    tips_raw = (queries.get_tips_after_seq_raw(seq, constants.BACKFILL_BATCH_LIMIT))
    # TODO: use more efficient serialization
    return jsonify([(dict(row.items())) for row in tips_raw[0]])

@app.route('/stamps/backfill/<seq>', methods=['GET'])
def get_stamps_backfill_after_seq(seq):
    if seq is None:
        return jsonify({})
    stamps_raw = (queries.get_stamps_after_seq_raw(seq, constants.BACKFILL_BATCH_LIMIT))
    # TODO: use more efficient serialization
    return jsonify([(dict(row.items())) for row in stamps_raw[0]])

@app.route('/replies/backfill/<seq>', methods=['GET'])
def get_replies_backfill_after_seq(seq):
    if seq is None:
        return jsonify({})
    stamps_raw = (queries.get_stamps_replies_after_seq_raw(seq, constants.BACKFILL_BATCH_LIMIT))
    # TODO: use more efficient serialization
    return jsonify([(dict(row.items())) for row in stamps_raw[0]])


parser = argparse.ArgumentParser(description='App Token backend.')
parser.add_argument('--port', type=int, default=5557)

if __name__ == '__main__':
    args = parser.parse_args()
    app.run(host='0.0.0.0', port=args.port, threaded=True)
