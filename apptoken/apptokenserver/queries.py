from sqlalchemy.sql import text
from app import db

def get_stamp(stamp_id):
    result = db.engine.execute(text("""
SELECT
    row_to_json(a) AS stamp
FROM (
    {format_stamp}
    WHERE s.transaction_id = decode(:id, 'hex')
) a
""".format(format_stamp=__FORMAT_STAMP__)), id=stamp_id).fetchone()
    return result['stamp'] if result else None


def get_stamps(offset, limit):
    cursor = db.engine.execute(text("""
SELECT
    array_to_json(array_agg(row_to_json(a))) AS stamps
FROM (
    {format_stamp}
    WHERE reply_to_id IS NULL
    ORDER BY s.timestamp DESC
    OFFSET :offset
    LIMIT :limit
) a
""".format(format_stamp=__FORMAT_STAMP__)), offset=offset, limit=limit)

    stamps = cursor.fetchone()['stamps']
    return stamps if stamps is not None else []


def get_stamps_by_author(author, offset, limit):
    cursor = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS stamps
    FROM (
        {format_stamp}
        WHERE s.author = :author AND s.reply_to_id IS NULL
        ORDER BY s.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), author=author, offset=offset, limit=limit)

    stamps = cursor.fetchone()['stamps']
    return stamps if stamps is not None else []

def get_stamps_after_seq(seq, offset, limit):
    cursor = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS stamps
    FROM (
        {format_stamp}
        WHERE s.seq >= :seq
        ORDER BY s.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), seq=seq, offset=offset, limit=limit)

    stamps = cursor.fetchone()['stamps']
    return stamps if stamps is not None else []


def get_replies(stamp_id, offset, limit):
    cursor = db.engine.execute(text("""
SELECT
    array_to_json(array_agg(row_to_json(a))) AS replies
FROM (
    {format_stamp}
    WHERE s.reply_to_id = decode(:id, 'hex')
    ORDER BY s.timestamp ASC
    OFFSET :offset
    LIMIT :limit
) a
""".format(format_stamp=__FORMAT_STAMP__)), id=stamp_id, offset=offset, limit=limit)
    replies = cursor.fetchone()['replies']
    return replies if replies is not None else []


def get_tip(tip_id):
    result = db.engine.execute(text("""
SELECT
    row_to_json(a) AS tip
FROM (
    SELECT
        ENCODE(t.transaction_id, 'hex') AS transaction_id,
        t.timestamp,
        t.seq,
        t.creator,
        t.amount,
        row_to_json(l) as to
    FROM tips t
    INNER JOIN LATERAL (
        {format_stamp}
    ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
    WHERE t.transaction_id = decode(:id, 'hex')
) a;
""".format(format_stamp=__FORMAT_STAMP__)), id=tip_id).fetchone()
    return result['tip'] if result else None


def get_tips(offset, limit):
    cursor = db.engine.execute(text("""
SELECT
    array_to_json(array_agg(row_to_json(a))) AS tips
FROM (
    SELECT
        ENCODE(t.transaction_id, 'hex') AS transaction_id,
        t.timestamp,
        t.seq,
        t.creator,
        t.amount,
        row_to_json(l) as to
    FROM tips t
    INNER JOIN LATERAL (
        {format_stamp}
    ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
    ORDER BY t.timestamp DESC
    OFFSET :offset
    LIMIT :limit
) a;
""".format(format_stamp=__FORMAT_STAMP__)), offset=offset, limit=limit)

    tips = cursor.fetchone()['tips']
    return tips if tips is not None else []


def get_tips_by_creator(creator, offset, limit):
    cursor = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM (
        SELECT
            ENCODE(t.transaction_id, 'hex') AS transaction_id,
            t.timestamp,
            t.seq,
            t.creator,
            t.amount,
            row_to_json(l) as to
        FROM tips t
        INNER JOIN LATERAL (
            {format_stamp}
        ) l ON DECODE(l.transaction_id, 'hex') = t.stamp_id
        WHERE t.creator = :creator
        ORDER BY t.timestamp DESC
        OFFSET :offset
        LIMIT :limit
    ) a
    """.format(format_stamp=__FORMAT_STAMP__)), creator=creator, offset=offset, limit=limit)

    tips = cursor.fetchone()['tips']
    return tips if tips is not None else []


def get_avatar(username):
    result = db.engine.execute(text("""
SELECT
    row_to_json(a) AS avatar
FROM (
    SELECT
        ENCODE(a.transaction_id, 'hex') AS transaction_id,
        a.timestamp,
        a.seq,
        a.username AS username,
        a.url
    FROM avatars a
    WHERE a.username = :username
    ORDER BY a.seq DESC
    LIMIT 1
) a;
"""), username=username).fetchone()
    return result['avatar'] if result else None

# backfill for backend queries
def get_stamps_after_seq_raw(seq, limit):
    result = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM
    (
        SELECT * FROM stamps
        WHERE seq >= :seq
            AND reply_to_id IS NULL
        ORDER BY stamps.timestamp DESC
        LIMIT :limit
    ) a;
    """), seq=seq, limit=limit).fetchone()
    return result

def get_stamps_replies_after_seq_raw(seq, limit):
    result = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM
    (
        SELECT * FROM stamps
        WHERE seq >= :seq
            AND reply_to_id IS NOT NULL
        ORDER BY stamps.timestamp DESC
        LIMIT :limit
    ) a;
    """), seq=seq, limit=limit).fetchone()
    return result


def get_tips_after_seq_raw(seq, limit):
    result = db.engine.execute(text("""
    SELECT
        array_to_json(array_agg(row_to_json(a))) AS tips
    FROM
    (
        SELECT * FROM tips
        WHERE seq >= :seq
        ORDER BY tips.timestamp DESC
        LIMIT :limit
    ) a;
    """), seq=seq, limit=limit).fetchone()
    return result


__FORMAT_STAMP__ = """
SELECT
    ENCODE(s.transaction_id, 'hex') AS transaction_id,
    s.seq,
    s.timestamp,
    s.author,
    ENCODE(s.reply_to_id, 'hex') AS reply_to,
    s.content,
    num_replies,
    tips as tips
FROM stamps s
LEFT JOIN LATERAL (
    SELECT COALESCE(COUNT(*), 0) AS num_replies
    FROM stamps r
    WHERE s.transaction_id = r.reply_to_id
) r ON TRUE
LEFT JOIN LATERAL (
    SELECT COALESCE(SUM(amount), 0) AS tips
    FROM tips t
    WHERE s.transaction_id = t.stamp_id
) t ON TRUE
"""
