#!/bin/sh

# Checks to run before running code

# E501: line too long (>80c)
# E302: expected 2 blank lines, found n
flake8 --ignore "E501,E302" .
