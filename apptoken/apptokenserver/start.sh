#!/usr/bin/env bash

# set up bash to handle errors more aggressively - a "strict mode" of sorts
set -e # give an error if any command finishes with a non-zero exit code
set -u # give an error if we reference unset variables
set -o pipefail # for a pipeline, if any of the commands fail with a non-zero exit code, fail the entire pipeline with that exit code

pushd ~/apptokenserver

git reset --hard
git clean -fd
git fetch origin
git reset --hard origin/master

pip3 install -r requirements.txt

echo 'smoke testing app'
./test.sh

# start apptoken server
echo 'killing old apptokenserver instance'
tmux send-keys -t apptoken C-c || true # kill old tmux session, if present
sleep 1
echo 'starting new apptokenserver instance'
tmux new-session -d -s apptoken 'gunicorn --bind 0.0.0.0:5557 wsgi:app 2>&1 | tee -a /root/server.log' # start a new tmux session, but don't attach to it just yet

# start blockchain listener (updates apptoken server's DB)
tmux send-keys -t blockchain_listener C-c || true # kill old tmux session, if present
sleep 1
tmux new-session -d -s blockchain_listener 'python3 blockchain_listener.py 2>&1 | tee -a /root/blockchain_listener.log' # start a new tmux session, but don't attach to it just yet

popd

echo 'APPTOKEN SERVER STARTED, MONITOR WITH tmux attach -t apptoken'
