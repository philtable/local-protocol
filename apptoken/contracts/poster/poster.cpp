#include <eosiolib/eosio.hpp>

using namespace eosio;

class poster : public contract {
  public:
    poster(account_name self) : contract(self) {}

    //@abi action
    void stamp(std::string content, std::string reply_to) {}

    //@abi action
    void reply(std::string stamp_transaction_id, std::string content) {}
    
    //@abi action
    void avatar(std::string url) {}
};

EOSIO_ABI(poster, (stamp)(reply)(avatar))
